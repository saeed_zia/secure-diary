package com.diary.with.lock.myjournal.notepad.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\r\u0018\u00002\u00020\u0001B!\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\t\"\u0004\b\r\u0010\u000bR\u001e\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\u0002\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0012"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "", "isLoaded", "", "idType", "", "idNumber", "(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V", "getIdNumber", "()Ljava/lang/String;", "setIdNumber", "(Ljava/lang/String;)V", "getIdType", "setIdType", "()Ljava/lang/Boolean;", "setLoaded", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "app_debug"})
public final class AdLoadStatus {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isLoaded;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String idType;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String idNumber;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isLoaded() {
        return null;
    }
    
    public final void setLoaded(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getIdType() {
        return null;
    }
    
    public final void setIdType(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getIdNumber() {
        return null;
    }
    
    public final void setIdNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public AdLoadStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean isLoaded, @org.jetbrains.annotations.NotNull()
    java.lang.String idType, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
        super();
    }
}