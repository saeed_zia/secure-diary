package com.diary.with.lock.myjournal.notepad.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u001e\u0010\f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eJ\u0016\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\u0011"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "", "()V", "ShowAdMob", "", "context", "Landroid/app/Activity;", "newIntent", "Landroid/content/Intent;", "adMobShoeClose", "adMobShowCloseOnly", "adMobShowOnly", "adMobStartActivityResult", "Code", "", "madMobShoeClose", "Companion", "app_debug"})
public final class InterstitalAdsSplash {
    @org.jetbrains.annotations.Nullable()
    private static com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    private static com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    private static com.facebook.ads.InterstitialAd interstitialAd_fb;
    public static final com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash.Companion Companion = null;
    
    public final void ShowAdMob(@org.jetbrains.annotations.NotNull()
    android.app.Activity context, @org.jetbrains.annotations.NotNull()
    android.content.Intent newIntent) {
    }
    
    public final void adMobShoeClose(@org.jetbrains.annotations.NotNull()
    android.app.Activity context, @org.jetbrains.annotations.NotNull()
    android.content.Intent newIntent) {
    }
    
    public final void madMobShoeClose(@org.jetbrains.annotations.NotNull()
    android.app.Activity context, @org.jetbrains.annotations.NotNull()
    android.content.Intent newIntent) {
    }
    
    public final void adMobStartActivityResult(@org.jetbrains.annotations.NotNull()
    android.app.Activity context, @org.jetbrains.annotations.NotNull()
    android.content.Intent newIntent, int Code) {
    }
    
    public final void adMobShowCloseOnly(@org.jetbrains.annotations.NotNull()
    android.app.Activity context) {
    }
    
    public final void adMobShowOnly(@org.jetbrains.annotations.NotNull()
    android.app.Activity context) {
    }
    
    public InterstitalAdsSplash() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f\u00a8\u0006\u0012"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash$Companion;", "", "()V", "interstitialAd_fb", "Lcom/facebook/ads/InterstitialAd;", "mInterstitialAd", "Lcom/google/android/gms/ads/InterstitialAd;", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "loadInterstitialAd", "", "context", "Landroid/content/Context;", "loadInterstitialAd_fb", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
            return null;
        }
        
        public final void setSharedPref(@org.jetbrains.annotations.Nullable()
        com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
        }
        
        public final void loadInterstitialAd(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.facebook.ads.InterstitialAd loadInterstitialAd_fb(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}