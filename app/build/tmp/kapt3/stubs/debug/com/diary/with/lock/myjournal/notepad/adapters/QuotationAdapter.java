package com.diary.with.lock.myjournal.notepad.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000eH\u0016J\u0014\u0010\u0017\u001a\u00020\u00102\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f\u00a8\u0006\u001a"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationAdapter$ViewHolder;", "quoteArrayList", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/QuoteModel;", "quotationListeners", "Lcom/diary/with/lock/myjournal/notepad/interfaces/QuotationListeners;", "(Ljava/util/ArrayList;Lcom/diary/with/lock/myjournal/notepad/interfaces/QuotationListeners;)V", "getQuoteArrayList", "()Ljava/util/ArrayList;", "setQuoteArrayList", "(Ljava/util/ArrayList;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "updateCheckList", "newQuoteList", "ViewHolder", "app_debug"})
public final class QuotationAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.diary.with.lock.myjournal.notepad.adapters.QuotationAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.QuoteModel> quoteArrayList;
    private final com.diary.with.lock.myjournal.notepad.interfaces.QuotationListeners quotationListeners = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.diary.with.lock.myjournal.notepad.adapters.QuotationAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.adapters.QuotationAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void updateCheckList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.QuoteModel> newQuoteList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.QuoteModel> getQuoteArrayList() {
        return null;
    }
    
    public final void setQuoteArrayList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.QuoteModel> p0) {
    }
    
    public QuotationAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.QuoteModel> quoteArrayList, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.QuotationListeners quotationListeners) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindItems", "", "quoteModel", "Lcom/diary/with/lock/myjournal/notepad/models/QuoteModel;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bindItems(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.models.QuoteModel quoteModel) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}