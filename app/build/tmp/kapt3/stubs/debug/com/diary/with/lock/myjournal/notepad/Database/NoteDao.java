package com.diary.with.lock.myjournal.notepad.Database;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u001f\u0010\n\u001a\u00020\u00032\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u0019\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H\'J\u0018\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013H\'J!\u0010\u0015\u001a\u00020\u00032\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u0016\"\u00020\bH\'\u00a2\u0006\u0002\u0010\u0017\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0018"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Database/NoteDao;", "", "deleteNoteById", "", "noteId", "", "getAll", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertAllNotes", "noteList", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertNote", "note", "(Lcom/diary/with/lock/myjournal/notepad/Database/Note;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "moveToTrash", "id", "status", "", "restoreNote", "updateNote", "", "([Lcom/diary/with/lock/myjournal/notepad/Database/Note;)V", "app_debug"})
public abstract interface NoteDao {
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "SELECT * FROM note_table")
    public abstract java.lang.Object getAll(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note>> p0);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert()
    public abstract java.lang.Object insertNote(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note note, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert()
    public abstract java.lang.Object insertAllNotes(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> noteList, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
    
    @androidx.room.Update()
    public abstract void updateNote(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note... note);
    
    @androidx.room.Query(value = "DELETE FROM note_table WHERE id = :noteId")
    public abstract void deleteNoteById(int noteId);
    
    @androidx.room.Query(value = "UPDATE note_table SET isTrashed=:status WHERE id = :id")
    public abstract void moveToTrash(int id, boolean status);
    
    @androidx.room.Query(value = "UPDATE note_table SET isTrashed=:status WHERE id = :id")
    public abstract void restoreNote(int id, boolean status);
}