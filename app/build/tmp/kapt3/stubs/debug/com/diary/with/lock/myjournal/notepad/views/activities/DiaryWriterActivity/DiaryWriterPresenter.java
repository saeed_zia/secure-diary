package com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004\u00a8\u0006\u0011"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterPresenter;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterContract$Presenter;", "view", "Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterContract$View;", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterContract$View;)V", "mView", "getMView", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterContract$View;", "setMView", "getAllNoteDatFromDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "moveNoteToTrash", "noteId", "", "moveNoteToTrashInBackup", "app_debug"})
public final class DiaryWriterPresenter implements com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.Presenter {
    @org.jetbrains.annotations.NotNull()
    private com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.View mView;
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.View getMView() {
        return null;
    }
    
    public final void setMView(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.View p0) {
    }
    
    @java.lang.Override()
    public void getAllNoteDatFromDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase) {
    }
    
    @java.lang.Override()
    public void moveNoteToTrash(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId) {
    }
    
    @java.lang.Override()
    public void moveNoteToTrashInBackup(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId) {
    }
    
    public DiaryWriterPresenter(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.View view) {
        super();
    }
}