package com.diary.with.lock.myjournal.notepad.views.activities.quotation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010,\u001a\u00020-J\u0006\u0010.\u001a\u00020-J\u0018\u0010/\u001a\u00020-2\b\u00100\u001a\u0004\u0018\u00010\u000b2\u0006\u00101\u001a\u00020\u000bJ\u0006\u00102\u001a\u00020-J\u0012\u00103\u001a\u00020-2\b\u00104\u001a\u0004\u0018\u000105H\u0014J\u0010\u00106\u001a\u00020-2\u0006\u00107\u001a\u00020\u0018H\u0016J\u0010\u00108\u001a\u00020-2\u0006\u00109\u001a\u00020\u000bH\u0002J\u0006\u0010:\u001a\u00020-J\u000e\u0010;\u001a\u00020-2\u0006\u0010<\u001a\u00020=J\u0006\u0010>\u001a\u00020-R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0007\"\u0004\b\u001f\u0010\tR\u001e\u0010 \u001a\u0004\u0018\u00010\u0018X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+\u00a8\u0006?"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/quotation/ShowQuotationActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/FrameClickListener;", "()V", "author", "", "getAuthor", "()Ljava/lang/String;", "setAuthor", "(Ljava/lang/String;)V", "combined", "Landroid/graphics/Bitmap;", "getCombined", "()Landroid/graphics/Bitmap;", "setCombined", "(Landroid/graphics/Bitmap;)V", "frameAdapter", "Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter;", "getFrameAdapter", "()Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter;", "setFrameAdapter", "(Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter;)V", "frameArraylist", "Ljava/util/ArrayList;", "", "getFrameArraylist", "()Ljava/util/ArrayList;", "setFrameArraylist", "(Ljava/util/ArrayList;)V", "quoteText", "getQuoteText", "setQuoteText", "selectedFrampos", "getSelectedFrampos", "()Ljava/lang/Integer;", "setSelectedFrampos", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "AddFrames", "", "applyFrame", "applyFrameOnTextbmp", "tv_bmp", "frame", "checkStoragePermissionAndShare", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onFrameClick", "position", "saveImageExternal", "image", "setSetting", "shareImageUri", "uri", "Landroid/net/Uri;", "shareQuotation", "app_debug"})
public final class ShowQuotationActivity extends androidx.appcompat.app.AppCompatActivity implements com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.Integer> frameArraylist;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter frameAdapter;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String quoteText;
    @org.jetbrains.annotations.Nullable()
    private android.graphics.Bitmap combined;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer selectedFrampos;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String author;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Integer> getFrameArraylist() {
        return null;
    }
    
    public final void setFrameArraylist(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter getFrameAdapter() {
        return null;
    }
    
    public final void setFrameAdapter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getQuoteText() {
        return null;
    }
    
    public final void setQuoteText(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.Bitmap getCombined() {
        return null;
    }
    
    public final void setCombined(@org.jetbrains.annotations.Nullable()
    android.graphics.Bitmap p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getSelectedFrampos() {
        return null;
    }
    
    public final void setSelectedFrampos(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAuthor() {
        return null;
    }
    
    public final void setAuthor(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void AddFrames() {
    }
    
    @java.lang.Override()
    public void onFrameClick(int position) {
    }
    
    public final void applyFrame() {
    }
    
    public final void applyFrameOnTextbmp(@org.jetbrains.annotations.Nullable()
    android.graphics.Bitmap tv_bmp, @org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap frame) {
    }
    
    public final void shareQuotation() {
    }
    
    private final void saveImageExternal(android.graphics.Bitmap image) {
    }
    
    public final void shareImageUri(@org.jetbrains.annotations.NotNull()
    android.net.Uri uri) {
    }
    
    public final void checkStoragePermissionAndShare() {
    }
    
    public final void setSetting() {
    }
    
    public ShowQuotationActivity() {
        super();
    }
}