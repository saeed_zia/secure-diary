package com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001:\u0002\u0002\u0003\u00a8\u0006\u0004"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract;", "", "Presenter", "View", "app_debug"})
public abstract interface TrashContract {
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006\u0007"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$View;", "", "showAllData", "", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "app_debug"})
    public static abstract interface View {
        
        public abstract void showAllData(@org.jetbrains.annotations.NotNull()
        java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\f"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$Presenter;", "", "deleteNoteFromBackupDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "noteId", "", "deleteNoteFromDatabase", "getAllDatFromDatabase", "restoreNote", "restoreNoteInBackup", "app_debug"})
    public static abstract interface Presenter {
        
        public abstract void getAllDatFromDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase);
        
        public abstract void deleteNoteFromDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId);
        
        public abstract void restoreNote(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId);
        
        public abstract void deleteNoteFromBackupDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId);
        
        public abstract void restoreNoteInBackup(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId);
    }
}