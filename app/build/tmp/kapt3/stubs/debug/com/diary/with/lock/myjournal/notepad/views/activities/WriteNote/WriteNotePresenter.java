package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0005\u00a8\u0006\u0014"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$Presenter;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$BackupPresenter;", "view", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;)V", "mView", "getMView", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;", "setMView", "getAllDatFromDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "saveNoteInBackupDatabase", "note", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "saveNoteInDatabase", "updateNoteInBackupDatabase", "updateNoteInDatabase", "app_debug"})
public final class WriteNotePresenter implements com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.Presenter, com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.BackupPresenter {
    @org.jetbrains.annotations.NotNull()
    private com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View mView;
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View getMView() {
        return null;
    }
    
    public final void setMView(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View p0) {
    }
    
    @java.lang.Override()
    public void getAllDatFromDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase) {
    }
    
    @java.lang.Override()
    public void saveNoteInDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note note) {
    }
    
    @java.lang.Override()
    public void updateNoteInDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note note) {
    }
    
    @java.lang.Override()
    public void saveNoteInBackupDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note note) {
    }
    
    @java.lang.Override()
    public void updateNoteInBackupDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.Note note) {
    }
    
    public WriteNotePresenter(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View view) {
        super();
    }
}