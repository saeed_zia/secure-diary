package com.diary.with.lock.myjournal.notepad.views.activities.checkList;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010?\u001a\u00020@H\u0002J\u0006\u0010A\u001a\u00020@J\u0010\u0010B\u001a\u00020@2\u0006\u0010C\u001a\u00020DH\u0016J\u0010\u0010E\u001a\u00020.2\u0006\u0010F\u001a\u00020.H\u0002J\u0006\u0010G\u001a\u00020HJ\u0006\u0010I\u001a\u00020@J\u0018\u0010J\u001a\u00020@2\u0006\u0010K\u001a\u00020.2\u0006\u0010C\u001a\u00020DH\u0016J\b\u0010L\u001a\u00020@H\u0016J\u0018\u0010M\u001a\u00020@2\u0006\u0010N\u001a\u00020\"2\u0006\u0010C\u001a\u00020DH\u0016J\u0012\u0010O\u001a\u00020@2\b\u0010P\u001a\u0004\u0018\u00010QH\u0014J\u0010\u0010R\u001a\u00020@2\u0006\u0010C\u001a\u00020DH\u0016J\b\u0010S\u001a\u00020@H\u0014J\u0006\u0010T\u001a\u00020@J\b\u0010U\u001a\u00020@H\u0002J\u0006\u0010V\u001a\u00020@J\u0006\u0010W\u001a\u00020@J\u0006\u0010X\u001a\u00020@J\u0006\u0010Y\u001a\u00020@J\u0006\u0010Z\u001a\u00020@J\u0016\u0010[\u001a\u00020@2\f\u0010\\\u001a\b\u0012\u0004\u0012\u00020^0]H\u0016J\u0006\u0010_\u001a\u00020@J\u0006\u0010`\u001a\u00020@J\u0006\u0010a\u001a\u00020@R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001e\u0010!\u001a\u0004\u0018\u00010\"X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010&\u001a\u0004\b!\u0010#\"\u0004\b$\u0010%R\u001c\u0010\'\u001a\u0004\u0018\u00010(X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001c\u0010-\u001a\u0004\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u001c\u00103\u001a\u0004\u0018\u000104X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u00106\"\u0004\b7\u00108R\u001c\u00109\u001a\u0004\u0018\u00010:X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>\u00a8\u0006b"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/checkList/CheckListActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/CheckBoxClickListener;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;", "()V", "adapter", "Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter;", "getAdapter", "()Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter;", "setAdapter", "(Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter;)V", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "cal", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "getCal", "()Ljava/util/Calendar;", "setCal", "(Ljava/util/Calendar;)V", "checkArraylist", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/CheckListItem;", "getCheckArraylist", "()Ljava/util/ArrayList;", "setCheckArraylist", "(Ljava/util/ArrayList;)V", "emojiPopup", "Lcom/vanniktech/emoji/EmojiPopup;", "getEmojiPopup", "()Lcom/vanniktech/emoji/EmojiPopup;", "setEmojiPopup", "(Lcom/vanniktech/emoji/EmojiPopup;)V", "isNewNote", "", "()Ljava/lang/Boolean;", "setNewNote", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "presenter", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;", "getPresenter", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;", "setPresenter", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNoteModel", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "getSelectedNoteModel", "()Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "setSelectedNoteModel", "(Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;)V", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "addFirstCheckItem", "", "addItems", "deleteCheckItem", "position", "", "getFormatedDate", "date", "getTextSize", "", "loadInterAd", "onAfterTextChangeListener", "newText", "onBackPressed", "onCheckBoxClickListener", "status", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onEnterPressed", "onResume", "receiveIntent", "saveCheckList", "setSetting", "shareText", "showAlertDialog", "showAlertEnterSomething", "showAlertTitleFirstDialog", "showAllData", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "showInterAd", "showInterstitialAdmob", "showInterstitialFb", "app_debug"})
public final class CheckListActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener, com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> checkArraylist;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter adapter;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isNewNote;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.models.NoteModel selectedNoteModel;
    private java.util.Calendar cal;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter presenter;
    @org.jetbrains.annotations.Nullable()
    private com.vanniktech.emoji.EmojiPopup emojiPopup;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> getCheckArraylist() {
        return null;
    }
    
    public final void setCheckArraylist(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isNewNote() {
        return null;
    }
    
    public final void setNewNote(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.models.NoteModel getSelectedNoteModel() {
        return null;
    }
    
    public final void setSelectedNoteModel(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.models.NoteModel p0) {
    }
    
    public final java.util.Calendar getCal() {
        return null;
    }
    
    public final void setCal(java.util.Calendar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.vanniktech.emoji.EmojiPopup getEmojiPopup() {
        return null;
    }
    
    public final void setEmojiPopup(@org.jetbrains.annotations.Nullable()
    com.vanniktech.emoji.EmojiPopup p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void showAlertEnterSomething() {
    }
    
    public final float getTextSize() {
        return 0.0F;
    }
    
    public final void shareText() {
    }
    
    public final void receiveIntent() {
    }
    
    private final void addFirstCheckItem() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showAlertTitleFirstDialog() {
    }
    
    public final void showAlertDialog() {
    }
    
    private final void saveCheckList() {
    }
    
    private final java.lang.String getFormatedDate(java.lang.String date) {
        return null;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void addItems() {
    }
    
    @java.lang.Override()
    public void onCheckBoxClickListener(boolean status, int position) {
    }
    
    @java.lang.Override()
    public void onAfterTextChangeListener(@org.jetbrains.annotations.NotNull()
    java.lang.String newText, int position) {
    }
    
    @java.lang.Override()
    public void onEnterPressed(int position) {
    }
    
    @java.lang.Override()
    public void deleteCheckItem(int position) {
    }
    
    public final void setSetting() {
    }
    
    @java.lang.Override()
    public void showAllData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    public final void showInterAd() {
    }
    
    public final void loadInterAd() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public CheckListActivity() {
        super();
    }
}