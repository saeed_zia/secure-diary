package com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020KJ\u0010\u0010L\u001a\u00020I2\u0006\u0010M\u001a\u00020KH\u0016J\u0006\u0010N\u001a\u00020IJ\u0006\u0010O\u001a\u00020IJ\u0010\u0010P\u001a\u00020I2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010Q\u001a\u00020IJ\u0016\u0010R\u001a\u00020I2\u0006\u0010S\u001a\u0002042\u0006\u0010T\u001a\u000204J\u0006\u0010U\u001a\u00020IJ\u0006\u0010V\u001a\u00020IJ\b\u0010W\u001a\u00020IH\u0016J\u0012\u0010X\u001a\u00020I2\b\u0010Y\u001a\u0004\u0018\u00010ZH\u0014J\b\u0010[\u001a\u00020IH\u0014J\u0010\u0010\\\u001a\u00020I2\u0006\u0010]\u001a\u00020^H\u0007J\b\u0010_\u001a\u00020IH\u0014J\b\u0010`\u001a\u00020IH\u0014J\b\u0010a\u001a\u00020IH\u0014J\u0016\u0010b\u001a\u00020I2\u0006\u0010c\u001a\u00020(2\u0006\u0010J\u001a\u00020KJ\u001a\u0010d\u001a\u00020I2\b\u0010B\u001a\u0004\u0018\u00010C2\u0006\u0010\u0005\u001a\u00020eH\u0002J\u0016\u0010f\u001a\u00020I2\u0006\u0010g\u001a\u00020K2\u0006\u0010M\u001a\u00020KJ\u0018\u0010h\u001a\u00020I2\u0006\u0010g\u001a\u00020K2\u0006\u0010M\u001a\u00020KH\u0016J\u0006\u0010i\u001a\u00020IJ\u0006\u0010j\u001a\u00020IJ\u0006\u0010k\u001a\u00020IJ\u0016\u0010l\u001a\u00020I2\f\u0010m\u001a\b\u0012\u0004\u0012\u00020o0nH\u0016J\b\u0010p\u001a\u00020IH\u0002J\b\u0010q\u001a\u00020IH\u0002J\u0006\u0010r\u001a\u00020IJ\u0006\u0010s\u001a\u00020IJ\u0006\u0010t\u001a\u00020IR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0017\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R \u0010&\u001a\b\u0012\u0004\u0012\u00020(0\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001c\u0010-\u001a\u0004\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u001c\u00103\u001a\u0004\u0018\u000104X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u00106\"\u0004\b7\u00108R\u001c\u00109\u001a\u0004\u0018\u000104X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u00106\"\u0004\b;\u00108R\u001c\u0010<\u001a\u0004\u0018\u00010=X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010?\"\u0004\b@\u0010AR\u001c\u0010B\u001a\u0004\u0018\u00010CX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010G\u00a8\u0006u"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$View;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/TrashListeners;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "adapter", "Lcom/diary/with/lock/myjournal/notepad/adapters/TrashAdapter;", "getAdapter", "()Lcom/diary/with/lock/myjournal/notepad/adapters/TrashAdapter;", "setAdapter", "(Lcom/diary/with/lock/myjournal/notepad/adapters/TrashAdapter;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "noteDataList", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "getNoteDataList", "()Ljava/util/ArrayList;", "setNoteDataList", "(Ljava/util/ArrayList;)V", "presenter", "Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashPresenter;", "getPresenter", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashPresenter;", "setPresenter", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashPresenter;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "deleteNote", "", "positon", "", "deleteNoteListener", "position", "fetchNoteList", "hideNativeAnim", "inflateAd", "loadInterAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onResume", "onStart", "onStop", "openPopup", "note", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "restoreNote", "id", "restoreNoteListener", "setSetting", "showAdmobNativeAd1", "showAdmobNativeAd2", "showAllData", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "showFacebookNative1", "showFacebookNative2", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "app_debug"})
public final class TrashActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.View, com.diary.with.lock.myjournal.notepad.interfaces.TrashListeners {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.adapters.TrashAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> noteDataList;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashPresenter presenter;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.adapters.TrashAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.adapters.TrashAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> getNoteDataList() {
        return null;
    }
    
    public final void setNoteDataList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashPresenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashPresenter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public final void openPopup(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note, int positon) {
    }
    
    @java.lang.Override()
    public void showAllData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    public final void fetchNoteList() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void deleteNote(int positon) {
    }
    
    public final void restoreNote(int id, int position) {
    }
    
    public final void setSetting() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    public final void loadInterAd() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    @java.lang.Override()
    public void deleteNoteListener(int position) {
    }
    
    @java.lang.Override()
    public void restoreNoteListener(int id, int position) {
    }
    
    public TrashActivity() {
        super();
    }
}