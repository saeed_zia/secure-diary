package com.diary.with.lock.myjournal.notepad.views.activities.Tutorials;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010=\u001a\u00020>J\u0006\u0010?\u001a\u00020>J\u0010\u0010@\u001a\u00020>2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0006\u0010A\u001a\u00020>J\u0016\u0010B\u001a\u00020>2\u0006\u0010C\u001a\u00020&2\u0006\u0010D\u001a\u00020&J\u0006\u0010E\u001a\u00020>J\u0006\u0010F\u001a\u00020>J\b\u0010G\u001a\u00020>H\u0016J\u0012\u0010H\u001a\u00020>2\b\u0010I\u001a\u0004\u0018\u00010JH\u0014J\u0010\u0010K\u001a\u00020>2\u0006\u0010L\u001a\u00020MH\u0007J\b\u0010N\u001a\u00020>H\u0014J\b\u0010O\u001a\u00020>H\u0014J\b\u0010P\u001a\u00020>H\u0014J\u0006\u0010Q\u001a\u00020>J\u001a\u0010R\u001a\u00020>2\b\u00107\u001a\u0004\u0018\u0001082\u0006\u0010\u0003\u001a\u00020SH\u0002J\u0006\u0010T\u001a\u00020>J\u0006\u0010U\u001a\u00020>J\b\u0010V\u001a\u00020>H\u0002J\b\u0010W\u001a\u00020>H\u0002J\u0006\u0010X\u001a\u00020>J\u0006\u0010Y\u001a\u00020>J\u0006\u0010Z\u001a\u00020>R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001c\u0010\u001f\u001a\u0004\u0018\u00010 X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001c\u0010%\u001a\u0004\u0018\u00010&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010(\"\u0004\b)\u0010*R\u001c\u0010+\u001a\u0004\u0018\u00010&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010(\"\u0004\b-\u0010*R\u001c\u0010.\u001a\u0004\u0018\u00010/X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u00101\"\u0004\b2\u00103R\u001a\u00104\u001a\u00020&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010(\"\u0004\b6\u0010*R\u001c\u00107\u001a\u0004\u0018\u000108X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<\u00a8\u0006["}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/Tutorials/TutorialActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "currentItem", "", "getCurrentItem", "()Ljava/lang/Integer;", "setCurrentItem", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "source", "getSource", "setSource", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "goToNextActivity", "", "hideNativeAnim", "inflateAd", "loadInterAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onResume", "onStart", "onStop", "openNextPage", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "showAdmobNativeAd1", "showAdmobNativeAd2", "showFacebookNative1", "showFacebookNative2", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "app_debug"})
public final class TutorialActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String source = "";
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer currentItem;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSource() {
        return null;
    }
    
    public final void setSource(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCurrentItem() {
        return null;
    }
    
    public final void setCurrentItem(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void openNextPage() {
    }
    
    public final void goToNextActivity() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    public final void loadInterAd() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public TutorialActivity() {
        super();
    }
}