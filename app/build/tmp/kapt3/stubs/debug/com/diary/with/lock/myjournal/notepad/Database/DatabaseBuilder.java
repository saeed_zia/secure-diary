package com.diary.with.lock.myjournal.notepad.Database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0016\u0010\f\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Database/DatabaseBuilder;", "", "()V", "BACKUP_INSTANCE", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "INSTANCE", "buildBackupRoomDB", "context", "Landroid/content/Context;", "file", "Ljava/io/File;", "buildRoomDB", "getBackupInstance", "getInstance", "app_debug"})
public final class DatabaseBuilder {
    private static com.diary.with.lock.myjournal.notepad.Database.NoteDatabase INSTANCE;
    private static com.diary.with.lock.myjournal.notepad.Database.NoteDatabase BACKUP_INSTANCE;
    public static final com.diary.with.lock.myjournal.notepad.Database.DatabaseBuilder INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.Database.NoteDatabase getInstance(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    private final com.diary.with.lock.myjournal.notepad.Database.NoteDatabase buildRoomDB(android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.Database.NoteDatabase getBackupInstance(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.io.File file) {
        return null;
    }
    
    private final com.diary.with.lock.myjournal.notepad.Database.NoteDatabase buildBackupRoomDB(android.content.Context context, java.io.File file) {
        return null;
    }
    
    private DatabaseBuilder() {
        super();
    }
}