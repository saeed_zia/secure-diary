package com.diary.with.lock.myjournal.notepad.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B5\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020 H\u0016J\u0018\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u00022\u0006\u0010$\u001a\u00020 H\u0016J\u0018\u0010%\u001a\u00020\u00022\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020 H\u0016J\u001e\u0010)\u001a\u00020\"2\u0006\u0010*\u001a\u00020\b2\u0006\u0010+\u001a\u00020 2\u0006\u0010,\u001a\u00020-J\u001e\u0010.\u001a\u00020\"2\u0016\u0010/\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R*\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0010\"\u0004\b\u0019\u0010\u0012R \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0014\"\u0004\b\u001c\u0010\u0016\u00a8\u00060"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/ListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/diary/with/lock/myjournal/notepad/views/ViewHolder/NoteViewHolder;", "Landroid/widget/Filterable;", "context", "Landroid/content/Context;", "list", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "Lkotlin/collections/ArrayList;", "cellClickListener", "Lcom/diary/with/lock/myjournal/notepad/interfaces/CellClickListener;", "longClickListener", "Lcom/diary/with/lock/myjournal/notepad/interfaces/LongClickListener;", "(Landroid/content/Context;Ljava/util/ArrayList;Lcom/diary/with/lock/myjournal/notepad/interfaces/CellClickListener;Lcom/diary/with/lock/myjournal/notepad/interfaces/LongClickListener;)V", "getContext", "()Landroid/content/Context;", "setContext", "(Landroid/content/Context;)V", "getList", "()Ljava/util/ArrayList;", "setList", "(Ljava/util/ArrayList;)V", "mContext", "getMContext", "setMContext", "noteFilterList", "getNoteFilterList", "setNoteFilterList", "getFilter", "Landroid/widget/Filter;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "openPopup", "note", "positon", "view", "Landroid/view/View;", "updateUsers", "newUsers", "app_debug"})
public final class ListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.diary.with.lock.myjournal.notepad.views.ViewHolder.NoteViewHolder> implements android.widget.Filterable {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> noteFilterList;
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContext;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> list;
    private final com.diary.with.lock.myjournal.notepad.interfaces.CellClickListener cellClickListener = null;
    private final com.diary.with.lock.myjournal.notepad.interfaces.LongClickListener longClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> getNoteFilterList() {
        return null;
    }
    
    public final void setNoteFilterList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.diary.with.lock.myjournal.notepad.views.ViewHolder.NoteViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.ViewHolder.NoteViewHolder holder, int position) {
    }
    
    public final void openPopup(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note, int positon, @org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void updateUsers(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> newUsers) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.widget.Filter getFilter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> p0) {
    }
    
    public ListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> list, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.CellClickListener cellClickListener, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.LongClickListener longClickListener) {
        super();
    }
}