package com.diary.with.lock.myjournal.notepad.Ads;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Ads/FacebookAds;", "", "()V", "Companion", "app_debug"})
public final class FacebookAds {
    @org.jetbrains.annotations.Nullable()
    private static com.facebook.ads.InterstitialAd interstitialAd;
    public static final com.diary.with.lock.myjournal.notepad.Ads.FacebookAds.Companion Companion = null;
    
    public FacebookAds() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Ads/FacebookAds$Companion;", "", "()V", "interstitialAd", "Lcom/facebook/ads/InterstitialAd;", "getInterstitialAd", "()Lcom/facebook/ads/InterstitialAd;", "setInterstitialAd", "(Lcom/facebook/ads/InterstitialAd;)V", "facebookInterstitialAds", "", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.facebook.ads.InterstitialAd getInterstitialAd() {
            return null;
        }
        
        public final void setInterstitialAd(@org.jetbrains.annotations.Nullable()
        com.facebook.ads.InterstitialAd p0) {
        }
        
        public final void facebookInterstitialAds(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}