package com.diary.with.lock.myjournal.notepad.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\"B-\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u0019\u001a\u00020\u0007H\u0016J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\u0018\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0007H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006#"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter$ItemHolder;", "context", "Landroid/content/Context;", "arrayList", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "frameClickListener", "Lcom/diary/with/lock/myjournal/notepad/interfaces/FrameClickListener;", "(Landroid/content/Context;Ljava/util/ArrayList;Lcom/diary/with/lock/myjournal/notepad/interfaces/FrameClickListener;)V", "getContext", "()Landroid/content/Context;", "setContext", "(Landroid/content/Context;)V", "getFrameClickListener", "()Lcom/diary/with/lock/myjournal/notepad/interfaces/FrameClickListener;", "setFrameClickListener", "(Lcom/diary/with/lock/myjournal/notepad/interfaces/FrameClickListener;)V", "marrayList", "getMarrayList", "()Ljava/util/ArrayList;", "setMarrayList", "(Ljava/util/ArrayList;)V", "getItemCount", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ItemHolder", "app_debug"})
public final class QuotationFrameAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter.ItemHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.Integer> marrayList;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    @org.jetbrains.annotations.NotNull()
    private com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener frameClickListener;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Integer> getMarrayList() {
        return null;
    }
    
    public final void setMarrayList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter.ItemHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter.ItemHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener getFrameClickListener() {
        return null;
    }
    
    public final void setFrameClickListener(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener p0) {
    }
    
    public QuotationFrameAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Integer> arrayList, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener frameClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/QuotationFrameAdapter$ItemHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindItems", "", "imageId", "", "app_debug"})
    public static final class ItemHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bindItems(int imageId) {
        }
        
        public ItemHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}