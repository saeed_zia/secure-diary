package com.diary.with.lock.myjournal.notepad.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0005H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0005H&\u00a8\u0006\n"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/interfaces/QuotationListeners;", "", "onCellClickListener", "", "quoteText", "", "onCopyClickListener", "author", "quote", "onShareClickListener", "app_debug"})
public abstract interface QuotationListeners {
    
    public abstract void onCopyClickListener(@org.jetbrains.annotations.NotNull()
    java.lang.String author, @org.jetbrains.annotations.NotNull()
    java.lang.String quote);
    
    public abstract void onShareClickListener(@org.jetbrains.annotations.NotNull()
    java.lang.String author, @org.jetbrains.annotations.NotNull()
    java.lang.String quote);
    
    public abstract void onCellClickListener(@org.jetbrains.annotations.NotNull()
    java.lang.String quoteText);
}