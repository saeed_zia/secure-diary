package com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010K\u001a\u00020LJ\u0006\u0010M\u001a\u00020LJ\u0010\u0010N\u001a\u00020L2\u0006\u0010O\u001a\u00020.H\u0002J\u0010\u0010P\u001a\u00020L2\u0006\u0010Q\u001a\u00020.H\u0002J\u0006\u0010R\u001a\u00020LJ\u0010\u0010S\u001a\u00020L2\u0006\u0010 \u001a\u00020!H\u0002J\u0016\u0010T\u001a\u00020L2\u0006\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020:J\u0006\u0010W\u001a\u00020LJ\u0006\u0010X\u001a\u00020LJ\u000e\u0010Y\u001a\u00020L2\u0006\u0010Z\u001a\u00020[J\u0010\u0010\\\u001a\u00020L2\u0006\u0010]\u001a\u00020[H\u0016J\u0010\u0010^\u001a\u00020L2\u0006\u0010_\u001a\u00020.H\u0016J\u0012\u0010`\u001a\u00020L2\b\u0010a\u001a\u0004\u0018\u00010bH\u0014J\u0010\u0010c\u001a\u00020L2\u0006\u0010d\u001a\u00020eH\u0007J\b\u0010f\u001a\u00020LH\u0014J\b\u0010g\u001a\u00020LH\u0014J\b\u0010h\u001a\u00020LH\u0014J\b\u0010i\u001a\u00020LH\u0014J\u0016\u0010j\u001a\u00020L2\u0006\u0010O\u001a\u00020.2\u0006\u0010Z\u001a\u00020[J\u001a\u0010k\u001a\u00020L2\b\u0010E\u001a\u0004\u0018\u00010F2\u0006\u0010\u0006\u001a\u00020lH\u0002J\u0006\u0010m\u001a\u00020LJ\u000e\u0010n\u001a\u00020L2\u0006\u0010O\u001a\u00020.J\u0010\u0010o\u001a\u00020L2\u0006\u0010O\u001a\u00020.H\u0016J\u0006\u0010p\u001a\u00020LJ\u0006\u0010q\u001a\u00020LJ\u0016\u0010r\u001a\u00020L2\f\u0010s\u001a\b\u0012\u0004\u0012\u00020u0tH\u0016J\b\u0010v\u001a\u00020LH\u0002J\b\u0010w\u001a\u00020LH\u0002J\u0006\u0010x\u001a\u00020LJ\u0006\u0010y\u001a\u00020LJ\u0006\u0010z\u001a\u00020LJ\u0006\u0010{\u001a\u00020LR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001e\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\"\u0010\u0019\u001a\n \u001b*\u0004\u0018\u00010\u001a0\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R \u0010,\u001a\b\u0012\u0004\u0012\u00020.0-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u001c\u00103\u001a\u0004\u0018\u000104X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u00106\"\u0004\b7\u00108R\u001c\u00109\u001a\u0004\u0018\u00010:X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>R\u001c\u0010?\u001a\u0004\u0018\u00010@X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010B\"\u0004\bC\u0010DR\u001c\u0010E\u001a\u0004\u0018\u00010FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010H\"\u0004\bI\u0010J\u00a8\u0006|"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/CellClickListener;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterContract$View;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/LongClickListener;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "adapter", "Lcom/diary/with/lock/myjournal/notepad/adapters/ListAdapter;", "getAdapter", "()Lcom/diary/with/lock/myjournal/notepad/adapters/ListAdapter;", "setAdapter", "(Lcom/diary/with/lock/myjournal/notepad/adapters/ListAdapter;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "cal", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "getCal", "()Ljava/util/Calendar;", "setCal", "(Ljava/util/Calendar;)V", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "noteDataList", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "getNoteDataList", "()Ljava/util/ArrayList;", "setNoteDataList", "(Ljava/util/ArrayList;)V", "presenter", "Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterPresenter;", "getPresenter", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterPresenter;", "setPresenter", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/DiaryWriterActivity/DiaryWriterPresenter;)V", "selectedNative", "", "getSelectedNative", "()Ljava/lang/String;", "setSelectedNative", "(Ljava/lang/String;)V", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "clickListeners", "", "fetchNoteList", "goToCheckListActivity", "note", "goToWriteNoteActivity", "noteModel", "hideNativeAnim", "inflateAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "moveToTrash", "positon", "", "moveToTrashclickListener", "position", "onCellClickListener", "data", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onRestart", "onResume", "onStart", "onStop", "openPopup", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "setSetting", "shareText", "shareTextClickListener", "showAdmobNativeAd1", "showAdmobNativeAd2", "showAllNoteData", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "showFacebookNative1", "showFacebookNative2", "showNativeAd", "sortAlphabatically", "sortLatestFirst", "sortOldestFirst", "app_debug"})
public final class DiaryWriterActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.diary.with.lock.myjournal.notepad.interfaces.CellClickListener, com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterContract.View, com.diary.with.lock.myjournal.notepad.interfaces.LongClickListener {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterPresenter presenter;
    private java.util.Calendar cal;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.adapters.ListAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> noteDataList;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterPresenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterPresenter p0) {
    }
    
    public final java.util.Calendar getCal() {
        return null;
    }
    
    public final void setCal(java.util.Calendar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.adapters.ListAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.adapters.ListAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> getNoteDataList() {
        return null;
    }
    
    public final void setNoteDataList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.NoteModel> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void sortLatestFirst() {
    }
    
    public final void sortOldestFirst() {
    }
    
    public final void sortAlphabatically() {
    }
    
    @java.lang.Override()
    protected void onRestart() {
    }
    
    public final void setSetting() {
    }
    
    public final void clickListeners() {
    }
    
    public final void moveToTrash(int positon) {
    }
    
    @java.lang.Override()
    public void showAllNoteData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    private final void goToWriteNoteActivity(com.diary.with.lock.myjournal.notepad.models.NoteModel noteModel) {
    }
    
    private final void goToCheckListActivity(com.diary.with.lock.myjournal.notepad.models.NoteModel note) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void fetchNoteList() {
    }
    
    @java.lang.Override()
    public void onCellClickListener(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel data) {
    }
    
    public final void openPopup(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note, int positon) {
    }
    
    public final void shareText(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note) {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    public void shareTextClickListener(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note) {
    }
    
    @java.lang.Override()
    public void moveToTrashclickListener(int position) {
    }
    
    public DiaryWriterActivity() {
        super();
    }
}