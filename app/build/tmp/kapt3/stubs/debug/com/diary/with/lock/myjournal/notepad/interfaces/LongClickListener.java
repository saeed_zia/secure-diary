package com.diary.with.lock.myjournal.notepad.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&\u00a8\u0006\t"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/interfaces/LongClickListener;", "", "moveToTrashclickListener", "", "position", "", "shareTextClickListener", "note", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "app_debug"})
public abstract interface LongClickListener {
    
    public abstract void shareTextClickListener(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.NoteModel note);
    
    public abstract void moveToTrashclickListener(int position);
}