package com.diary.with.lock.myjournal.notepad.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0016\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseAdClass;", "", "()V", "Companion", "app_release"})
public class BaseAdClass {
    @org.jetbrains.annotations.Nullable()
    private static com.google.android.gms.ads.formats.UnifiedNativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String adStatus;
    public static final com.diary.with.lock.myjournal.notepad.views.activities.BaseAdClass.Companion Companion = null;
    
    public BaseAdClass() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u0013"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseAdClass$Companion;", "", "()V", "adStatus", "", "getAdStatus", "()Ljava/lang/String;", "setAdStatus", "(Ljava/lang/String;)V", "nativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "loadAds", "", "context", "Landroid/content/Context;", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.android.gms.ads.formats.UnifiedNativeAd getNativeAd() {
            return null;
        }
        
        public final void setNativeAd(@org.jetbrains.annotations.Nullable()
        com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAdStatus() {
            return null;
        }
        
        public final void setAdStatus(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public final void loadAds(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}