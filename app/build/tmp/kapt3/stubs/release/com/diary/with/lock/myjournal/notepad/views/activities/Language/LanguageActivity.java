package com.diary.with.lock.myjournal.notepad.views.activities.Language;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0006\u0010;\u001a\u00020<J\u0006\u0010=\u001a\u00020<J\u0010\u0010>\u001a\u00020<2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010?\u001a\u00020<J\u0016\u0010@\u001a\u00020<2\u0006\u0010A\u001a\u00020\'2\u0006\u0010B\u001a\u00020\'J\u0006\u0010C\u001a\u00020<J\u0006\u0010D\u001a\u00020<J\b\u0010E\u001a\u00020<H\u0016J\u0012\u0010F\u001a\u00020<2\b\u0010G\u001a\u0004\u0018\u00010HH\u0014J\u0010\u0010I\u001a\u00020<2\u0006\u0010J\u001a\u00020KH\u0007J\u0010\u0010L\u001a\u00020<2\u0006\u0010M\u001a\u00020\u0015H\u0016J\b\u0010N\u001a\u00020<H\u0014J\b\u0010O\u001a\u00020<H\u0014J\b\u0010P\u001a\u00020<H\u0014J\u001a\u0010Q\u001a\u00020<2\b\u00105\u001a\u0004\u0018\u0001062\u0006\u0010\u0004\u001a\u00020RH\u0002J\u0006\u0010S\u001a\u00020<J\u0006\u0010T\u001a\u00020<J\u0006\u0010U\u001a\u00020<J\u000e\u0010V\u001a\u00020<2\u0006\u0010M\u001a\u00020\u0015J\b\u0010W\u001a\u00020<H\u0002J\b\u0010X\u001a\u00020<H\u0002J\u0006\u0010Y\u001a\u00020<J\u0006\u0010Z\u001a\u00020<J\u0006\u0010[\u001a\u00020<J\u000e\u0010\\\u001a\u00020<2\u0006\u0010]\u001a\u00020^R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001e\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010)\"\u0004\b.\u0010+R\u001c\u0010/\u001a\u0004\u0018\u000100X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u00102\"\u0004\b3\u00104R\u001c\u00105\u001a\u0004\u0018\u000106X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u00108\"\u0004\b9\u0010:\u00a8\u0006_"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/Language/LanguageActivity;", "Lcom/diary/with/lock/myjournal/notepad/interfaces/LanguageClickListener;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "language", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/Language;", "getLanguage", "()Ljava/util/ArrayList;", "setLanguage", "(Ljava/util/ArrayList;)V", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "addLanguages", "", "hideNativeAnim", "inflateAd", "loadInterAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onLanguageClickListener", "lang", "onResume", "onStart", "onStop", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "setSetting", "showAdmobNativeAd1", "showAdmobNativeAd2", "showAlertDialog", "showFacebookNative1", "showFacebookNative2", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "triggerRebirth", "context", "Landroid/content/Context;", "app_release"})
public final class LanguageActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.diary.with.lock.myjournal.notepad.interfaces.LanguageClickListener {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.Language> language;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.Language> getLanguage() {
        return null;
    }
    
    public final void setLanguage(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.Language> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void loadInterAd() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    public final void addLanguages() {
    }
    
    @java.lang.Override()
    public void onLanguageClickListener(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.Language lang) {
    }
    
    public final void showAlertDialog(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.models.Language lang) {
    }
    
    public final void triggerRebirth(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void setSetting() {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    public LanguageActivity() {
        super();
    }
}