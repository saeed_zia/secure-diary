package com.diary.with.lock.myjournal.notepad.views.activities.Dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00ac\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010R\u001a\u00020SJ\u0006\u0010T\u001a\u00020SJ\u0006\u0010U\u001a\u00020SJ\u0010\u0010V\u001a\u00020S2\u0006\u0010+\u001a\u00020,H\u0002J\u0006\u0010W\u001a\u00020SJ\u0016\u0010X\u001a\u00020S2\u0006\u0010Y\u001a\u00020#2\u0006\u0010Z\u001a\u00020#J\u0006\u0010[\u001a\u00020SJ\u0006\u0010\\\u001a\u00020SJ\b\u0010]\u001a\u00020SH\u0016J\u001a\u0010^\u001a\u00020S2\u0006\u0010_\u001a\u00020`2\b\u0010a\u001a\u0004\u0018\u00010bH\u0016J\b\u0010c\u001a\u00020SH\u0016J\u0012\u0010d\u001a\u00020S2\b\u0010e\u001a\u0004\u0018\u00010fH\u0014J\b\u0010g\u001a\u00020SH\u0014J\u0010\u0010h\u001a\u00020S2\u0006\u0010i\u001a\u00020jH\u0007J\u001a\u0010k\u001a\u00020S2\u0006\u0010l\u001a\u00020#2\b\u0010m\u001a\u0004\u0018\u00010nH\u0016J\b\u0010o\u001a\u00020SH\u0016J\b\u0010p\u001a\u00020SH\u0014J\b\u0010q\u001a\u00020SH\u0014J\b\u0010r\u001a\u00020SH\u0014J\u001a\u0010s\u001a\u00020S2\b\u0010L\u001a\u0004\u0018\u00010M2\u0006\u0010\u0005\u001a\u00020tH\u0002J\u0006\u0010u\u001a\u00020SJ\u0006\u0010v\u001a\u00020SJ\u0006\u0010w\u001a\u00020SJ\u0006\u0010x\u001a\u00020SJ\u0006\u0010y\u001a\u00020SJ\u0006\u0010z\u001a\u00020SJ\u0006\u0010{\u001a\u00020SJ\u0016\u0010|\u001a\u00020S2\f\u0010}\u001a\b\u0012\u0004\u0012\u00020\u007f0~H\u0016J\t\u0010\u0080\u0001\u001a\u00020SH\u0002J\t\u0010\u0081\u0001\u001a\u00020SH\u0002J\t\u0010\u0082\u0001\u001a\u00020SH\u0002J\u0007\u0010\u0083\u0001\u001a\u00020SJ\u0007\u0010\u0084\u0001\u001a\u00020SJ\u0007\u0010\u0085\u0001\u001a\u00020SJ\u0007\u0010\u0086\u0001\u001a\u00020SJ\t\u0010\u0087\u0001\u001a\u00020SH\u0002R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001e\u0010 \u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b \u0010\u000e\"\u0004\b!\u0010\u0010R\u001c\u0010\"\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\u001c\u0010(\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010%\"\u0004\b*\u0010\'R\u001c\u0010+\u001a\u0004\u0018\u00010,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u001c\u00101\u001a\u0004\u0018\u000102X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u001c\u00107\u001a\u0004\u0018\u000108X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010:\"\u0004\b;\u0010<R\u001e\u0010=\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0011\u001a\u0004\b>\u0010\u000e\"\u0004\b?\u0010\u0010R\u001c\u0010@\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010%\"\u0004\bB\u0010\'R\u001c\u0010C\u001a\u0004\u0018\u00010#X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010%\"\u0004\bE\u0010\'R\u001c\u0010F\u001a\u0004\u0018\u00010GX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010I\"\u0004\bJ\u0010KR\u001c\u0010L\u001a\u0004\u0018\u00010MX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010O\"\u0004\bP\u0010Q\u00a8\u0006\u0088\u0001"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardContract$View;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "bottomSheet", "Lcom/diary/with/lock/myjournal/notepad/views/fragments/BottomSheetFragment;", "getBottomSheet", "()Lcom/diary/with/lock/myjournal/notepad/views/fragments/BottomSheetFragment;", "setBottomSheet", "(Lcom/diary/with/lock/myjournal/notepad/views/fragments/BottomSheetFragment;)V", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "isExitShown", "setExitShown", "mIdNumber", "", "getMIdNumber", "()Ljava/lang/String;", "setMIdNumber", "(Ljava/lang/String;)V", "mIdType", "getMIdType", "setMIdType", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "presenter", "Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardPresenter;", "getPresenter", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardPresenter;", "setPresenter", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardPresenter;)V", "requestFromResume", "getRequestFromResume", "setRequestFromResume", "selectedInter", "getSelectedInter", "setSelectedInter", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "clickListeners", "", "fetchNoteListFromBackup", "hideNativeAnim", "inflateAd", "loadInterAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "onBackPressed", "onBillingError", "errorCode", "", "error", "", "onBillingInitialized", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onProductPurchased", "productId", "details", "Lcom/anjlab/android/iab/v3/TransactionDetails;", "onPurchaseHistoryRestored", "onResume", "onStart", "onStop", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "requestStoragePermissions", "restoreData", "setSetting", "shareApp", "showAdmobNativeAd1", "showAdmobNativeAd2", "showAlertDialog", "showAllNoteData", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "showDialog", "showFacebookNative1", "showFacebookNative2", "showInterstitialAd", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "showStoragePermissionDialog", "app_release"})
public final class DashboardActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler, com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.View {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.fragments.BottomSheetFragment bottomSheet;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isExitShown = false;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardPresenter presenter;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean requestFromResume;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String mIdType = "first";
    @org.jetbrains.annotations.Nullable()
    private java.lang.String mIdNumber;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.fragments.BottomSheetFragment getBottomSheet() {
        return null;
    }
    
    public final void setBottomSheet(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.fragments.BottomSheetFragment p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isExitShown() {
        return null;
    }
    
    public final void setExitShown(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardPresenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardPresenter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getRequestFromResume() {
        return null;
    }
    
    public final void setRequestFromResume(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMIdType() {
        return null;
    }
    
    public final void setMIdType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMIdNumber() {
        return null;
    }
    
    public final void setMIdNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showAlertDialog() {
    }
    
    private final void showDialog() {
    }
    
    public final void clickListeners() {
    }
    
    public final void restoreData() {
    }
    
    private final void showStoragePermissionDialog() {
    }
    
    public final void fetchNoteListFromBackup() {
    }
    
    public final void requestStoragePermissions() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    public final void shareApp() {
    }
    
    public final void setSetting() {
    }
    
    @java.lang.Override()
    public void onBillingInitialized() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onPurchaseHistoryRestored() {
    }
    
    @java.lang.Override()
    public void onProductPurchased(@org.jetbrains.annotations.NotNull()
    java.lang.String productId, @org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.TransactionDetails details) {
    }
    
    @java.lang.Override()
    public void onBillingError(int errorCode, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable error) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    public void showAllNoteData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    public final void showInterstitialAd() {
    }
    
    public final void loadInterAd() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public DashboardActivity() {
        super();
    }
}