package com.diary.with.lock.myjournal.notepad.Ads;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0016\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Ads/FacebookNativeAd;", "", "()V", "Companion", "app_release"})
public class FacebookNativeAd {
    @org.jetbrains.annotations.Nullable()
    private static com.facebook.ads.NativeAd nativeAd1;
    @org.jetbrains.annotations.Nullable()
    private static com.facebook.ads.NativeAd nativeAd2;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String adStatus1;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String adStatus2;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String selectedAd;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String idTye;
    @org.jetbrains.annotations.Nullable()
    private static java.lang.String idNumber;
    public static final com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd.Companion Companion = null;
    
    public FacebookNativeAd() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001c\u0010\f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0015\"\u0004\b\u001a\u0010\u0017R\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\b\u00a8\u0006#"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Ads/FacebookNativeAd$Companion;", "", "()V", "adStatus1", "", "getAdStatus1", "()Ljava/lang/String;", "setAdStatus1", "(Ljava/lang/String;)V", "adStatus2", "getAdStatus2", "setAdStatus2", "idNumber", "getIdNumber", "setIdNumber", "idTye", "getIdTye", "setIdTye", "nativeAd1", "Lcom/facebook/ads/NativeAd;", "getNativeAd1", "()Lcom/facebook/ads/NativeAd;", "setNativeAd1", "(Lcom/facebook/ads/NativeAd;)V", "nativeAd2", "getNativeAd2", "setNativeAd2", "selectedAd", "getSelectedAd", "setSelectedAd", "loadNativeFacebookAd1", "", "context", "Landroid/content/Context;", "loadNativeFacebookAd2", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.facebook.ads.NativeAd getNativeAd1() {
            return null;
        }
        
        public final void setNativeAd1(@org.jetbrains.annotations.Nullable()
        com.facebook.ads.NativeAd p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.facebook.ads.NativeAd getNativeAd2() {
            return null;
        }
        
        public final void setNativeAd2(@org.jetbrains.annotations.Nullable()
        com.facebook.ads.NativeAd p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAdStatus1() {
            return null;
        }
        
        public final void setAdStatus1(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAdStatus2() {
            return null;
        }
        
        public final void setAdStatus2(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSelectedAd() {
            return null;
        }
        
        public final void setSelectedAd(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIdTye() {
            return null;
        }
        
        public final void setIdTye(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIdNumber() {
            return null;
        }
        
        public final void setIdNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public final void loadNativeFacebookAd1(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void loadNativeFacebookAd2(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        private Companion() {
            super();
        }
    }
}