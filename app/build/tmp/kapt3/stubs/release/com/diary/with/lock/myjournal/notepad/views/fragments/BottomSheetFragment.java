package com.diary.with.lock.myjournal.notepad.views.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010-\u001a\u00020.J\u0010\u0010/\u001a\u00020.2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0006\u00100\u001a\u00020.J\u0006\u00101\u001a\u00020.J*\u00102\u001a\u0004\u0018\u0001032\u0006\u00104\u001a\u0002052\n\b\u0001\u00106\u001a\u0004\u0018\u0001072\n\b\u0001\u00108\u001a\u0004\u0018\u000109H\u0016J\u0010\u0010:\u001a\u00020.2\u0006\u0010;\u001a\u00020<H\u0007J\b\u0010=\u001a\u00020.H\u0016J\b\u0010>\u001a\u00020.H\u0016J\u001a\u0010?\u001a\u00020.2\u0006\u0010@\u001a\u0002032\b\u00108\u001a\u0004\u0018\u000109H\u0016J\u001a\u0010A\u001a\u00020.2\b\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010\u0003\u001a\u00020BH\u0002J\u0006\u0010C\u001a\u00020.J\u0006\u0010D\u001a\u00020.J\u0006\u0010E\u001a\u00020.J\b\u0010F\u001a\u00020.H\u0002J\b\u0010G\u001a\u00020.H\u0002J\u0006\u0010H\u001a\u00020.R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001c\u0010!\u001a\u0004\u0018\u00010\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u001c\u0010\'\u001a\u0004\u0018\u00010(X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,\u00a8\u0006I"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/fragments/BottomSheetFragment;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Z", "setAddStatus", "(Z)V", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "selectedNative", "", "getSelectedNative", "()Ljava/lang/String;", "setSelectedNative", "(Ljava/lang/String;)V", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "hideNativeAnim", "", "inflateAd", "makeNonClickAbleAD", "makeNonClickAbleFB", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onStart", "onStop", "onViewCreated", "view", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "setSetting", "showAdmobNativeAd1", "showAdmobNativeAd2", "showFacebookNative1", "showFacebookNative2", "showNativeAd", "app_release"})
public final class BottomSheetFragment extends com.google.android.material.bottomsheet.BottomSheetDialogFragment {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    private boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getAddStatus() {
        return false;
    }
    
    public final void setAddStatus(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    @androidx.annotation.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    @androidx.annotation.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    public final void setSetting() {
    }
    
    public final void hideNativeAnim() {
    }
    
    private final void showFacebookNative1() {
    }
    
    private final void showFacebookNative2() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    public BottomSheetFragment() {
        super();
    }
}