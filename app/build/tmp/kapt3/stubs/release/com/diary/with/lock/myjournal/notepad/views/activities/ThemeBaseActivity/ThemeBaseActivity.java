package com.diary.with.lock.myjournal.notepad.views.activities.ThemeBaseActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014J\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000fR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0010"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/ThemeBaseActivity/ThemeBaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "sp", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSp", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSp", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "setMyTheme", "font", "", "app_release"})
public class ThemeBaseActivity extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sp;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSp() {
        return null;
    }
    
    public final void setSp(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setMyTheme(@org.jetbrains.annotations.NotNull()
    java.lang.String font) {
    }
    
    public ThemeBaseActivity() {
        super();
    }
}