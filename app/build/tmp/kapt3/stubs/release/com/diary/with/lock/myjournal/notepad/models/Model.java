package com.diary.with.lock.myjournal.notepad.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/models/Model;", "", "titleResId", "", "layoutResId", "(Ljava/lang/String;III)V", "getLayoutResId", "()I", "getTitleResId", "RED", "RED1", "RED2", "RED3", "RED4", "RED5", "RED6", "RED7", "RED8", "RED9", "RED10", "RED11", "app_release"})
public enum Model {
    /*public static final*/ RED /* = new RED(0, 0) */,
    /*public static final*/ RED1 /* = new RED1(0, 0) */,
    /*public static final*/ RED2 /* = new RED2(0, 0) */,
    /*public static final*/ RED3 /* = new RED3(0, 0) */,
    /*public static final*/ RED4 /* = new RED4(0, 0) */,
    /*public static final*/ RED5 /* = new RED5(0, 0) */,
    /*public static final*/ RED6 /* = new RED6(0, 0) */,
    /*public static final*/ RED7 /* = new RED7(0, 0) */,
    /*public static final*/ RED8 /* = new RED8(0, 0) */,
    /*public static final*/ RED9 /* = new RED9(0, 0) */,
    /*public static final*/ RED10 /* = new RED10(0, 0) */,
    /*public static final*/ RED11 /* = new RED11(0, 0) */;
    private final int titleResId = 0;
    private final int layoutResId = 0;
    
    public final int getTitleResId() {
        return 0;
    }
    
    public final int getLayoutResId() {
        return 0;
    }
    
    Model(int titleResId, int layoutResId) {
    }
}