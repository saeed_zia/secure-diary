package com.diary.with.lock.myjournal.notepad.Database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0010\u0010\u0005\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u0006H\u0007J\u001a\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\n"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/Database/CheckListTypeConverter;", "", "()V", "fromCheckList", "", "countryLang", "", "Lcom/diary/with/lock/myjournal/notepad/models/CheckListItem;", "toCheckList", "countryLangString", "app_release"})
public final class CheckListTypeConverter {
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String fromCheckList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.diary.with.lock.myjournal.notepad.models.CheckListItem> countryLang) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.util.List<com.diary.with.lock.myjournal.notepad.models.CheckListItem> toCheckList(@org.jetbrains.annotations.Nullable()
    java.lang.String countryLangString) {
        return null;
    }
    
    public CheckListTypeConverter() {
        super();
    }
}