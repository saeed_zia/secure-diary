package com.diary.with.lock.myjournal.notepad.views.activities.LockSetting;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u00103\u001a\u000204J\u0010\u00105\u001a\u0002042\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0006\u00106\u001a\u000204J\u0016\u00107\u001a\u0002042\u0006\u00108\u001a\u00020\u001f2\u0006\u00109\u001a\u00020\u001fJ\u0006\u0010:\u001a\u000204J\u0006\u0010;\u001a\u000204J\b\u0010<\u001a\u000204H\u0016J\u0012\u0010=\u001a\u0002042\b\u0010>\u001a\u0004\u0018\u00010?H\u0014J\u0010\u0010@\u001a\u0002042\u0006\u0010A\u001a\u00020BH\u0007J\b\u0010C\u001a\u000204H\u0014J\b\u0010D\u001a\u000204H\u0014J\b\u0010E\u001a\u000204H\u0014J\u001a\u0010F\u001a\u0002042\b\u0010-\u001a\u0004\u0018\u00010.2\u0006\u0010\u0003\u001a\u00020GH\u0002J\u0006\u0010H\u001a\u000204J\u0006\u0010I\u001a\u000204J\u0006\u0010J\u001a\u000204J\u0006\u0010K\u001a\u000204J\b\u0010L\u001a\u000204H\u0002J\u0006\u0010M\u001a\u000204J\u0006\u0010N\u001a\u000204J\u0006\u0010O\u001a\u000204J\u0006\u0010P\u001a\u000204R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001e\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000f\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001c\u0010$\u001a\u0004\u0018\u00010\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010!\"\u0004\b&\u0010#R\u001c\u0010\'\u001a\u0004\u0018\u00010(X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\u001c\u0010-\u001a\u0004\u0018\u00010.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102\u00a8\u0006Q"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/LockSetting/LockSettingActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "()V", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "hideNativeAnim", "", "inflateAd", "loadInterAd", "loadNewNativeAd", "type", "idNumber", "makeNonClickAbleAD", "makeNonClickAbleFB", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onEvent", "adloadStatus", "Lcom/diary/with/lock/myjournal/notepad/utils/AdLoadStatus;", "onResume", "onStart", "onStop", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "setClickListeners", "setSetting", "showAdmobNativeAd1", "showAdmobNativeAd2", "showFacebookNative1", "showFacebookNative2", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "app_release"})
public final class LockSettingActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setSetting() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public final void setClickListeners() {
    }
    
    public final void makeNonClickAbleAD() {
    }
    
    public final void makeNonClickAbleFB() {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    private final void showFacebookNative1() {
    }
    
    public final void showFacebookNative2() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void showAdmobNativeAd1() {
    }
    
    public final void showAdmobNativeAd2() {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public final void loadInterAd() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void onEvent(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus adloadStatus) {
    }
    
    public LockSettingActivity() {
        super();
    }
}