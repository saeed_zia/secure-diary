package com.diary.with.lock.myjournal.notepad.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b%\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u001c\n\u0002\u0010\u0007\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b_\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0011\u001a\u00020\u0006J\u0006\u0010\u0012\u001a\u00020\u0006J\u0006\u0010\u0013\u001a\u00020\u0006J\u0006\u0010\u0014\u001a\u00020\u0006J\u0006\u0010\u0015\u001a\u00020\bJ\u0006\u0010\u0016\u001a\u00020\u0006J\u0006\u0010\u0017\u001a\u00020\u0006J\u0006\u0010\u0018\u001a\u00020\u0006J\u0006\u0010\u0019\u001a\u00020\u0006J\u0006\u0010\u001a\u001a\u00020\u0006J\u0006\u0010\u001b\u001a\u00020\u0006J\u0006\u0010\u001c\u001a\u00020\u0006J\u0006\u0010\u001d\u001a\u00020\u0006J\u0006\u0010\u001e\u001a\u00020\u0006J\u0006\u0010\u001f\u001a\u00020\u0006J\u0006\u0010 \u001a\u00020\u0006J\u0006\u0010!\u001a\u00020\u0006J\u0006\u0010\"\u001a\u00020\u0006J\u0006\u0010#\u001a\u00020\u0006J\u0006\u0010$\u001a\u00020\u0006J\u0006\u0010%\u001a\u00020\u0006J\u0006\u0010&\u001a\u00020\u0006J\u0006\u0010\'\u001a\u00020\u0006J\u0006\u0010(\u001a\u00020\u0006J\u0006\u0010)\u001a\u00020\u0006J\u0006\u0010*\u001a\u00020\u0006J\u0006\u0010+\u001a\u00020\u0006J\u0006\u0010,\u001a\u00020\u0006J\u0006\u0010-\u001a\u00020\u0006J\u0006\u0010.\u001a\u00020\u0006J\u0006\u0010/\u001a\u00020\u0006J\u0006\u00100\u001a\u00020\u0006J\u0006\u00101\u001a\u00020\u0006J\u0006\u00102\u001a\u00020\u0006J\u0006\u00103\u001a\u000204J\u0006\u00105\u001a\u00020\u0006J\u0006\u00106\u001a\u00020\u0006J\u0006\u00107\u001a\u000208J\u0006\u00109\u001a\u000208J\u0006\u0010:\u001a\u000208J\u0006\u0010;\u001a\u000208J\u0006\u0010<\u001a\u000208J\u0006\u0010=\u001a\u000208J\u0006\u0010>\u001a\u000208J\u0006\u0010?\u001a\u000208J\u0006\u0010@\u001a\u000208J\u0006\u0010A\u001a\u000208J\u0006\u0010B\u001a\u000208J\u0006\u0010C\u001a\u000208J\u0006\u0010D\u001a\u000208J\u0006\u0010E\u001a\u000208J\u0006\u0010F\u001a\u00020\u0006J\u0006\u0010G\u001a\u00020\u0006J\u0006\u0010H\u001a\u00020\u0006J\u0006\u0010I\u001a\u00020\u0006J\u0006\u0010J\u001a\u00020\u0006J\u0006\u0010K\u001a\u00020\u0006J\u0006\u0010L\u001a\u00020\u0006J\u0006\u0010M\u001a\u00020\u0006J\u0006\u0010N\u001a\u00020\u0006J\u0006\u0010O\u001a\u00020\u0006J\u0006\u0010P\u001a\u00020\u0006J\u0006\u0010Q\u001a\u00020\u0006J\u0006\u0010R\u001a\u00020\u0006J\u0006\u0010S\u001a\u00020\u0006J\u0006\u0010T\u001a\u00020UJ\u0006\u0010V\u001a\u00020\u0006J\u0006\u0010W\u001a\u00020\u0006J\u0006\u0010X\u001a\u00020\u0006J\u0006\u0010Y\u001a\u00020\u0006J\u0006\u0010Z\u001a\u00020\u0006J\u0006\u0010[\u001a\u00020\u0006J\u0006\u0010\\\u001a\u00020\u0006J\u0006\u0010]\u001a\u00020\u0006J\u0006\u0010^\u001a\u00020\u0006J\u0006\u0010_\u001a\u00020\u0006J\u0006\u0010`\u001a\u000204J\u0006\u0010a\u001a\u00020\u0006J\u0006\u0010b\u001a\u00020\u0006J\u0006\u0010c\u001a\u00020\u0006J\u0006\u0010d\u001a\u000204J\u0006\u0010e\u001a\u000204J\u0006\u0010f\u001a\u000204J\u0006\u0010g\u001a\u000204J\u000e\u0010h\u001a\u00020i2\u0006\u0010j\u001a\u000204J\u0006\u0010k\u001a\u00020iJ\u000e\u0010l\u001a\u00020i2\u0006\u0010m\u001a\u00020\u0006J\u000e\u0010n\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0016\u0010p\u001a\u00020i2\u0006\u0010q\u001a\u00020\u00062\u0006\u0010r\u001a\u00020\u0006J\u000e\u0010s\u001a\u00020i2\u0006\u0010t\u001a\u00020\bJ\u000e\u0010u\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010v\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010w\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010x\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010y\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010z\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010{\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010|\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010}\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010~\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000e\u0010\u007f\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0080\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0081\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0082\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0010\u0010\u0083\u0001\u001a\u00020i2\u0007\u0010\u0084\u0001\u001a\u00020\u0006J\u0010\u0010\u0085\u0001\u001a\u00020i2\u0007\u0010\u0086\u0001\u001a\u00020\u0006J\u000f\u0010\u0087\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0088\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0089\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008a\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008b\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008c\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008d\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008e\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u008f\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0090\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0010\u0010\u0091\u0001\u001a\u00020i2\u0007\u0010\u0092\u0001\u001a\u00020\u0006J\u000f\u0010\u0093\u0001\u001a\u00020i2\u0006\u0010o\u001a\u000204J\u000f\u0010\u0094\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0095\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0007\u0010\u0096\u0001\u001a\u00020iJ\u000f\u0010\u0097\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u0098\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0010\u0010\u0099\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u009b\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u009c\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u009d\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u009e\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u009f\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a0\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a1\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a2\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a3\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a4\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a5\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a6\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u0010\u0010\u00a7\u0001\u001a\u00020i2\u0007\u0010\u009a\u0001\u001a\u000208J\u000f\u0010\u00a8\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0010\u0010\u00a9\u0001\u001a\u00020i2\u0007\u0010\u00aa\u0001\u001a\u00020\u0006J\u000f\u0010\u00ab\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00ac\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00ad\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00ae\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00af\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b0\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b1\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b2\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b3\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b4\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b5\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00b6\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0010\u0010\u00b7\u0001\u001a\u00020i2\u0007\u0010\u00b8\u0001\u001a\u00020UJ\u000f\u0010\u00b9\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00ba\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00bb\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00bc\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00bd\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00be\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00bf\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00c0\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00c1\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00c2\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u0007\u0010\u00c3\u0001\u001a\u00020iJ\u000f\u0010\u00c4\u0001\u001a\u00020i2\u0006\u0010o\u001a\u000204J\u000f\u0010\u00c5\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00c6\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006J\u000f\u0010\u00c7\u0001\u001a\u00020i2\u0006\u0010o\u001a\u00020\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\u0004R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u00c8\u0001"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "PREF_NAME", "", "PRIVATE_MODE", "", "mContext", "getMContext", "()Landroid/content/Context;", "setMContext", "sharedPref", "Landroid/content/SharedPreferences;", "getSharedPref", "()Landroid/content/SharedPreferences;", "getAdId", "getAllNativeStatus", "getAnswer1", "getAnswer2", "getBgColor", "getBottomSheetActivityNativeAdStatusAD", "getBottomSheetActivityNativeAdStatusFB", "getChecklistInterAD", "getChecklistInterFB", "getColorStyleActivityInterAdStatusAD", "getColorStyleActivityInterAdStatusFB", "getColorStyleNativeAdStatusAD", "getColorStyleNativeAdStatusFB", "getDashboardActivityNativeAdStatusAD", "getDashboardActivityNativeAdStatusFB", "getDiaryWriterActivityNativeAdStatusAD", "getDiaryWriterActivityNativeAdStatusFB", "getFeedbackActivityInterAdStatusAD", "getFeedbackActivityInterAdStatusFB", "getFont", "getLanguage", "getLanguageActivityInterAdStatusAD", "getLanguageActivityInterAdStatusFB", "getLanguageActivityNativeAdStatusAD", "getLanguageActivityNativeAdStatusFB", "getLockSettingActivityNativeAdStatusAD", "getLockSettingActivityNativeAdStatusFB", "getLockSettingInterAD", "getLockSettingInterFB", "getLoginActivityInterAdStatusAD", "getLoginActivityInterAdStatusFB", "getPassword", "getPermissionActivityAdStatusAD", "getPermissionActivityAdStatusFB", "getPermissionStatus", "", "getPrivacyActivityInterAdStatusAD", "getPrivacyActivityInterAdStatusFB", "getProgChecklist", "", "getProgColorStyle", "getProgFeedback", "getProgLanguage", "getProgLockSetting", "getProgLoginInter", "getProgPrivacyPolicy", "getProgQuotation", "getProgRateAndReview", "getProgReminder", "getProgThankyou", "getProgTrash", "getProgTutorial", "getProgWriteNote", "getQrIconStatus", "getQrIconUrl", "getQuotationActivityInterAdStatusAD", "getQuotationActivityInterAdStatusFB", "getQuotationActivityNativeAdStatusAD", "getQuotationActivityNativeAdStatusFB", "getReminderActivityInterAdStatusAD", "getReminderActivityInterAdStatusFB", "getReminderActivityNativeAdStatusAD", "getReminderActivityNativeAdStatusFB", "getSplashNativeAdStatusAD", "getSplashNativeAdStatusFB", "getStorageActivityNativeAdStatusAD", "getStorageActivityNativeAdStatusFB", "getTextSize", "", "getThankYouActivityInterAdStatusAD", "getThankYouActivityInterAdStatusFB", "getTrashActivityInterAdStatusAD", "getTrashActivityInterAdStatusFB", "getTrashActivityNativeAdStatusAD", "getTrashActivityNativeAdStatusFB", "getTutorialActivityInterAdStatusAD", "getTutorialActivityInterAdStatusFB", "getTutorialActivityNativeAdStatusAD", "getTutorialActivityNativeAdStatusFB", "getTutorialStatus", "getWriteNoteInterAD", "getWriteNoteInterFB", "getcheckListActivityAdStatus", "isBillPayed", "isPasswordSetup", "isPrivacyPolicyAccepted", "isUsePassword", "payBillStatus", "", "billStatus", "privacyPolicyAccepted", "setAdId", "id", "setAllNativeStatus", "status", "setAnswers", "ans1", "ans2", "setBgColor", "c", "setBottomSheetActivityNativeAdStatusAD", "setBottomSheetActivityNativeAdStatusFB", "setChecklistInterAD", "setChecklistInterFB", "setColorStyleActivityInterAdStatusAD", "setColorStyleActivityInterAdStatusFB", "setColorStyleNativeAdStatusAD", "setColorStyleNativeAdStatusFB", "setDashboardActivityNativeAdStatusAD", "setDashboardActivityNativeAdStatusFB", "setDiaryWriterActivityNativeAdStatusAD", "setDiaryWriterActivityNativeAdStatusFB", "setFeedbackActivityInterAdStatusAD", "setFeedbackActivityInterAdStatusFB", "setFont", "font", "setLanguage", "lang", "setLanguageActivityInterAdStatusAD", "setLanguageActivityInterAdStatusFB", "setLanguageActivityNativeAdStatusAD", "setLanguageActivityNativeAdStatusFB", "setLockSettingActivityNativeAdStatusAD", "setLockSettingActivityNativeAdStatusFB", "setLockSettingInterAD", "setLockSettingInterFB", "setLoginActivityInterAdStatusAD", "setLoginActivityInterAdStatusFB", "setPassword", "password", "setPasswordSetupStatus", "setPermissionActivityAdStatusAD", "setPermissionActivityAdStatusFB", "setPermissionStatus", "setPrivacyActivityInterAdStatusAD", "setPrivacyActivityInterAdStatusFB", "setProgChecklist", "time", "setProgColorStyle", "setProgFeedback", "setProgLanguage", "setProgLockSetting", "setProgLoginInter", "setProgPrivacyPolicy", "setProgQuotation", "setProgRateAndReview", "setProgReminder", "setProgThankyou", "setProgTrash", "setProgTutorial", "setProgWriteNote", "setQrIconStatus", "setQrIconURL", "icon", "setQuotationActivityInterAdStatusAD", "setQuotationActivityInterAdStatusFB", "setQuotationActivityNativeAdStatusAD", "setQuotationActivityNativeAdStatusFB", "setReminderActivityInterAdStatusAD", "setReminderActivityInterAdStatusFB", "setReminderActivityNativeAdStatusAD", "setReminderActivityNativeAdStatusFB", "setSplashNativeAdStatusAD", "setSplashNativeAdStatusFB", "setStorageActivityNativeAdStatusAD", "setStorageActivityNativeAdStatusFB", "setTextSize", "s", "setThankYouActivityInterAdStatusAD", "setThankYouActivityInterAdStatusFB", "setTrashActivityInterAdStatusAD", "setTrashActivityInterAdStatusFB", "setTrashActivityNativeAdStatusAD", "setTrashActivityNativeAdStatusFB", "setTutorialActivityInterAdStatusAD", "setTutorialActivityInterAdStatusFB", "setTutorialActivityNativeAdStatusAD", "setTutorialActivityNativeAdStatusFB", "setTutorialStatua", "setUsePassword", "setWriteNoteInterAD", "setWriteNoteInterFB", "setcheckListActivityAdStatus", "app_release"})
public final class SharedPref {
    private int PRIVATE_MODE = 0;
    private final java.lang.String PREF_NAME = "password";
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContext;
    @org.jetbrains.annotations.NotNull()
    private final android.content.SharedPreferences sharedPref = null;
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.SharedPreferences getSharedPref() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPassword() {
        return null;
    }
    
    public final void setPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String password) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAnswer1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAnswer2() {
        return null;
    }
    
    public final void setAnswers(@org.jetbrains.annotations.NotNull()
    java.lang.String ans1, @org.jetbrains.annotations.NotNull()
    java.lang.String ans2) {
    }
    
    public final int getBgColor() {
        return 0;
    }
    
    public final void setBgColor(int c) {
    }
    
    public final void setTextSize(float s) {
    }
    
    public final float getTextSize() {
        return 0.0F;
    }
    
    public final void setLanguage(@org.jetbrains.annotations.NotNull()
    java.lang.String lang) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLanguage() {
        return null;
    }
    
    public final boolean getPermissionStatus() {
        return false;
    }
    
    public final void setPermissionStatus() {
    }
    
    public final void setTutorialStatua() {
    }
    
    public final boolean getTutorialStatus() {
        return false;
    }
    
    public final void setFont(@org.jetbrains.annotations.NotNull()
    java.lang.String font) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFont() {
        return null;
    }
    
    public final boolean isBillPayed() {
        return false;
    }
    
    public final void payBillStatus(boolean billStatus) {
    }
    
    public final boolean isPrivacyPolicyAccepted() {
        return false;
    }
    
    public final void privacyPolicyAccepted() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAdId() {
        return null;
    }
    
    public final void setAdId(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
    }
    
    public final boolean isUsePassword() {
        return false;
    }
    
    public final void setUsePassword(boolean status) {
    }
    
    public final boolean isPasswordSetup() {
        return false;
    }
    
    public final void setPasswordSetupStatus(boolean status) {
    }
    
    public final void setcheckListActivityAdStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getcheckListActivityAdStatus() {
        return null;
    }
    
    public final void setColorStyleActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getColorStyleActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setColorStyleActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getColorStyleActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setDiaryWriterActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDiaryWriterActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setDiaryWriterActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDiaryWriterActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setLanguageActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLanguageActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setLanguageActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLanguageActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setLanguageActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLanguageActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setLanguageActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLanguageActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setLockSettingActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLockSettingActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setLockSettingActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLockSettingActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setLoginActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLoginActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setLoginActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLoginActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setPermissionActivityAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPermissionActivityAdStatusFB() {
        return null;
    }
    
    public final void setPermissionActivityAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPermissionActivityAdStatusAD() {
        return null;
    }
    
    public final void setPrivacyActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrivacyActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setPrivacyActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrivacyActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setQuotationActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuotationActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setQuotationActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuotationActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setQuotationActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuotationActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setQuotationActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuotationActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setReminderActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getReminderActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setReminderActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getReminderActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setReminderActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getReminderActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setReminderActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getReminderActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setThankYouActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getThankYouActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setThankYouActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getThankYouActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setTrashActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTrashActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setTrashActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTrashActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setTrashActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTrashActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setTrashActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTrashActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setTutorialActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTutorialActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setTutorialActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTutorialActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setTutorialActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTutorialActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setTutorialActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTutorialActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setDashboardActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDashboardActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setDashboardActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDashboardActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setFeedbackActivityInterAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFeedbackActivityInterAdStatusFB() {
        return null;
    }
    
    public final void setFeedbackActivityInterAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFeedbackActivityInterAdStatusAD() {
        return null;
    }
    
    public final void setBottomSheetActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBottomSheetActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setBottomSheetActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBottomSheetActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setSplashNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSplashNativeAdStatusAD() {
        return null;
    }
    
    public final void setSplashNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSplashNativeAdStatusFB() {
        return null;
    }
    
    public final void setStorageActivityNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStorageActivityNativeAdStatusFB() {
        return null;
    }
    
    public final void setStorageActivityNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStorageActivityNativeAdStatusAD() {
        return null;
    }
    
    public final void setColorStyleNativeAdStatusAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getColorStyleNativeAdStatusAD() {
        return null;
    }
    
    public final void setColorStyleNativeAdStatusFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getColorStyleNativeAdStatusFB() {
        return null;
    }
    
    public final void setLockSettingInterFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLockSettingInterFB() {
        return null;
    }
    
    public final void setLockSettingInterAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLockSettingInterAD() {
        return null;
    }
    
    public final void setWriteNoteInterAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getWriteNoteInterAD() {
        return null;
    }
    
    public final void setWriteNoteInterFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getWriteNoteInterFB() {
        return null;
    }
    
    public final void setChecklistInterAD(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getChecklistInterAD() {
        return null;
    }
    
    public final void setChecklistInterFB(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getChecklistInterFB() {
        return null;
    }
    
    public final void setAllNativeStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAllNativeStatus() {
        return null;
    }
    
    public final void setProgLoginInter(long time) {
    }
    
    public final long getProgLoginInter() {
        return 0L;
    }
    
    public final void setProgChecklist(long time) {
    }
    
    public final long getProgChecklist() {
        return 0L;
    }
    
    public final void setProgColorStyle(long time) {
    }
    
    public final long getProgColorStyle() {
        return 0L;
    }
    
    public final void setProgLanguage(long time) {
    }
    
    public final long getProgLanguage() {
        return 0L;
    }
    
    public final void setProgLockSetting(long time) {
    }
    
    public final long getProgLockSetting() {
        return 0L;
    }
    
    public final void setProgPrivacyPolicy(long time) {
    }
    
    public final long getProgPrivacyPolicy() {
        return 0L;
    }
    
    public final void setProgQuotation(long time) {
    }
    
    public final long getProgQuotation() {
        return 0L;
    }
    
    public final void setProgRateAndReview(long time) {
    }
    
    public final long getProgRateAndReview() {
        return 0L;
    }
    
    public final void setProgReminder(long time) {
    }
    
    public final long getProgReminder() {
        return 0L;
    }
    
    public final void setProgThankyou(long time) {
    }
    
    public final long getProgThankyou() {
        return 0L;
    }
    
    public final void setProgTrash(long time) {
    }
    
    public final long getProgTrash() {
        return 0L;
    }
    
    public final void setProgTutorial(long time) {
    }
    
    public final long getProgTutorial() {
        return 0L;
    }
    
    public final void setProgFeedback(long time) {
    }
    
    public final long getProgFeedback() {
        return 0L;
    }
    
    public final void setProgWriteNote(long time) {
    }
    
    public final long getProgWriteNote() {
        return 0L;
    }
    
    public final void setQrIconStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String status) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQrIconStatus() {
        return null;
    }
    
    public final void setQrIconURL(@org.jetbrains.annotations.NotNull()
    java.lang.String icon) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQrIconUrl() {
        return null;
    }
    
    public SharedPref(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}