package com.diary.with.lock.myjournal.notepad.views.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0012J\b\u0010\u0014\u001a\u00020\u0012H\u0016J\u0012\u0010\u0015\u001a\u00020\u00122\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u0006\u0010\u0018\u001a\u00020\u0012J\u0006\u0010\u0019\u001a\u00020\u0012J\u0006\u0010\u001a\u001a\u00020\u0012R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001b"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/FeedbackActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "()V", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "feedBack", "", "loadInterAd", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setSetting", "showInterstitialAdmob", "showInterstitialFb", "app_release"})
public final class FeedbackActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void feedBack() {
    }
    
    public final void setSetting() {
    }
    
    public final void loadInterAd() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public FeedbackActivity() {
        super();
    }
}