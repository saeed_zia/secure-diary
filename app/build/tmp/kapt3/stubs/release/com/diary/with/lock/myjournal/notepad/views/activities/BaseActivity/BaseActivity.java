package com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000b\u001a\u00020\u0004J\u0012\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\b\u0010\u0010\u001a\u00020\rH\u0014J\u000e\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u0013R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "mCurrentLocale1", "Ljava/util/Locale;", "sp", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSp", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSp", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "getLocale", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onRestart", "setLocale1", "lang", "", "app_release"})
public class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sp;
    private java.util.Locale mCurrentLocale1;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSp() {
        return null;
    }
    
    public final void setSp(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onRestart() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Locale getLocale() {
        return null;
    }
    
    public final void setLocale1(@org.jetbrains.annotations.NotNull()
    java.lang.String lang) {
    }
    
    public BaseActivity() {
        super();
    }
}