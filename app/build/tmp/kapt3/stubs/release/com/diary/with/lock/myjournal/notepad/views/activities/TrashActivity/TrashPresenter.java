package com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000eH\u0016J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\u0012\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u0013\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004\u00a8\u0006\u0014"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashPresenter;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$Presenter;", "view", "Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$View;", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$View;)V", "mView", "getMView", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/TrashActivity/TrashContract$View;", "setMView", "deleteNoteFromBackupDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "noteId", "", "deleteNoteFromDatabase", "id", "getAllDatFromDatabase", "restoreNote", "restoreNoteInBackup", "app_release"})
public final class TrashPresenter implements com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.Presenter {
    @org.jetbrains.annotations.NotNull()
    private com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.View mView;
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.View getMView() {
        return null;
    }
    
    public final void setMView(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.View p0) {
    }
    
    @java.lang.Override()
    public void getAllDatFromDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase) {
    }
    
    @java.lang.Override()
    public void deleteNoteFromDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int id) {
    }
    
    @java.lang.Override()
    public void restoreNote(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId) {
    }
    
    @java.lang.Override()
    public void deleteNoteFromBackupDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId) {
    }
    
    @java.lang.Override()
    public void restoreNoteInBackup(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, int noteId) {
    }
    
    public TrashPresenter(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashContract.View view) {
        super();
    }
}