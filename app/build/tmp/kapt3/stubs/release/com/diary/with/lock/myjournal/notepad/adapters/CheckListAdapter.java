package com.diary.with.lock.myjournal.notepad.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0002\b\u0016\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001-B5\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\b\u0010!\u001a\u00020\fH\u0016J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020\fH\u0016J\u0018\u0010&\u001a\u00020\u00022\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\fH\u0016J\u0006\u0010*\u001a\u00020#J\u001e\u0010+\u001a\u00020#2\u0016\u0010,\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0015\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001a\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001e\u0010\u001b\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010 \u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001f\u00a8\u0006."}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter$ViewHolder;", "list", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/CheckListItem;", "Lkotlin/collections/ArrayList;", "checkBoxClickListener", "Lcom/diary/with/lock/myjournal/notepad/interfaces/CheckBoxClickListener;", "textSize", "", "focusPosition", "", "(Ljava/util/ArrayList;Lcom/diary/with/lock/myjournal/notepad/interfaces/CheckBoxClickListener;FI)V", "getCheckBoxClickListener", "()Lcom/diary/with/lock/myjournal/notepad/interfaces/CheckBoxClickListener;", "checkList", "getCheckList", "()Ljava/util/ArrayList;", "setCheckList", "(Ljava/util/ArrayList;)V", "mFocusPosition", "getMFocusPosition", "()Ljava/lang/Integer;", "setMFocusPosition", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "mTextSize", "getMTextSize", "()Ljava/lang/Float;", "setMTextSize", "(Ljava/lang/Float;)V", "Ljava/lang/Float;", "getItemCount", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "requestFcus", "updateCheckList", "newCheckList1", "ViewHolder", "app_release"})
public final class CheckListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter.ViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> checkList;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Float mTextSize;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Integer mFocusPosition;
    private final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> list = null;
    @org.jetbrains.annotations.NotNull()
    private final com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener checkBoxClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> getCheckList() {
        return null;
    }
    
    public final void setCheckList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Float getMTextSize() {
        return null;
    }
    
    public final void setMTextSize(@org.jetbrains.annotations.Nullable()
    java.lang.Float p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMFocusPosition() {
        return null;
    }
    
    public final void setMFocusPosition(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void updateCheckList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> newCheckList1) {
    }
    
    public final void requestFcus() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener getCheckBoxClickListener() {
        return null;
    }
    
    public CheckListAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.CheckListItem> list, @org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener checkBoxClickListener, float textSize, int focusPosition) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/adapters/CheckListAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindItems", "", "checkListItem", "Lcom/diary/with/lock/myjournal/notepad/models/CheckListItem;", "count", "", "app_release"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bindItems(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.models.CheckListItem checkListItem, int count) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}