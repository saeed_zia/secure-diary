package com.diary.with.lock.myjournal.notepad.interfaces;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&\u00a8\u0006\u0007"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/interfaces/FragmentChangeListener;", "", "replaceFragment", "", "fragment", "Landroidx/fragment/app/Fragment;", "storageAllow", "app_release"})
public abstract interface FragmentChangeListener {
    
    public abstract void replaceFragment(@org.jetbrains.annotations.Nullable()
    androidx.fragment.app.Fragment fragment);
    
    public abstract void storageAllow();
}