package com.diary.with.lock.myjournal.notepad.views.activities.splashactivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010J\u001a\u00020KH\u0002J\u0006\u0010L\u001a\u00020KJ\u0006\u0010M\u001a\u00020KJ\u0010\u0010N\u001a\u00020K2\u0006\u0010$\u001a\u00020%H\u0002J\u0006\u0010O\u001a\u00020KJ\u0006\u0010P\u001a\u00020KJ\u0006\u0010Q\u001a\u00020KJ\u0016\u0010R\u001a\u00020K2\u0006\u0010S\u001a\u0002062\u0006\u0010T\u001a\u000206J\u001a\u0010U\u001a\u00020K2\u0006\u0010V\u001a\u00020W2\b\u0010X\u001a\u0004\u0018\u00010YH\u0016J\b\u0010Z\u001a\u00020KH\u0016J\u0012\u0010[\u001a\u00020K2\b\u0010\\\u001a\u0004\u0018\u00010]H\u0014J\u001a\u0010^\u001a\u00020K2\u0006\u0010_\u001a\u0002062\b\u0010`\u001a\u0004\u0018\u00010aH\u0016J\b\u0010b\u001a\u00020KH\u0016J\b\u0010c\u001a\u00020KH\u0014J\u001a\u0010d\u001a\u00020K2\b\u0010D\u001a\u0004\u0018\u00010E2\u0006\u0010\u0006\u001a\u00020eH\u0002J\u000e\u0010f\u001a\u00020K2\u0006\u0010g\u001a\u000206J\u0006\u0010h\u001a\u00020KJ\b\u0010i\u001a\u00020KH\u0002J\u0006\u0010j\u001a\u00020KJ\u0006\u0010k\u001a\u00020KJ\u0006\u0010l\u001a\u00020KJ\u0006\u0010m\u001a\u00020KJ\u0006\u0010n\u001a\u00020KJ\u0006\u0010o\u001a\u00020KJ\u0006\u0010p\u001a\u00020KJ\u0006\u0010q\u001a\u00020KJ\u0006\u0010r\u001a\u00020KR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001e\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0012\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001d\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0010\u0010\"\u001a\u0004\u0018\u00010#X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010$\u001a\u0004\u0018\u00010%X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001c\u0010*\u001a\u0004\u0018\u00010+X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001c\u00100\u001a\u0004\u0018\u00010\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u00102\"\u0004\b3\u00104R\u001c\u00105\u001a\u0004\u0018\u000106X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R\u001c\u0010;\u001a\u0004\u0018\u000106X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u00108\"\u0004\b=\u0010:R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\u001c\u0010D\u001a\u0004\u0018\u00010EX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010G\"\u0004\bH\u0010I\u00a8\u0006s"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/splashactivity/SplashActivity;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Lcom/anjlab/android/iab/v3/BillingProcessor$IBillingHandler;", "()V", "SPLASH_TIME_OUT", "", "adView", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getAdView", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "setAdView", "(Landroidx/constraintlayout/widget/ConstraintLayout;)V", "addStatus", "", "getAddStatus", "()Ljava/lang/Boolean;", "setAddStatus", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "bp", "Lcom/anjlab/android/iab/v3/BillingProcessor;", "getBp", "()Lcom/anjlab/android/iab/v3/BillingProcessor;", "setBp", "(Lcom/anjlab/android/iab/v3/BillingProcessor;)V", "fbRemoteConfig", "Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;", "interStatus", "getInterStatus", "()Z", "setInterStatus", "(Z)V", "mCurrentLocale", "Ljava/util/Locale;", "nativeAd", "Lcom/facebook/ads/NativeAd;", "getNativeAd", "()Lcom/facebook/ads/NativeAd;", "setNativeAd", "(Lcom/facebook/ads/NativeAd;)V", "nativeAdLayout", "Lcom/facebook/ads/NativeAdLayout;", "getNativeAdLayout", "()Lcom/facebook/ads/NativeAdLayout;", "setNativeAdLayout", "(Lcom/facebook/ads/NativeAdLayout;)V", "remoteConfig", "getRemoteConfig", "()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;", "setRemoteConfig", "(Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;)V", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNative", "getSelectedNative", "setSelectedNative", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "unifiedNativeAd", "Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "getUnifiedNativeAd", "()Lcom/google/android/gms/ads/formats/UnifiedNativeAd;", "setUnifiedNativeAd", "(Lcom/google/android/gms/ads/formats/UnifiedNativeAd;)V", "fcmSubscription", "", "goToNextActivity", "hideNativeAnim", "inflateAd", "initializeAdmobAd", "loadInterAd", "loadNativeAd", "loadNewNativeAd", "type", "idNumber", "onBillingError", "errorCode", "", "error", "", "onBillingInitialized", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onProductPurchased", "productId", "details", "Lcom/anjlab/android/iab/v3/TransactionDetails;", "onPurchaseHistoryRestored", "onResume", "populateUnifiedNativeAdView", "Lcom/google/android/gms/ads/formats/UnifiedNativeAdView;", "setLocale", "lang", "setSetting", "setUpRemoteConfig", "showAdmobNativeAd", "showBookAnimation", "showFacebookNativeAd", "showInterstitialAd", "showInterstitialAdmob", "showInterstitialFb", "showNativeAd", "showSplashScreen", "showStartButton", "app_release"})
public final class SplashActivity extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    private com.google.firebase.remoteconfig.FirebaseRemoteConfig fbRemoteConfig;
    private final long SPLASH_TIME_OUT = 5000L;
    @org.jetbrains.annotations.Nullable()
    private com.google.firebase.remoteconfig.FirebaseRemoteConfig remoteConfig;
    @org.jetbrains.annotations.Nullable()
    private com.anjlab.android.iab.v3.BillingProcessor bp;
    private boolean interStatus = false;
    private java.util.Locale mCurrentLocale;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedNative;
    @org.jetbrains.annotations.Nullable()
    private com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAd nativeAd;
    @org.jetbrains.annotations.Nullable()
    private com.facebook.ads.NativeAdLayout nativeAdLayout;
    @org.jetbrains.annotations.Nullable()
    private androidx.constraintlayout.widget.ConstraintLayout adView;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean addStatus = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.firebase.remoteconfig.FirebaseRemoteConfig getRemoteConfig() {
        return null;
    }
    
    public final void setRemoteConfig(@org.jetbrains.annotations.Nullable()
    com.google.firebase.remoteconfig.FirebaseRemoteConfig p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.anjlab.android.iab.v3.BillingProcessor getBp() {
        return null;
    }
    
    public final void setBp(@org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.BillingProcessor p0) {
    }
    
    public final boolean getInterStatus() {
        return false;
    }
    
    public final void setInterStatus(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedNative() {
        return null;
    }
    
    public final void setSelectedNative(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.ads.formats.UnifiedNativeAd getUnifiedNativeAd() {
        return null;
    }
    
    public final void setUnifiedNativeAd(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.ads.formats.UnifiedNativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAd getNativeAd() {
        return null;
    }
    
    public final void setNativeAd(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAd p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.facebook.ads.NativeAdLayout getNativeAdLayout() {
        return null;
    }
    
    public final void setNativeAdLayout(@org.jetbrains.annotations.Nullable()
    com.facebook.ads.NativeAdLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.constraintlayout.widget.ConstraintLayout getAdView() {
        return null;
    }
    
    public final void setAdView(@org.jetbrains.annotations.Nullable()
    androidx.constraintlayout.widget.ConstraintLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAddStatus() {
        return null;
    }
    
    public final void setAddStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void showBookAnimation() {
    }
    
    public final void initializeAdmobAd() {
    }
    
    public final void setLocale(@org.jetbrains.annotations.NotNull()
    java.lang.String lang) {
    }
    
    public final void showSplashScreen() {
    }
    
    public final void showStartButton() {
    }
    
    public final void goToNextActivity() {
    }
    
    public final void loadNativeAd() {
    }
    
    public final void loadInterAd() {
    }
    
    public final void showInterstitialAd() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public final void setSetting() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onBillingInitialized() {
    }
    
    @java.lang.Override()
    public void onPurchaseHistoryRestored() {
    }
    
    @java.lang.Override()
    public void onProductPurchased(@org.jetbrains.annotations.NotNull()
    java.lang.String productId, @org.jetbrains.annotations.Nullable()
    com.anjlab.android.iab.v3.TransactionDetails details) {
    }
    
    @java.lang.Override()
    public void onBillingError(int errorCode, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable error) {
    }
    
    private final void fcmSubscription() {
    }
    
    private final void setUpRemoteConfig() {
    }
    
    public final void hideNativeAnim() {
    }
    
    public final void showNativeAd() {
    }
    
    public final void loadNewNativeAd(@org.jetbrains.annotations.NotNull()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.lang.String idNumber) {
    }
    
    private final void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
    }
    
    private final void inflateAd(com.facebook.ads.NativeAd nativeAd) {
    }
    
    public final void showAdmobNativeAd() {
    }
    
    public final void showFacebookNativeAd() {
    }
    
    public SplashActivity() {
        super();
    }
}