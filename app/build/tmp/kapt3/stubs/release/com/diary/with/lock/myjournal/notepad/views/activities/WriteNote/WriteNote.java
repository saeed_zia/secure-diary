package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010K\u001a\u0002072\u0006\u0010L\u001a\u000203J\u000e\u0010M\u001a\u00020N2\u0006\u0010O\u001a\u000207J\u000e\u0010P\u001a\u00020N2\u0006\u0010Q\u001a\u000207J\u000e\u0010R\u001a\u0002072\u0006\u0010S\u001a\u000207J\u0014\u0010T\u001a\u00020N2\f\u0010U\u001a\b\u0012\u0004\u0012\u0002070VJ\u0006\u0010W\u001a\u00020NJ\"\u0010X\u001a\u00020N2\u0006\u0010Y\u001a\u00020\u00062\u0006\u0010Z\u001a\u00020\u00062\b\u0010[\u001a\u0004\u0018\u00010\\H\u0014J\b\u0010]\u001a\u00020NH\u0016J\u0012\u0010^\u001a\u00020N2\b\u0010_\u001a\u0004\u0018\u00010`H\u0016J\u0012\u0010a\u001a\u00020N2\b\u0010b\u001a\u0004\u0018\u00010cH\u0014J\b\u0010d\u001a\u00020NH\u0014J\u0006\u0010e\u001a\u00020NJ\u0006\u0010f\u001a\u00020NJ\u000e\u0010g\u001a\u00020N2\u0006\u0010O\u001a\u000207J\u0006\u0010h\u001a\u00020NJ\u0006\u0010i\u001a\u00020NJ\u0006\u0010j\u001a\u00020NJ\u0006\u0010%\u001a\u00020NJ\u0006\u0010k\u001a\u00020NJ\u0006\u0010l\u001a\u00020NJ\u0006\u0010m\u001a\u00020NJ\u0006\u0010n\u001a\u00020NJ\u0016\u0010o\u001a\u00020N2\f\u0010p\u001a\b\u0012\u0004\u0012\u00020q0VH\u0016J\b\u0010r\u001a\u00020NH\u0002J\u0010\u0010s\u001a\u00020N2\u0006\u0010t\u001a\u00020\u0006H\u0002J\u0006\u0010u\u001a\u00020NJ\u0006\u0010v\u001a\u00020NJ\u0006\u0010w\u001a\u00020NJ\u0010\u0010x\u001a\u00020N2\u0006\u0010O\u001a\u000207H\u0002J\u0010\u0010y\u001a\u00020N2\u0006\u0010O\u001a\u000207H\u0002J\u0014\u0010z\u001a\u00020N2\f\u0010{\u001a\b\u0012\u0004\u0012\u0002070!J\u0006\u0010|\u001a\u00020NR\u0014\u0010\u0005\u001a\u00020\u0006X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\u0006X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\bR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR \u0010 \u001a\b\u0012\u0004\u0012\u00020\"0!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u001e\u0010\'\u001a\u0004\u0018\u00010\u0015X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010+\u001a\u0004\b\'\u0010(\"\u0004\b)\u0010*R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R \u00102\u001a\b\u0012\u0004\u0012\u0002030!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010$\"\u0004\b5\u0010&R\u001c\u00106\u001a\u0004\u0018\u000107X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001c\u0010<\u001a\u0004\u0018\u00010=X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010?\"\u0004\b@\u0010AR\u001c\u0010B\u001a\u0004\u0018\u00010CX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u001a\u0010H\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\u0017\"\u0004\bJ\u0010\u0019\u00a8\u0006}"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNote;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/BaseActivity/BaseActivity;", "Landroid/view/View$OnClickListener;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;", "()V", "IMAGE_PICK_CAMERA", "", "getIMAGE_PICK_CAMERA", "()I", "IMAGE_PICK_GALLERY", "getIMAGE_PICK_GALLERY", "ads", "Lcom/diary/with/lock/myjournal/notepad/utils/InterstitalAdsSplash;", "cal", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "getCal", "()Ljava/util/Calendar;", "setCal", "(Ljava/util/Calendar;)V", "cameraPermission", "", "getCameraPermission", "()Z", "setCameraPermission", "(Z)V", "emojiPopup", "Lcom/vanniktech/emoji/EmojiPopup;", "getEmojiPopup", "()Lcom/vanniktech/emoji/EmojiPopup;", "setEmojiPopup", "(Lcom/vanniktech/emoji/EmojiPopup;)V", "imageIds", "Ljava/util/ArrayList;", "Lcom/diary/with/lock/myjournal/notepad/models/ImageModel;", "getImageIds", "()Ljava/util/ArrayList;", "setImageIds", "(Ljava/util/ArrayList;)V", "isNewNote", "()Ljava/lang/Boolean;", "setNewNote", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "presenter", "Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;", "getPresenter", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;", "setPresenter", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNotePresenter;)V", "selectedImages", "Landroid/graphics/Bitmap;", "getSelectedImages", "setSelectedImages", "selectedInter", "", "getSelectedInter", "()Ljava/lang/String;", "setSelectedInter", "(Ljava/lang/String;)V", "selectedNoteModel", "Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "getSelectedNoteModel", "()Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;", "setSelectedNoteModel", "(Lcom/diary/with/lock/myjournal/notepad/models/NoteModel;)V", "sharedPref", "Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "getSharedPref", "()Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;", "setSharedPref", "(Lcom/diary/with/lock/myjournal/notepad/utils/SharedPref;)V", "storagePermission", "getStoragePermission", "setStoragePermission", "SaveInStorageBitmap", "qrBitmapImage", "checkPermissions", "", "nextFun", "deleteImage", "path", "getFormatedDate", "date", "loadImages", "imageList", "", "loadInterAd", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "receiveIntent", "refreshImageLayout", "requestPermissions", "requestStoragePermissions", "saveNote", "setClickListeners", "setSetting", "shareNote", "shareText", "showAlertDialog", "showAllData", "note_list", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "showDialog", "showImageDialog", "position", "showInterAd", "showInterstitialAdmob", "showInterstitialFb", "showPermissionTutorialDialog", "showStoragePermissionDialog", "updateStorage", "addresses", "writeFile", "app_release"})
public final class WriteNote extends com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity implements android.view.View.OnClickListener, com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract.View {
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.utils.SharedPref sharedPref;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter presenter;
    private java.util.Calendar cal;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isNewNote;
    private final int IMAGE_PICK_GALLERY = 1;
    private final int IMAGE_PICK_CAMERA = 2;
    @org.jetbrains.annotations.Nullable()
    private com.diary.with.lock.myjournal.notepad.models.NoteModel selectedNoteModel;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<android.graphics.Bitmap> selectedImages;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.ImageModel> imageIds;
    @org.jetbrains.annotations.Nullable()
    private com.vanniktech.emoji.EmojiPopup emojiPopup;
    private boolean storagePermission = false;
    private boolean cameraPermission = false;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String selectedInter;
    private com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash ads;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.utils.SharedPref getSharedPref() {
        return null;
    }
    
    public final void setSharedPref(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.utils.SharedPref p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter getPresenter() {
        return null;
    }
    
    public final void setPresenter(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter p0) {
    }
    
    public final java.util.Calendar getCal() {
        return null;
    }
    
    public final void setCal(java.util.Calendar p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isNewNote() {
        return null;
    }
    
    public final void setNewNote(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    public final int getIMAGE_PICK_GALLERY() {
        return 0;
    }
    
    public final int getIMAGE_PICK_CAMERA() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.diary.with.lock.myjournal.notepad.models.NoteModel getSelectedNoteModel() {
        return null;
    }
    
    public final void setSelectedNoteModel(@org.jetbrains.annotations.Nullable()
    com.diary.with.lock.myjournal.notepad.models.NoteModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<android.graphics.Bitmap> getSelectedImages() {
        return null;
    }
    
    public final void setSelectedImages(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<android.graphics.Bitmap> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.ImageModel> getImageIds() {
        return null;
    }
    
    public final void setImageIds(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.diary.with.lock.myjournal.notepad.models.ImageModel> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.vanniktech.emoji.EmojiPopup getEmojiPopup() {
        return null;
    }
    
    public final void setEmojiPopup(@org.jetbrains.annotations.Nullable()
    com.vanniktech.emoji.EmojiPopup p0) {
    }
    
    public final boolean getStoragePermission() {
        return false;
    }
    
    public final void setStoragePermission(boolean p0) {
    }
    
    public final boolean getCameraPermission() {
        return false;
    }
    
    public final void setCameraPermission(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSelectedInter() {
        return null;
    }
    
    public final void setSelectedInter(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setClickListeners() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void saveNote() {
    }
    
    public final void receiveIntent() {
    }
    
    public final void setImageIds() {
    }
    
    public final void setSetting() {
    }
    
    public final void updateStorage(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> addresses) {
    }
    
    public final void deleteImage(@org.jetbrains.annotations.NotNull()
    java.lang.String path) {
    }
    
    public final void shareNote() {
    }
    
    public final void loadImages(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> imageList) {
    }
    
    public final void refreshImageLayout() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFormatedDate(@org.jetbrains.annotations.NotNull()
    java.lang.String date) {
        return null;
    }
    
    public final void checkPermissions(@org.jetbrains.annotations.NotNull()
    java.lang.String nextFun) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void showPermissionTutorialDialog(java.lang.String nextFun) {
    }
    
    public final void requestPermissions(@org.jetbrains.annotations.NotNull()
    java.lang.String nextFun) {
    }
    
    private final void showStoragePermissionDialog(java.lang.String nextFun) {
    }
    
    public final void requestStoragePermissions() {
    }
    
    private final void showDialog() {
    }
    
    public final void showAlertDialog() {
    }
    
    private final void showImageDialog(int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String SaveInStorageBitmap(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap qrBitmapImage) {
        return null;
    }
    
    @java.lang.Override()
    public void showAllData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    public final void writeFile() {
    }
    
    public final void shareText() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    public final void showInterAd() {
    }
    
    public final void loadInterAd() {
    }
    
    public final void showInterstitialFb() {
    }
    
    public final void showInterstitialAdmob() {
    }
    
    public WriteNote() {
        super();
    }
}