package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0003\u0002\u0003\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract;", "", "BackupPresenter", "Presenter", "View", "app_release"})
public abstract interface WriteNoteContract {
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006\u0007"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$View;", "", "showAllData", "", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "app_release"})
    public static abstract interface View {
        
        public abstract void showAllData(@org.jetbrains.annotations.NotNull()
        java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH&\u00a8\u0006\n"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$Presenter;", "", "getAllDatFromDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "saveNoteInDatabase", "note", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "updateNoteInDatabase", "app_release"})
    public static abstract interface Presenter {
        
        public abstract void getAllDatFromDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase);
        
        public abstract void saveNoteInDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.Note note);
        
        public abstract void updateNoteInDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.Note note);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\t"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/WriteNote/WriteNoteContract$BackupPresenter;", "", "saveNoteInBackupDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "note", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "updateNoteInBackupDatabase", "app_release"})
    public static abstract interface BackupPresenter {
        
        public abstract void saveNoteInBackupDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.Note note);
        
        public abstract void updateNoteInBackupDatabase(@org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
        com.diary.with.lock.myjournal.notepad.Database.Note note);
    }
}