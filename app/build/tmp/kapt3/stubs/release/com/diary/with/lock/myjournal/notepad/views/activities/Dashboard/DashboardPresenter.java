package com.diary.with.lock.myjournal.notepad.views.activities.Dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001e\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0016R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004\u00a8\u0006\u0011"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardPresenter;", "Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardContract$BackupPresenter;", "view", "Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardContract$View;", "(Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardContract$View;)V", "mView", "getMView", "()Lcom/diary/with/lock/myjournal/notepad/views/activities/Dashboard/DashboardContract$View;", "setMView", "getAllNoteDatFromBackupDatabase", "", "appDatabase", "Lcom/diary/with/lock/myjournal/notepad/Database/NoteDatabase;", "saveAllDataInDatabase", "note_list", "", "Lcom/diary/with/lock/myjournal/notepad/Database/Note;", "app_release"})
public final class DashboardPresenter implements com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.BackupPresenter {
    @org.jetbrains.annotations.NotNull()
    private com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.View mView;
    
    @org.jetbrains.annotations.NotNull()
    public final com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.View getMView() {
        return null;
    }
    
    public final void setMView(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.View p0) {
    }
    
    @java.lang.Override()
    public void getAllNoteDatFromBackupDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase) {
    }
    
    @java.lang.Override()
    public void saveAllDataInDatabase(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.Database.NoteDatabase appDatabase, @org.jetbrains.annotations.NotNull()
    java.util.List<com.diary.with.lock.myjournal.notepad.Database.Note> note_list) {
    }
    
    public DashboardPresenter(@org.jetbrains.annotations.NotNull()
    com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardContract.View view) {
        super();
    }
}