package com.diary.with.lock.myjournal.notepad.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0012\u001a\u00020\u0013H\u0003J\u0006\u0010\u0014\u001a\u00020\u0011J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016R\u0014\u0010\u0007\u001a\u00020\u0005X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0005X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u001a\u0010\f\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\t\"\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/diary/with/lock/myjournal/notepad/utils/NotificationUtils;", "Landroid/content/ContextWrapper;", "base", "Landroid/content/Context;", "detail", "", "(Landroid/content/Context;Ljava/lang/String;)V", "MYCHANNEL_ID", "getMYCHANNEL_ID", "()Ljava/lang/String;", "MYCHANNEL_NAME", "getMYCHANNEL_NAME", "mDetail", "getMDetail", "setMDetail", "(Ljava/lang/String;)V", "manager", "Landroid/app/NotificationManager;", "createChannels", "", "getManager", "getNotificationBuilder", "Landroidx/core/app/NotificationCompat$Builder;", "app_release"})
public final class NotificationUtils extends android.content.ContextWrapper {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String MYCHANNEL_ID = "App Alert Notification ID";
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String MYCHANNEL_NAME = "App Alert Notification";
    @org.jetbrains.annotations.NotNull()
    private java.lang.String mDetail = "";
    private android.app.NotificationManager manager;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMYCHANNEL_ID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMYCHANNEL_NAME() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMDetail() {
        return null;
    }
    
    public final void setMDetail(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @android.annotation.TargetApi(value = android.os.Build.VERSION_CODES.O)
    private final void createChannels() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.NotificationManager getManager() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.core.app.NotificationCompat.Builder getNotificationBuilder() {
        return null;
    }
    
    public NotificationUtils(@org.jetbrains.annotations.NotNull()
    android.content.Context base, @org.jetbrains.annotations.NotNull()
    java.lang.String detail) {
        super(null);
    }
}