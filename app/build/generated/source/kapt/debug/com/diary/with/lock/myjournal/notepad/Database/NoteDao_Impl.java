package com.diary.with.lock.myjournal.notepad.Database;

import android.database.Cursor;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.diary.with.lock.myjournal.notepad.models.CheckListItem;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;

@SuppressWarnings({"unchecked", "deprecation"})
public final class NoteDao_Impl implements NoteDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<Note> __insertionAdapterOfNote;

  private final Converters __converters = new Converters();

  private final CheckListTypeConverter __checkListTypeConverter = new CheckListTypeConverter();

  private final EntityDeletionOrUpdateAdapter<Note> __updateAdapterOfNote;

  private final SharedSQLiteStatement __preparedStmtOfDeleteNoteById;

  private final SharedSQLiteStatement __preparedStmtOfMoveToTrash;

  public NoteDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfNote = new EntityInsertionAdapter<Note>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `note_table` (`id`,`type`,`noteTitle`,`noteContent`,`date`,`hasImage`,`imageList`,`isTrashed`,`checkList`) VALUES (?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Note value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getType() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getType());
        }
        if (value.getNoteTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getNoteTitle());
        }
        if (value.getNoteContent() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getNoteContent());
        }
        if (value.getDate() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDate());
        }
        final int _tmp;
        _tmp = value.getHasImage() ? 1 : 0;
        stmt.bindLong(6, _tmp);
        final String _tmp_1;
        _tmp_1 = __converters.toString(value.getImageList());
        if (_tmp_1 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_1);
        }
        final int _tmp_2;
        _tmp_2 = value.isTrashed() ? 1 : 0;
        stmt.bindLong(8, _tmp_2);
        final String _tmp_3;
        _tmp_3 = __checkListTypeConverter.fromCheckList(value.getCheckList());
        if (_tmp_3 == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, _tmp_3);
        }
      }
    };
    this.__updateAdapterOfNote = new EntityDeletionOrUpdateAdapter<Note>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `note_table` SET `id` = ?,`type` = ?,`noteTitle` = ?,`noteContent` = ?,`date` = ?,`hasImage` = ?,`imageList` = ?,`isTrashed` = ?,`checkList` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Note value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getType() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getType());
        }
        if (value.getNoteTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getNoteTitle());
        }
        if (value.getNoteContent() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getNoteContent());
        }
        if (value.getDate() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDate());
        }
        final int _tmp;
        _tmp = value.getHasImage() ? 1 : 0;
        stmt.bindLong(6, _tmp);
        final String _tmp_1;
        _tmp_1 = __converters.toString(value.getImageList());
        if (_tmp_1 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_1);
        }
        final int _tmp_2;
        _tmp_2 = value.isTrashed() ? 1 : 0;
        stmt.bindLong(8, _tmp_2);
        final String _tmp_3;
        _tmp_3 = __checkListTypeConverter.fromCheckList(value.getCheckList());
        if (_tmp_3 == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, _tmp_3);
        }
        if (value.getId() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindLong(10, value.getId());
        }
      }
    };
    this.__preparedStmtOfDeleteNoteById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM note_table WHERE id = ?";
        return _query;
      }
    };
    this.__preparedStmtOfMoveToTrash = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE note_table SET isTrashed=? WHERE id = ?";
        return _query;
      }
    };
  }

  @Override
  public Object insertNote(final Note note, final Continuation<? super Unit> p1) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfNote.insert(note);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, p1);
  }

  @Override
  public Object insertAllNotes(final List<Note> noteList, final Continuation<? super Unit> p1) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfNote.insert(noteList);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, p1);
  }

  @Override
  public void updateNote(final Note... note) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfNote.handleMultiple(note);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteNoteById(final int noteId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteNoteById.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, noteId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteNoteById.release(_stmt);
    }
  }

  @Override
  public void moveToTrash(final int id, final boolean status) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfMoveToTrash.acquire();
    int _argIndex = 1;
    final int _tmp;
    _tmp = status ? 1 : 0;
    _stmt.bindLong(_argIndex, _tmp);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfMoveToTrash.release(_stmt);
    }
  }

  @Override
  public void restoreNote(final int id, final boolean status) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfMoveToTrash.acquire();
    int _argIndex = 1;
    final int _tmp;
    _tmp = status ? 1 : 0;
    _stmt.bindLong(_argIndex, _tmp);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfMoveToTrash.release(_stmt);
    }
  }

  @Override
  public Object getAll(final Continuation<? super List<Note>> p0) {
    final String _sql = "SELECT * FROM note_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return CoroutinesRoom.execute(__db, false, new Callable<List<Note>>() {
      @Override
      public List<Note> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfType = CursorUtil.getColumnIndexOrThrow(_cursor, "type");
          final int _cursorIndexOfNoteTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "noteTitle");
          final int _cursorIndexOfNoteContent = CursorUtil.getColumnIndexOrThrow(_cursor, "noteContent");
          final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "date");
          final int _cursorIndexOfHasImage = CursorUtil.getColumnIndexOrThrow(_cursor, "hasImage");
          final int _cursorIndexOfImageList = CursorUtil.getColumnIndexOrThrow(_cursor, "imageList");
          final int _cursorIndexOfIsTrashed = CursorUtil.getColumnIndexOrThrow(_cursor, "isTrashed");
          final int _cursorIndexOfCheckList = CursorUtil.getColumnIndexOrThrow(_cursor, "checkList");
          final List<Note> _result = new ArrayList<Note>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Note _item;
            final Integer _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getInt(_cursorIndexOfId);
            }
            final String _tmpType;
            _tmpType = _cursor.getString(_cursorIndexOfType);
            final String _tmpNoteTitle;
            _tmpNoteTitle = _cursor.getString(_cursorIndexOfNoteTitle);
            final String _tmpNoteContent;
            _tmpNoteContent = _cursor.getString(_cursorIndexOfNoteContent);
            final String _tmpDate;
            _tmpDate = _cursor.getString(_cursorIndexOfDate);
            final boolean _tmpHasImage;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfHasImage);
            _tmpHasImage = _tmp != 0;
            final List<String> _tmpImageList;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfImageList);
            _tmpImageList = __converters.fromString(_tmp_1);
            final boolean _tmpIsTrashed;
            final int _tmp_2;
            _tmp_2 = _cursor.getInt(_cursorIndexOfIsTrashed);
            _tmpIsTrashed = _tmp_2 != 0;
            final List<CheckListItem> _tmpCheckList;
            final String _tmp_3;
            _tmp_3 = _cursor.getString(_cursorIndexOfCheckList);
            _tmpCheckList = __checkListTypeConverter.toCheckList(_tmp_3);
            _item = new Note(_tmpId,_tmpType,_tmpNoteTitle,_tmpNoteContent,_tmpDate,_tmpHasImage,_tmpImageList,_tmpIsTrashed,_tmpCheckList);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, p0);
  }
}
