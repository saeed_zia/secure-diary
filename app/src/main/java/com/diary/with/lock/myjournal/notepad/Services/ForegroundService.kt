package com.diary.with.lock.myjournal.notepad.Services

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.diary.with.lock.myjournal.notepad.BroadCastReceiver.AlarmBroadcastReceiver
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.views.activities.ReminderActivity.ReminderActivity
import java.util.*


open class ForegroundService() : Service() {
    private val CHANNEL_ID = "ForegroundService Kotlin"

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
try {
    var time = intent?.getLongExtra("timeInMillis", 100)
    var status = intent!!.getStringExtra("status")
    //check if it is repeating alarm or not
    var c = Calendar.getInstance()

    if (status.equals("start")) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, AlarmBroadcastReceiver::class.java)
        val pendingIntent1 = PendingIntent.getBroadcast(this, 0, intent, 0)

        alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time!!, pendingIntent1)
        createNotificationChannel()
        val notificationIntent = Intent(this, ReminderActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Your alarm has been set")
            .setContentText("Alarm")
            .setSmallIcon(R.drawable.ic_baseline_alarm_on_24)
            .setContentIntent(pendingIntent)
            .setChannelId(CHANNEL_ID)
            .setAutoCancel(false)
            .setSound(null)
            .build()
        startForeground(1, notification)
    } else if (status.equals("stop")) {
        stopForeground(true)
        stopSelf()
    }


}
catch (e:Exception){

}
        return START_STICKY
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel",  NotificationManager.IMPORTANCE_LOW)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }
}
