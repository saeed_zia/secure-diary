package com.diary.with.lock.myjournal.notepad.views.activities

import android.content.Context
import com.diary.with.lock.myjournal.notepad.R
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.formats.UnifiedNativeAd


open class BaseAdClass {
    companion object {
        var  nativeAd: UnifiedNativeAd? = null
        var adStatus:String?=null

        fun loadAds(context: Context) {

            val builder = AdLoader.Builder(context, context.getString(R.string.admob_native_id1))
                .forUnifiedNativeAd { unifiedNativeAd ->
                    if (nativeAd != null) {
                        nativeAd?.destroy()
                    }
                    nativeAd = unifiedNativeAd

                }
            builder.withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(p0: Int) {
                    super.onAdFailedToLoad(p0)
                    adStatus="failed"
                   // EventBus.getDefault().post(AdLoadStatus(false,""))

                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adStatus="loaded"
                   // EventBus.getDefault().post(AdLoadStatus(true,""))

                }

            })
            var adLoader: AdLoader = builder.build()
            adLoader.loadAd(AdRequest.Builder().build())
        }
    }
}