package com.diary.with.lock.myjournal.notepad.models

data class Language(val language_name: String, val id: Int,val notation:String)