package com.diary.with.lock.myjournal.notepad.views.activities.SecurityQuestion

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.diary.with.lock.myjournal.notepad.R

import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import com.diary.with.lock.myjournal.notepad.views.activities.setPassword.SetPasswordActivity
import kotlinx.android.synthetic.main.activity_security_question.*

class SecurityQuestionActivity : BaseActivity(),View.OnFocusChangeListener {

    var sharedPref: SharedPref?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_question)

        sharedPref=SharedPref(applicationContext)
        ans1.onFocusChangeListener = this
        ans2.onFocusChangeListener = this
        var source=intent.getStringExtra("source")
        if(source.equals("SetPasswordActivity")){
            tv_description.setText(getString(R.string.set_security_question))
        }
        else{
            tv_description.setText(getString(R.string.answer_security_question))
        }
        next.setOnClickListener{
            var a1=ans1.text.toString()
            var a2=ans2.text.toString()
            if(a1.length>2) {
                if (a2.length>2) {
                    if (source.equals("SetPasswordActivity")) {
                        sharedPref!!.setUsePassword(true)
                        sharedPref?.setAnswers(a1, a2)
                        sharedPref!!.setPasswordSetupStatus(true)
                        finishAffinity()
                        startActivity(Intent(this, DashboardActivity::class.java))
                    } else {  //source is login activity(forget code)

                        if (a1.equals(sharedPref?.getAnswer1().toString()) && a2.equals(sharedPref?.getAnswer2().toString()))
                        {
                            intent = Intent(this, SetPasswordActivity::class.java)
                            intent.putExtra("source", "SecurityQuestionActivity")
                            startActivity(intent)
                        }
                        else{
                            Toast.makeText(applicationContext, "Invalid Answers", Toast.LENGTH_SHORT).show()}

                    }
                }
                else{
                    ans2.setError("Answer length should be more than 2 characters")
                }
            }
            else{ans1.setError("Answer length should be more than 2 characters")}

        }

    }

    override fun onResume() {
        super.onResume()
        var bgColor=sharedPref?.getBgColor()
        security_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
       if(hasFocus){

       }
    }
}