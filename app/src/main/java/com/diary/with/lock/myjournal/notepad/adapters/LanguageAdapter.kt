package com.diary.with.lock.myjournal.notepad.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.interfaces.LanguageClickListener
import com.diary.with.lock.myjournal.notepad.models.Language



class LanguageAdapter(val language_list: ArrayList<Language>, private val languageClickListener: LanguageClickListener) : RecyclerView.Adapter<LanguageAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.language_item, parent, false)
        return LanguageAdapter.ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: LanguageAdapter.ViewHolder, position: Int) {
        holder.bindItems(language_list[position])
        holder.itemView.setOnClickListener {
            languageClickListener.onLanguageClickListener(language_list[position])
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return language_list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(language: Language) {
           val flag=itemView.findViewById<ImageView>(R.id.flag)
            val language_name=itemView.findViewById<TextView>(R.id.language_name)
           try{
               flag.setImageResource(language.id)
           }
           catch (e:Exception){
               e.printStackTrace()
           }
            language_name.text=language.language_name
        }
    }
}