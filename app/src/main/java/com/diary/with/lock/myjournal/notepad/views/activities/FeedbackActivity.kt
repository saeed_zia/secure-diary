package com.diary.with.lock.myjournal.notepad.views.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import kotlinx.android.synthetic.main.activity_feedback.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.toolbar.*


class FeedbackActivity : BaseActivity() {
    var sharedPref: SharedPref?=null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)
        sharedPref= SharedPref(this)
        setSetting()
        loadInterAd()
        submit_btn.setOnClickListener{
            feedBack()
        }
        back_arrow.setOnClickListener{
          onBackPressed()
        }
        qr_icon.setOnClickListener{
            try {
                Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    fun feedBack(){
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("maximustoolsapp@gmail.com"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Feed back")
        i.putExtra(Intent.EXTRA_TEXT, content_input.text.toString())
        try {
            startActivity(Intent.createChooser(i, "Send mail..."))
            content_input.setText("")
            onBackPressed()
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }
    }
    fun setSetting(){
        var bgColor=sharedPref?.getBgColor()
        feed_back_root?.setBackgroundColor(bgColor!!)
        activity_title.setText(getString(R.string.feed_back))
        window.statusBarColor = sharedPref!!.getBgColor()
        if(sharedPref!!.getQrIconStatus().equals("false")){
            qr_icon.visibility=View.GONE
        }
    }

    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getFeedbackActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getFeedbackActivityInterAdStatusAD()
                    .equals("false"))
            {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if ((sharedPref!!.getFeedbackActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getFeedbackActivityInterAdStatusAD()
                    .equals("true"))
            ) {


                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }
    override fun onBackPressed() {
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgFeedback())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgTrash())
        }
        else{
            finish()
            //  startActivity(Intent(this, DashboardActivity::class.java))
        }
    }

}