package com.diary.with.lock.myjournal.notepad.views.activities.LockSetting

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.setPassword.SetPasswordActivity
import com.facebook.ads.AdOptionsView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdLayout
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import kotlinx.android.synthetic.main.activity_lock_setting.*
import kotlinx.android.synthetic.main.customize_native_ad.*
import kotlinx.android.synthetic.main.loading_fb_native.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.native_ads.view.*
import kotlinx.android.synthetic.main.toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class LockSettingActivity : BaseActivity() {
    var sharedPref: SharedPref?=null
    var nativeAd:NativeAd?=null
    var nativeAdLayout:NativeAdLayout?=null
    var adView:ConstraintLayout?=null
    var selectedInter:String?=null
    var selectedNative:String?=null
    var unifiedNativeAd:UnifiedNativeAd?=null
    private var ads: InterstitalAdsSplash?=null
    var addStatus:Boolean?=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lock_setting)
        sharedPref=SharedPref(this)
        loadInterAd()
        setSetting()
        setClickListeners()
        showNativeAd()

    }
    fun setSetting(){

        lock_root.setBackgroundColor(sharedPref!!.getBgColor())
        activity_title.setText(getString(R.string.set_diary_lock))
        window.statusBarColor = sharedPref!!.getBgColor()
        if(sharedPref!!.getQrIconStatus().equals("false")){
            qr_icon.visibility=View.GONE
        }

    }

    override fun onResume() {
        super.onResume()
        if(sharedPref!!.isUsePassword()) {
            password_switch.isChecked = true
        }
    }

    fun setClickListeners(){
        password_switch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
          if(isChecked){
              if(sharedPref!!.isPasswordSetup()){
                  sharedPref!!.setUsePassword(true)
              }
              else{
                  var intent=Intent(this, SetPasswordActivity::class.java)
                  intent.putExtra("source","other")
                  startActivity(intent)
              }

          }
            else{
              sharedPref!!.setUsePassword(false)
          }
        })
        const1.setOnClickListener{
            password_switch.toggle()
        }
        const2.setOnClickListener{
            var intent=Intent(this,SetPasswordActivity::class.java)
            intent.putExtra("source","other")
            startActivity(intent)
        }
      back_arrow.setOnClickListener{
          onBackPressed()
      }
        qr_icon.setOnClickListener{
            try {
                Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }
    }
    fun makeNonClickAbleAD(){
        ad_media.isClickable = false
        ad_headline.isClickable = false
        ad_body.isClickable = false
        ad_icon.isClickable = false
    }
    fun makeNonClickAbleFB(){
        native_ad_media.isClickable = false
        native_ad_social_context.isClickable = false
        native_ad_body.isClickable = false
        native_ad_icon.isClickable = false
    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.fb_ad)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
        // native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }
    private fun showFacebookNative1() {
        //
        if (FacebookNativeAd.nativeAd1 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd2(this)
            nativeAd = FacebookNativeAd.nativeAd1
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus1.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd2(this)
            hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    fun showFacebookNative2() {
        //
        if (FacebookNativeAd.nativeAd2 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd1(this)
            nativeAd = FacebookNativeAd.nativeAd2
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
               //makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus2.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd1(this)
            hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }


    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        native_ad_const.visibility=View.GONE
    }
    fun showNativeAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getLockSettingActivityNativeAdStatusFB()
                    .equals("true") && sharedPref!!.getLockSettingActivityNativeAdStatusAD()
                    .equals("false"))
            {
                if(FacebookNativeAd.selectedAd.equals("ad1")){
                    showFacebookNative1()
                }
                else if (FacebookNativeAd.selectedAd.equals("ad2")){
                    showFacebookNative2()
                }
                selectedNative = "fb"
            }
            else if ((sharedPref!!.getLockSettingActivityNativeAdStatusFB()
                    .equals("false") && sharedPref!!.getLockSettingActivityNativeAdStatusAD()
                    .equals("true")))
            {
                if(AdmobNativeAd.selectedAd.equals("ad1")){
                    showAdmobNativeAd1()
                }
                else if (AdmobNativeAd.selectedAd.equals("ad2")){
                    showAdmobNativeAd2()
                }
                selectedNative = "ad"
            } else {
                hideNativeAnim()
                selectedNative = "no"
            }
        }
        else{
            hideNativeAnim()
            selectedNative="no"
        }
    }
    fun showAdmobNativeAd1(){

        if (AdmobNativeAd.nativeAd1 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd1
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
          //  makeNonClickAbleAD()

        } else if (AdmobNativeAd.adStatus1.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }

    fun showAdmobNativeAd2(){

        if (AdmobNativeAd.nativeAd2 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd2
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
           // makeNonClickAbleAD()
        } else if (AdmobNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }


    fun loadNewNativeAd(type:String,idNumber:String){
        if (type.equals("fb")){
            if (idNumber.equals("id1")){
                FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
            }
            else{
                FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
            }
        }
        else if(type.equals("ad")){
            if (idNumber.equals("id1")){
                AdmobNativeAd.loadAdId2(applicationContext)
            }
            else{
                AdmobNativeAd.loadAdId1(applicationContext)
            }
        }
    }
    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd?,
        adView: UnifiedNativeAdView
    )
    {

        var media=adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }

            /* if (unifiedNativeAd.advertiser == null) {
                 adView.advertiserView.visibility = View.INVISIBLE
             } else {
                 adView.ad_advertiser.text = unifiedNativeAd.advertiser
                 adView.advertiserView.visibility = View.VISIBLE
             }*/
            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgLockSetting())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgLockSetting())
        }
        else{
            finish()
          //  startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getLockSettingInterFB()
                    .equals("true") && sharedPref!!.getLockSettingInterAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getLockSettingInterFB()
                    .equals("false") && sharedPref!!.getLockSettingInterAD()
                    .equals("true")
            ) {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }
    override fun onBackPressed() {
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(adloadStatus: AdLoadStatus) {
        if(adloadStatus.isLoaded!!) {
            if(!addStatus!!){
                showNativeAd()

            }
        }
        else{
            if (!addStatus!!){
                hideNativeAnim()
            }
            addStatus=true
            //
            loadNewNativeAd(adloadStatus.idType,adloadStatus.idNumber)
        }
    }
}