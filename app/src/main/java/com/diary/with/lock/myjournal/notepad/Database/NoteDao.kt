package com.diary.with.lock.myjournal.notepad.Database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface NoteDao {

    @Query("SELECT * FROM note_table")
    suspend fun getAll(): List<Note>


    @Insert
    suspend fun insertNote(note: Note)
    @Insert
    suspend fun insertAllNotes(noteList: List<Note>)

    @Update
    fun updateNote(vararg note: Note)


    @Query("DELETE FROM note_table WHERE id = :noteId")
    fun deleteNoteById(noteId: Int)

    @Query("UPDATE note_table SET isTrashed=:status WHERE id = :id")
    fun moveToTrash( id: Int,status:Boolean)

    @Query("UPDATE note_table SET isTrashed=:status WHERE id = :id")
    fun restoreNote( id: Int,status:Boolean)

}