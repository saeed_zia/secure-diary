package com.diary.with.lock.myjournal.notepad.views.activities.quotation

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.adapters.QuotationFrameAdapter
import com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_show_quotation.*
import kotlinx.android.synthetic.main.show_quote_toolbar.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class ShowQuotationActivity : AppCompatActivity(),FrameClickListener {
    var frameArraylist= arrayListOf<Int>()
    var frameAdapter:QuotationFrameAdapter?=null
    var quoteText:String?=null
    var combined:Bitmap?=null
    var selectedFrampos:Int?=null
    var author:String?=null
    var sharedPref: SharedPref?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_quotation)

        sharedPref= SharedPref(applicationContext)

        quoteText=intent.getStringExtra("quoteText")
        author=intent.getStringExtra("author")
        AddFrames()
        activity_title.setText(getString(R.string.share_quotes))
        activity_title.setTextColor(getColor(R.color.colorPrimary))
        quotation_frame_recycler.layoutManager = LinearLayoutManager(applicationContext,LinearLayoutManager.HORIZONTAL,false)
        frameAdapter = QuotationFrameAdapter(applicationContext,frameArraylist,this)
        quotation_frame_recycler?.adapter = frameAdapter
        tv_quote_text.text=quoteText


        btn_share.setOnClickListener{
          applyFrame()
        }
        back_arrow.setOnClickListener{
            super.onBackPressed()
        }
       qr_icon.setOnClickListener {

           try {
               Utils.openQrReference(this)
           } catch (e: Exception) {
               e.printStackTrace()
           }
       }
       onFrameClick(2)
    }

     fun AddFrames()  {
        frameArraylist.add(R.drawable.frame)
        frameArraylist.add(R.drawable.frame_a)
        frameArraylist.add(R.drawable.frame_b)
        frameArraylist.add(R.drawable.frame_c)
        frameArraylist.add(R.drawable.frame_d)
     //frameAdapter!!.notifyDataSetChanged()

     }
    override fun onFrameClick(position: Int){
        selectedFrampos=position
        iv_full_frame.setImageDrawable(getDrawable(frameArraylist.get(position)))
    }
  fun applyFrame() {

      try {
          var tv = findViewById<TextView>(R.id.tv_quote_text)

          tv.setDrawingCacheEnabled(true)
          tv.buildDrawingCache()

          val tv_bmp: Bitmap = Bitmap.createBitmap(tv.getDrawingCache())
          var frame: Bitmap? = null
          frame =
              BitmapFactory.decodeResource(getResources(), frameArraylist.get(selectedFrampos!!))
          applyFrameOnTextbmp(tv_bmp, frame!!)
          tv.setText("")
      }
      catch (e:Exception){
          e.printStackTrace()
      }
    }
    fun applyFrameOnTextbmp(tv_bmp: Bitmap?, frame: Bitmap) {
        if (tv_bmp==null){
           // img_bitmap1.setImageBitmap(qrcode)
        }
        else {
            try {
                combined = Bitmap.createBitmap(frame.width, frame.height, frame.config)
                val canvas = Canvas(combined!!)
                val canvasWidth = canvas.width
                val canvasHeight = canvas.height
                canvas.drawBitmap(frame, Matrix(), null)

                val centreX = (canvasWidth - tv_bmp.width) / 2
                val centreY = (canvasHeight - tv_bmp.height) / 2
                canvas.drawBitmap(tv_bmp, centreX.toFloat(), centreY.toFloat(), null)

                iv_full_frame.setImageBitmap(combined)
                shareQuotation()
            }
            catch (e:Exception)
            {
                e.printStackTrace()
            }
        }
    }
    fun shareQuotation(){

      checkStoragePermissionAndShare()
    }
    private fun saveImageExternal(image: Bitmap) {

        var uri: Uri? = null
        try {
            val file =
                File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "to-share.png")
            val stream = FileOutputStream(file)
            image.compress(Bitmap.CompressFormat.PNG, 100, stream)
            stream.close()
            uri = Uri.fromFile(file)
            //
            shareImageUri(uri)
            //
        } catch (e: IOException) {
            Log.d("TAG", "IOException while trying to write file for sharing: " + e.message)
        }

    }
     fun shareImageUri(uri: Uri) {
        try {

        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }
        catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun checkStoragePermissionAndShare(){
        var permission=false
        val permissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
        Permissions.check(this /*context*/, permissions,null /*rationale*/,null /*options*/, object : PermissionHandler() {
            override fun onGranted() {
                // do your task.
                try {

                    saveImageExternal(combined!!)!!

                }
                catch (e:Exception){
                    e.printStackTrace()
                }
            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                Toast.makeText(applicationContext, "Permission is required", Toast.LENGTH_SHORT).show()
            }
        })

    }
    fun setSetting(){
        activity_title.setText("Show Quotation Activity")
       // back_arrow.setImageDrawable(getDrawable(R.drawable.back_frame))
    }
}