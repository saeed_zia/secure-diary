package com.diary.with.lock.myjournal.notepad.Database

import androidx.room.TypeConverter
import com.diary.with.lock.myjournal.notepad.models.CheckListItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class CheckListTypeConverter {
    @TypeConverter
    fun fromCheckList(countryLang: List<CheckListItem?>?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<CheckListItem?>?>() {}.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toCheckList(countryLangString: String?): List<CheckListItem>? {
        if (countryLangString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<CheckListItem?>?>() {}.type
        return gson.fromJson<List<CheckListItem>>(countryLangString, type)
    }
}