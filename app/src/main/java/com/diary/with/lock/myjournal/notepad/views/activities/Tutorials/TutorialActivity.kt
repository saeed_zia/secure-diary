package com.diary.with.lock.myjournal.notepad.views.activities.Tutorials


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.adapters.CustomPagerAdapter
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import com.diary.with.lock.myjournal.notepad.views.activities.setPassword.SetPasswordActivity
import com.facebook.ads.AdOptionsView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdLayout
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.customize_native_ad.*
import kotlinx.android.synthetic.main.loading_fb_native.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.native_ads.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class TutorialActivity : BaseActivity() {
    var sharedPref: SharedPref? = null

    var source=""
    var currentItem:Int?=null
    var unifiedNativeAd:UnifiedNativeAd?=null
    var selectedNative:String?=null
    var nativeAd:NativeAd?=null
    var nativeAdLayout: NativeAdLayout?=null
    var adView: ConstraintLayout?=null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    var addStatus:Boolean?=false
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        sharedPref = SharedPref(applicationContext)

        showNativeAd()
        var viewPager = findViewById<View>(R.id.viewpager) as ViewPager
        viewPager.adapter = CustomPagerAdapter(this)
        source = intent.getStringExtra("source")!!
        loadInterAd()

        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                currentItem = viewpager.currentItem
                Log.d("test", currentItem.toString())
                if(currentItem!!>0){
                    back_btn.visibility=View.VISIBLE
                }

                if(currentItem==2){
                    if(source.equals("SplashActivity")){
                        goToNextActivity()
                    }

                }
                else if(currentItem==11){
                    goToNextActivity()
                }

            }

            override fun onPageSelected(position: Int) {
                // Check if this is the page you want.
            }
        })
        if(viewpager.currentItem==0){
            back_btn.visibility=View.GONE
        }
        next_btn.setOnClickListener {

            openNextPage()
        }

        back_btn.setOnClickListener {
            viewpager.setCurrentItem(viewpager.currentItem - 1, true)
            if(viewpager.currentItem==0){
                back_btn.visibility=View.GONE
            }

        }
    }
    fun openNextPage(){
        currentItem = viewpager.currentItem

        if(currentItem==2){
            if(source.equals("SplashActivity")){
                goToNextActivity()
            }
            else{
                viewpager.setCurrentItem(viewpager.currentItem + 1, true)
            }
        }
        else if(currentItem==11){
            goToNextActivity()
        }
        else {
            viewpager.setCurrentItem(viewpager.currentItem + 1, true)
        }
    }
    fun goToNextActivity(){
            sharedPref!!.setTutorialStatua()
                if(source.equals("DashboardActivity")) {
                    if (selectedInter.equals("fb")) {
                        if (FacebookAds.interstitialAd != null) {
                            showInterstitialFb()

                        } else {
                           finish()
                            startActivity(Intent(this,
                                DashboardActivity::class.java))
                        }
                    } else if (selectedInter.equals("ad")) {
                        ads = InterstitalAdsSplash()
                        showInterstitialAdmob()
                    } else {
                        finish()
                        startActivity(Intent(this,
                            DashboardActivity::class.java))
                    }

            }
            else  {
               // finishAffinity()
                var intent = Intent(applicationContext, SetPasswordActivity::class.java)
                intent.putExtra("source", "FirstTime")
                startActivity(intent)
            }


    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.fb_ad)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
        // native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }

    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        ad_container.visibility=View.INVISIBLE
    }

    fun showNativeAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getTutorialActivityNativeAdStatusFB()
                    .equals("true") && sharedPref!!.getTutorialActivityNativeAdStatusAD()
                    .equals("false"))
            {
                if(FacebookNativeAd.selectedAd.equals("ad1")){
                    showFacebookNative1()
                }
                else if (FacebookNativeAd.selectedAd.equals("ad2")){
                    showFacebookNative2()
                }
                selectedNative = "fb"
            }
            else if ((sharedPref!!.getTutorialActivityNativeAdStatusFB()
                    .equals("false") && sharedPref!!.getTutorialActivityNativeAdStatusAD()
                    .equals("true")))
            {
                if(AdmobNativeAd.selectedAd.equals("ad1")){
                    showAdmobNativeAd1()
                }
                else if (AdmobNativeAd.selectedAd.equals("ad2")){
                    showAdmobNativeAd2()
                }
                selectedNative = "ad"
            } else {
                hideNativeAnim()
                selectedNative = "no"
            }
        }
        else{
            hideNativeAnim()
            selectedNative="no"
        }
    }

    fun showAdmobNativeAd1(){
        AdmobNativeAd.loadAdId2(this)
        if (AdmobNativeAd.nativeAd1 != null) {
            addStatus=true
            unifiedNativeAd = AdmobNativeAd.nativeAd1
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
           // makeNonClickAbleAD()

        } else if (AdmobNativeAd.adStatus1.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    fun showAdmobNativeAd2(){

        if (AdmobNativeAd.nativeAd2 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd2
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
            makeNonClickAbleAD()
        } else if (AdmobNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    private fun showFacebookNative1() {
        //
        if (FacebookNativeAd.nativeAd1 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd2(this)
            nativeAd = FacebookNativeAd.nativeAd1
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus1.equals("failed")) {
            Log.d("d","d")
            hideNativeAnim()
            FacebookNativeAd.loadNativeFacebookAd2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    private fun showFacebookNative2() {
        //
        if (FacebookNativeAd.nativeAd2 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd1(this)
            nativeAd = FacebookNativeAd.nativeAd2
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
             //makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus2.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd1(this)
            hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(adloadStatus: AdLoadStatus) {
        if(adloadStatus.isLoaded!!) {
            if(!addStatus!!){
                showNativeAd()
            }
        }
        else{
            if (!addStatus!!){
                hideNativeAnim()
            }
            addStatus=true
            //
            loadNewNativeAd(adloadStatus.idType,adloadStatus.idNumber)
        }
    }

    fun loadNewNativeAd(type:String,idNumber:String){
        if (type.equals("fb")){
            if (idNumber.equals("id1")){
                FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
            }
            else{
                FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
            }
        }
        else if(type.equals("ad")){
            if (idNumber.equals("id1")){
                AdmobNativeAd.loadAdId2(applicationContext)
            }
            else{
                AdmobNativeAd.loadAdId1(applicationContext)
            }
        }
    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
    override fun onResume() {
        super.onResume()
        var bgColor = sharedPref?.getBgColor()
        tutorial_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
    }
    fun makeNonClickAbleAD(){
        ad_media.isClickable = false
        ad_headline.isClickable = false
        ad_body.isClickable = false
        ad_icon.isClickable = false
    }
    fun makeNonClickAbleFB(){
        native_ad_media.isClickable = false
        native_ad_social_context.isClickable = false
        native_ad_body.isClickable = false
        native_ad_icon.isClickable = false
    }
    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd?,
        adView: UnifiedNativeAdView)
    {

        var media=adView.findViewById<MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }

            /* if (unifiedNativeAd.advertiser == null) {
                 adView.advertiserView.visibility = View.INVISIBLE
             } else {
                 adView.ad_advertiser.text = unifiedNativeAd.advertiser
                 adView.advertiserView.visibility = View.VISIBLE
             }*/
            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getTutorialActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getTutorialActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if ((sharedPref!!.getTutorialActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getTutorialActivityInterAdStatusAD()
                    .equals("true"))
            ) {
               // InterstitalAdsSplash.loadInterstitialAd(applicationContext)
                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }
    override fun onBackPressed() {
        if(source.equals("DashboardActivity")) {
            if (selectedInter.equals("fb")) {
                if (FacebookAds.interstitialAd != null) {
                    showInterstitialFb()

                } else {
                    super.onBackPressed()
                }
            } else if (selectedInter.equals("ad")) {
                ads = InterstitalAdsSplash()
                showInterstitialAdmob()
            } else {
                super.onBackPressed()
            }
        }
        else{
            finishAffinity()
        }
    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgTutorial())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgTutorial())
        }
        else{
            finish()
           // startActivity(Intent(this, DashboardActivity::class.java))
        }
    }


}