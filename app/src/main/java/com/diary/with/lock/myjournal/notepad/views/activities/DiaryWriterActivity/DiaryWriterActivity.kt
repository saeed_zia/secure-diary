package com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity


import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.Database.DatabaseBuilder
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.adapters.ListAdapter
import com.diary.with.lock.myjournal.notepad.interfaces.CellClickListener
import com.diary.with.lock.myjournal.notepad.interfaces.LongClickListener
import com.diary.with.lock.myjournal.notepad.models.NoteModel
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNote
import com.diary.with.lock.myjournal.notepad.views.activities.checkList.CheckListActivity
import com.facebook.ads.AdOptionsView
import com.facebook.ads.MediaView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdLayout
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_diary_writer.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.customize_native_ad.*
import kotlinx.android.synthetic.main.list_item.*
import kotlinx.android.synthetic.main.loading_fb_native.*
import kotlinx.android.synthetic.main.native_ads.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Type
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class DiaryWriterActivity: BaseActivity(), CellClickListener, DiaryWriterContract.View,
    LongClickListener {

    var presenter: DiaryWriterPresenter? = null
    var cal = Calendar.getInstance()
    var sharedPref: SharedPref?=null
    var adapter: ListAdapter? = null
    var noteDataList = arrayListOf<NoteModel>()

    var nativeAd:NativeAd?=null
    var nativeAdLayout:NativeAdLayout?=null
    var adView:ConstraintLayout?=null
    var unifiedNativeAd:UnifiedNativeAd?=null
    var selectedNative:String?=null
    var addStatus:Boolean?=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diary_writer)
        sharedPref=SharedPref(applicationContext)

        showNativeAd()
        setSetting()
        clickListeners()
        list_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
        adapter = ListAdapter(applicationContext,noteDataList,this,this)
        list_recycler_view.adapter = adapter

    }
    fun sortLatestFirst(){
        var sortedList=noteDataList
        Collections.reverse(sortedList)
        adapter!!.updateUsers(sortedList)
    }
    fun sortOldestFirst(){
        fetchNoteList()
    }
    fun sortAlphabatically(){
        var sortedList=noteDataList
        Collections.sort(sortedList, Comparator { item, t1 ->
            val s1: String = item.noteTitle
            val s2: String = t1.noteTitle
            s1.compareTo(s2, ignoreCase = true) })
        adapter!!.updateUsers(sortedList)

    }
    override fun onRestart() {
        super.onRestart()
       // fetchNoteList()
    }

    fun setSetting(){
        val actionBar: ActionBar? = this.supportActionBar
        actionBar?.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM)
        //actionBar?.setDisplayShowCustomEnabled(true)
        //actionBar?.setCustomView(R.id.toolbar1)
       // supportActionBar?.setDisplayHomeAsUpEnabled(true)
        bar_title.setText(getString(R.string.diary_notes))

       // window.statusBarColor = sharedPref!!.getBgColor()


    }

    fun clickListeners(){

        back_arrow.setOnClickListener{
           onBackPressed()
        }
        note_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                adapter?.filter?.filter(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
               // Toast.makeText(applicationContext, "dgfgbfg", Toast.LENGTH_LONG).show()
                return false
            }

        })
       note_search.setOnQueryTextFocusChangeListener { v, hasFocus ->
           if(hasFocus){

               native_ad_const.visibility=View.GONE

           }
           else{
               if(addStatus!!){
                   native_ad_const.visibility=View.VISIBLE
               }

           }
       }
        fabBtn.setOnClickListener {
            note_search.setIconified(true)
           if(popup_const.visibility==View.VISIBLE) {
               popup_const.visibility=View.GONE
           }
            else{
               popup_const.visibility=View.VISIBLE
           }

        }
        btn_checklist.setOnClickListener{
            popup_const.visibility=View.GONE
            val intent = Intent(baseContext, CheckListActivity::class.java)
            intent.putExtra("isNewNote", true)
            startActivity(intent)
        }
        btn_text.setOnClickListener{
            popup_const.visibility=View.GONE
            val intent = Intent(baseContext, WriteNote::class.java)
            intent.putExtra("isNewNote", true)
            startActivity(intent)
        }
        iv_sort.setOnClickListener{
            val popupMenu: PopupMenu = PopupMenu(this,iv_sort)
            popupMenu.menuInflater.inflate(R.menu.sort_popup,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.menu_sort_latest_first ->
                    {
                       sortLatestFirst()

                    }
                    R.id.menu_sort_oldest_first ->
                    {
                      sortOldestFirst()
                    }
                    R.id.menu_sort_alpha ->
                    {
                        sortAlphabatically()
                    }

                }
                true
            })
            popupMenu.show()
            //

            //fetchNoteListFromBackup()

        }

        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder): Boolean {
              Log.d("hello","dd")
                return  true
//***
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val swipedPosition: Int = viewHolder.getAdapterPosition()
                moveToTrash(swipedPosition)
                val toast = Toast.makeText(applicationContext,"Moved to Trash", Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
                toast.show()

            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(list_recycler_view)

    }

    fun moveToTrash(positon: Int){
     try {
         var noteModel = noteDataList.get(positon)
         noteDataList.removeAt(positon)
         adapter?.notifyDataSetChanged()
         //delete note from db
         val noteDatabase: NoteDatabase = DatabaseBuilder.getInstance(applicationContext)
         presenter = DiaryWriterPresenter(this)
         presenter!!.moveNoteToTrash(noteDatabase, noteModel.id!!)
         //
         var root = Environment.getExternalStorageDirectory().toString()
         val myFile = File("$root/Maximus/BackupDiary/backup-diary-database")
         if (myFile.exists()) {
             val noteBackupDatabase: NoteDatabase =
                 DatabaseBuilder.getBackupInstance(applicationContext, myFile)
             presenter!!.moveNoteToTrashInBackup(noteBackupDatabase, noteModel.id!!)
         }
     }
     catch (e:Exception){
         e.printStackTrace()
     }
    }
    override fun showAllNoteData(note_list: List<Note>) {

        runOnUiThread {
            noteDataList.clear()
            for (note in note_list) {
                if (!note.isTrashed) {
                    var date = note.date.split("/")
                    var note_model = NoteModel(
                        note?.id!!,
                        note?.type,
                        note?.noteTitle,
                        note?.noteContent,
                        note?.imageList,
                        note?.checkList,
                        date[0],
                        date[1],
                        date[2])
                    noteDataList.add(note_model)
                }
            }

            adapter?.updateUsers(noteDataList)


            if (noteDataList.size == 0) {
                empty_placeholder_lo.visibility = View.VISIBLE
            } else {
                empty_placeholder_lo.visibility = View.GONE
            }
        }

    }
    private fun goToWriteNoteActivity(noteModel: NoteModel) {
        val gson = Gson()
        val type: Type = object : TypeToken<NoteModel?>() {}.getType()
        val json: String = gson.toJson(noteModel, type)
        val intent = Intent(applicationContext, WriteNote::class.java)
        intent.putExtra("selectedNote", json)
        intent.putExtra("isNewNote", false)
        startActivity(intent)
    }
    private fun goToCheckListActivity(note:NoteModel){
        val gson = Gson()
        val type: Type = object : TypeToken<NoteModel?>() {}.getType()
        val json: String = gson.toJson(note, type)
        val intent = Intent(applicationContext, CheckListActivity::class.java)
        intent.putExtra("selectedNote", json)
        intent.putExtra("isNewNote", false)
        startActivity(intent)
    }
    override fun onResume() {
        super.onResume()
        var bgColor=sharedPref?.getBgColor()
        diary_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
        note_search.setIconified(true)
        fetchNoteList()
       // toolbar2.setBackgroundColor(bgColor!!)
    }
    fun fetchNoteList() {

        val noteDatabase: NoteDatabase = DatabaseBuilder.getInstance(applicationContext)
        presenter = DiaryWriterPresenter(this)
        presenter!!.getAllNoteDatFromDatabase(noteDatabase)
        Log.d("hello", "dd")

    }

//    override fun onBackPressed() {
//        if (!sharedPref!!.isBillPayed() && FacebookAds.interstitialAd!=null) {
//            if (FacebookAds.interstitialAd!!.isAdLoaded) {
//                FacebookAds.interstitialAd!!.show()
//                super.onBackPressed(
//            } else {
//                super.onBackPressed()
//            }
//
//        } else {
//            super.onBackPressed()
//        }
//    }


    override fun onCellClickListener(data: NoteModel) {
        if(data.noteType.equals("simpleNote")) {
            goToWriteNoteActivity(data)
        }
        else if(data.noteType.equals("checkListNote")){
            goToCheckListActivity(data)
        }
    }


    fun openPopup(note:NoteModel,positon: Int){
        val popup = PopupMenu(this, option_btn)

        popup.menuInflater.inflate(R.menu.share_delete_menu, popup.menu)


        popup.setOnMenuItemClickListener { item ->
            if(item.title.equals("Share")){
                shareText(note)
            }
            else{//Restore
                moveToTrash(positon)
            }
            true
        }

        popup.show() //showing popup menu
    }
    fun shareText(note:NoteModel) {
        val path: File = applicationContext.getFilesDir()
        val file = File(path, "Note.txt")
        val stream = FileOutputStream(file)
        try {
            var s="Date:"+note.day+"-"+note.month+"-"+note.year+"\n"
            s=s+"Title:"+note.noteTitle+"\n"
          if(note.noteType.equals("simpleNote")){
              s=s+"Details:"+note.noteContent
          }
            else{
              s=s+note.checkList.toString()
          }

            stream.write(s.toByteArray())
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        finally {
            stream.close()
        }

        val shareIntent = Intent(Intent.ACTION_SEND).apply {
            type ="*/*" //will accepts all types of files, if you want specific then change it on your need.
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(Intent.EXTRA_SUBJECT,"Sharing file from the Diary with Lock")
            putExtra( Intent.EXTRA_TEXT,"This file contain data of one Note")
            val fileURI = FileProvider.getUriForFile(
                applicationContext!!, applicationContext!!.packageName + ".provider", File(file.toString())
            )
            putExtra(Intent.EXTRA_STREAM, fileURI)
        }
        startActivity(shareIntent)
    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.fb_ad)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
       // native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }

    private fun showFacebookNative1() {
        //
        if (FacebookNativeAd.nativeAd1 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd2(this)
            nativeAd = FacebookNativeAd.nativeAd1
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus1.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd2(this)
           hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    private fun showFacebookNative2() {
        //
        if (FacebookNativeAd.nativeAd2 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd1(this)
            nativeAd = FacebookNativeAd.nativeAd2
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
                makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            FacebookNativeAd.loadNativeFacebookAd1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }

    fun makeNonClickAbleAD(){
        if(ad_media!=null) {
            ad_media.isClickable = false
            ad_headline.isClickable = false
            ad_body.isClickable = false
            ad_icon.isClickable = false
        }
    }
    fun makeNonClickAbleFB(){
        native_ad_media.isClickable = false
        native_ad_social_context.isClickable = false
        native_ad_body.isClickable = false
        native_ad_icon.isClickable = false
    }
    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        native_ad_const.visibility=View.GONE
    }
    fun showNativeAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getDiaryWriterActivityNativeAdStatusFB()
                    .equals("true") && sharedPref!!.getDiaryWriterActivityNativeAdStatusAD()
                    .equals("false"))
            {
                if(FacebookNativeAd.selectedAd.equals("ad1")){
                    showFacebookNative1()
                }
                else if (FacebookNativeAd.selectedAd.equals("ad2")){
                    showFacebookNative2()
                }
                selectedNative = "fb"
            }
            else if ((sharedPref!!.getDiaryWriterActivityNativeAdStatusFB()
                    .equals("false") && sharedPref!!.getDiaryWriterActivityNativeAdStatusAD()
                    .equals("true")))
            {
                if(AdmobNativeAd.selectedAd.equals("ad1")){
                    showAdmobNativeAd1()
                }
                else if (AdmobNativeAd.selectedAd.equals("ad2")){
                    showAdmobNativeAd2()
                }
                selectedNative = "ad"
            } else {
                hideNativeAnim()
                selectedNative = "no"
            }
        }
        else{
            hideNativeAnim()
            selectedNative="no"
        }
    }
    fun showAdmobNativeAd1(){
        if (AdmobNativeAd.nativeAd1 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd1
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
          //  makeNonClickAbleAD()

        } else if (AdmobNativeAd.adStatus1.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }

    fun showAdmobNativeAd2(){

        if (AdmobNativeAd.nativeAd2 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd2
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
            //makeNonClickAbleAD()
        } else if (AdmobNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(adloadStatus: AdLoadStatus) {
        if(adloadStatus.isLoaded!!) {
            if(!addStatus!!){
                showNativeAd()

            }
        }
        else{
            if (!addStatus!!){
                hideNativeAnim()
            }
            addStatus=true
            //
            loadNewNativeAd(adloadStatus.idType,adloadStatus.idNumber)
        }
    }

    fun loadNewNativeAd(type:String,idNumber:String){
        if (type.equals("fb")){
            if (idNumber.equals("id1")){
                FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
            }
            else{
                FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
            }
        }
        else if(type.equals("ad")){
            if (idNumber.equals("id1")){
                AdmobNativeAd.loadAdId2(applicationContext)
            }
            else{
                AdmobNativeAd.loadAdId1(applicationContext)
            }
        }
    }
    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd?,
        adView: UnifiedNativeAdView)
    {

        var media=adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }
            
            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun shareTextClickListener(note: NoteModel) {
       shareText(note)
    }

    override fun moveToTrashclickListener(position: Int) {
        moveToTrash(position)
    }
}