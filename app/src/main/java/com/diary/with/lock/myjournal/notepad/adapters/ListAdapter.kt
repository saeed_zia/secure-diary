package com.diary.with.lock.myjournal.notepad.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.interfaces.CellClickListener
import com.diary.with.lock.myjournal.notepad.interfaces.LongClickListener
import com.diary.with.lock.myjournal.notepad.models.NoteModel

import com.diary.with.lock.myjournal.notepad.views.ViewHolder.NoteViewHolder
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*
import kotlin.collections.ArrayList

class ListAdapter(var context: Context, var list: ArrayList<NoteModel>, private val cellClickListener: CellClickListener, private val longClickListener: LongClickListener)
    : RecyclerView.Adapter<NoteViewHolder>(), Filterable {

    var noteFilterList = ArrayList<NoteModel>()
    var mContext:Context?=null
    init {
        noteFilterList = list
        mContext=context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return NoteViewHolder(inflater, parent )
    }
    override fun getItemCount(): Int = noteFilterList.size
    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note: NoteModel = noteFilterList[position]
        holder.bind(note)

        val data = noteFilterList[position]
        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(data)

        }
        holder.itemView.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(view: View?): Boolean {
                openPopup(data,position,holder.itemView.option_btn)
                return true // returning true instead of false, works for me
            }
        })
      holder.itemView.option_btn.setOnClickListener{
          //longClickListener.onLongClickListener(data,position)
          openPopup(data,position,holder.itemView.option_btn)
      }
    }


    fun openPopup(note:NoteModel,positon: Int,view:View){
        val popup = PopupMenu(mContext,view)

        popup.menuInflater.inflate(R.menu.share_delete_menu, popup.menu)


        popup.setOnMenuItemClickListener { item ->
            if(item.title.equals("Share")){
                longClickListener.shareTextClickListener(note)
            }
            else{//Restore
               longClickListener.moveToTrashclickListener(positon)
            }
            true
        }

        popup.show() //showing popup menu
    }
    fun updateUsers(newUsers: ArrayList<NoteModel>) {
        list=newUsers
        //list.addAll(newUsers)
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty())
                {
                    noteFilterList = list
                }
                else
                {
                    val resultList = ArrayList<NoteModel>()
                    for (row in list) {
                        if (row.noteTitle.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT)))
                        {
                            resultList.add(row)
                        }
                    }
                    noteFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = noteFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                noteFilterList = results?.values as ArrayList<NoteModel>
                notifyDataSetChanged()
               // updateUsers(list)

            }

        }
    }
}