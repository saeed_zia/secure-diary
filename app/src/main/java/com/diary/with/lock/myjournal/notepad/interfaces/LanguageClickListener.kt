package com.diary.with.lock.myjournal.notepad.interfaces

import com.diary.with.lock.myjournal.notepad.models.Language


interface LanguageClickListener {
    fun onLanguageClickListener(lang: Language)
}
