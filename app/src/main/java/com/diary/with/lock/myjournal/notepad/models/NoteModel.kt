package com.diary.with.lock.myjournal.notepad.models


import com.google.gson.annotations.SerializedName;

data class NoteModel(
@SerializedName("id")
    val id:Int?,
@SerializedName("noteType")
val noteType:String,
@SerializedName("noteTitle")
    val noteTitle: String,
@SerializedName("noteContent")
    val noteContent: String?,
@SerializedName("addressList")
val addressList:List<String>?,
@SerializedName("checkList")
val checkList:List<CheckListItem>?,
@SerializedName("day")
    val day:String,
@SerializedName("month")
    val month:String,
@SerializedName("year")
    val year:String

)
