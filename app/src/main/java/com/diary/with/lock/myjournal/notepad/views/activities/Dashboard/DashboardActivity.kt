package com.diary.with.lock.myjournal.notepad.views.activities.Dashboard


import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.BuildConfig
import com.diary.with.lock.myjournal.notepad.Database.DatabaseBuilder
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.ColorStyle.ColorStyleActivity
import com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity.DiaryWriterActivity
import com.diary.with.lock.myjournal.notepad.views.activities.FeedbackActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Language.LanguageActivity
import com.diary.with.lock.myjournal.notepad.views.activities.LockSetting.LockSettingActivity
import com.diary.with.lock.myjournal.notepad.views.activities.PrivacyPolicy.PrivacyPolicy
import com.diary.with.lock.myjournal.notepad.views.activities.RateAndReview.RateAndReview
import com.diary.with.lock.myjournal.notepad.views.activities.ReminderActivity.ReminderActivity
import com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity.TrashActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Tutorials.TutorialActivity
import com.diary.with.lock.myjournal.notepad.views.activities.quotation.QuotationActivity
import com.diary.with.lock.myjournal.notepad.views.fragments.BottomSheetFragment
import com.facebook.ads.AdOptionsView
import com.facebook.ads.MediaView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdLayout
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import kotlinx.android.synthetic.main.customize_native_ad_ctr.*

import kotlinx.android.synthetic.main.dashboard_content.*
import kotlinx.android.synthetic.main.loading_fb_native.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.native_ads.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.util.*

class DashboardActivity : BaseActivity(),BillingProcessor.IBillingHandler,DashboardContract.View {

    var sharedPref: SharedPref?=null
    var nativeAd:NativeAd?=null
    var nativeAdLayout:NativeAdLayout?=null
    var adView:ConstraintLayout?=null
    var bp: BillingProcessor? = null
    var bottomSheet:BottomSheetFragment?=null
    var unifiedNativeAd:UnifiedNativeAd?=null
    var selectedNative:String?=null
    var isExitShown:Boolean?=false
    var addStatus:Boolean?=false
    var presenter: DashboardPresenter? = null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    var requestFromResume:Boolean?=null
    var mIdType:String?="first"
    var mIdNumber:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        sharedPref= SharedPref(this)

        loadInterAd()
        setSetting()
       // showNativeAd()

        if(BuildConfig.DEBUG){
            bp = BillingProcessor(this, "", this)
            bp!!.initialize()
        }
        else{
            bp = BillingProcessor(this, getString(R.string.in_app_billing), this)
            bp!!.initialize()
        }

        clickListeners()
        navigationView.itemIconTintList=null
       btn_start.setOnClickListener{

           startActivity(Intent(this, DiaryWriterActivity::class.java))
       }
        btn_quotes.setOnClickListener{

           // startActivity(Intent(this, QuotationActivity::class.java))
            showInterstitialAd()
        }
        btn_tutorial.setOnClickListener{

            var intent=Intent(this, TutorialActivity::class.java)
            intent.putExtra("source","DashboardActivity")
            startActivity(intent)
        }


      //  btn_share.setText("dd")
    }
    override fun onBackPressed() {
     // loadNativeFacebookAdForExit()
       // showDialog()
        if(bottomSheet!=null){
            if(bottomSheet!!.isInLayout){
               // Toast.makeText(this, "In layout", Toast.LENGTH_SHORT).show()
            }
            else{
                showAlertDialog()
            }

        }
        else{
            showAlertDialog()
        }

    }
    fun showAlertDialog(){
     //  if(nativeAd!=null){
          // nativeAd!!.destroy()
      // }
        if(!isExitShown!!){
            isExitShown=true
            bottomSheet = BottomSheetFragment()
            bottomSheet!!.show(supportFragmentManager,"ModalBottomSheet" )

        }
        else{
           // Toast.makeText(this, "wait", Toast.LENGTH_SHORT).show()
                bottomSheet!!.dismiss()
                bottomSheet = BottomSheetFragment()
                bottomSheet!!.show(supportFragmentManager, "ModalBottomSheet")

        }

       // native_ad_const.visibility=View.GONE
    }

    private fun showDialog() {
        var dialog= Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.custom_exit_dialog_box)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val gallery=dialog!!.findViewById<Button>(R.id.btn_exit)
        gallery.setOnClickListener{

                dialog!!.dismiss()

        }
        val camera=dialog!!.findViewById<Button>(R.id.btn_cancel)
        camera.setOnClickListener {

        }
    }
    fun clickListeners() {
        val header: View = navigationView.getHeaderView(0)
        var remove_ad_btn = header.findViewById<Button>(R.id.btn_remove_ad)
        remove_ad_btn.setOnClickListener{
           if(BuildConfig.DEBUG){
               bp!!.purchase(this,"android.test.purchased")
           } //set the package for release
            else{
               bp!!.purchase(this, packageName) } }

        menu_button.setOnClickListener {
            if(drawerLayout.isDrawerOpen(GravityCompat.START)) {
                //drawer is open
                drawerLayout.close()
            }
            else
            {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
        navigationView.setNavigationItemSelectedListener { menuItem ->
            when (menuItem?.itemId) {

                R.id.menu_codelock -> {
                    intent = Intent(applicationContext, LockSettingActivity::class.java)
                    //intent.putExtra("source", "DiaryWriterActivity")
                    startActivity(intent)
                }
                R.id.menu_reminder -> {
                    intent = Intent(applicationContext, ReminderActivity::class.java)
                    startActivity(intent)
                }
                R.id.menu_theme -> {
                    intent = Intent(applicationContext, ColorStyleActivity::class.java)
                    startActivity(intent)
                }
                R.id.menu_restore -> {
                    restoreData()
                }
                R.id.menu_language -> {
                    startActivity(Intent(applicationContext, LanguageActivity::class.java))
                }

                R.id.rate -> {
                    intent = Intent(applicationContext, RateAndReview::class.java)
                    startActivity(intent)
                }
                R.id.share -> {
                    shareApp()
                }
                R.id.privacy_policy -> {
                    startActivity(Intent(applicationContext, PrivacyPolicy::class.java))

                }
                R.id.quit_app -> {
                    this.finishAffinity()
                }
                R.id.trash -> {
                    startActivity(Intent(applicationContext, TrashActivity::class.java))
                }
                R.id.feed_back -> {
                    startActivity(Intent(this, FeedbackActivity::class.java))
                }
                R.id.more_apps->{
                    val uri =Uri.parse(sharedPref!!.getQrIconUrl())
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)
                }
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
        qr_icon.setOnClickListener{
            try {

               Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }
    }
    fun restoreData(){
        if (ContextCompat.checkSelfPermission( this,Manifest.permission.WRITE_EXTERNAL_STORAGE ) == 0)
        {
             fetchNoteListFromBackup()
        }
        else{
            showStoragePermissionDialog()
        }

    }
    private fun showStoragePermissionDialog() {
        var dialog=Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.storage_permission_dialog)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val allow=dialog!!.findViewById<Button>(R.id.btn_allow_access)
        allow.setOnClickListener{
            dialog!!.dismiss()

            requestStoragePermissions()
        }
    }
    fun fetchNoteListFromBackup() {
       try {
           val root = Environment.getExternalStorageDirectory().toString()
           val myFile = File("$root/Maximus/BackupDiary/backup-diary-database")
           if (myFile.exists()) {
               val noteBackupDatabase: NoteDatabase =
                   DatabaseBuilder.getBackupInstance(applicationContext, myFile)
               presenter = DashboardPresenter(this)
               presenter!!.getAllNoteDatFromBackupDatabase(noteBackupDatabase)

               Toast.makeText(this, "Data successfully Restored", Toast.LENGTH_SHORT).show()
           } else {
               Toast.makeText(applicationContext, "Backup file does not exist", Toast.LENGTH_SHORT)
                   .show()
           }

       }
       catch (e:Exception){
           e.printStackTrace()
       }
    }
    fun requestStoragePermissions(){

        val permissions = arrayOf( Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        Permissions.check(this /*context*/, permissions,null /*rationale*/,null /*options*/, object : PermissionHandler() {
            override fun onGranted() {
                fetchNoteListFromBackup()
            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                Toast.makeText(applicationContext, "Storage permission is required", Toast.LENGTH_SHORT).show()
            }
        })

    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.fb_ad)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)



        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
        // native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }

    private fun showFacebookNative1() {
   //
        if (FacebookNativeAd.nativeAd1 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd2(this)
            nativeAd = FacebookNativeAd.nativeAd1
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus1.equals("failed")) {
            addStatus=true
            hideNativeAnim()
            FacebookNativeAd.loadNativeFacebookAd2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    private fun showFacebookNative2() {
        //
        if (FacebookNativeAd.nativeAd2 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd1(this)
            nativeAd = FacebookNativeAd.nativeAd2
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus2.equals("failed")) {
            addStatus=true
            hideNativeAnim()
            FacebookNativeAd.loadNativeFacebookAd1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }

    fun shareApp(){
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "")
            var shareMessage = "\nLet me recommend you this application\n\n"
            shareMessage ="""${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}""".trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }
    }

    fun setSetting(){
        var bgColor=sharedPref?.getBgColor()
        dash_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
        bar_title.setText(getString(R.string.diary_notes))
        if(sharedPref!!.getQrIconStatus().equals("false")){
            qr_icon.visibility=View.GONE
        }
    }

    override fun onBillingInitialized() {

    }

    override fun onResume() {
        super.onResume()
        loading_root.visibility= View.INVISIBLE
        constloademail.visibility= View.INVISIBLE
        setSetting()
        addStatus=false

        showNativeAd()


    }

    override fun onPurchaseHistoryRestored() {

    }

    override fun onProductPurchased(productId: String, details: TransactionDetails?) {
        sharedPref!!.payBillStatus(true)
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {

    }
    override fun onDestroy() {
        if (bp != null) {
            bp!!.release()
        }
        super.onDestroy()
    }
    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        native_ad_const.visibility=View.GONE
    }

    fun showNativeAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getDashboardActivityNativeAdStatusFB()
                    .equals("true") && sharedPref!!.getDashboardActivityNativeAdStatusAD()
                    .equals("false"))
            {
                if(FacebookNativeAd.selectedAd.equals("ad1")){
                    showFacebookNative1()
                }
                else if (FacebookNativeAd.selectedAd.equals("ad2")){
                    showFacebookNative2()
                }
                selectedNative = "fb"
            }
            else if ((sharedPref!!.getDashboardActivityNativeAdStatusFB()
                    .equals("false") && sharedPref!!.getDashboardActivityNativeAdStatusAD()
                    .equals("true")))
            {
                if(AdmobNativeAd.selectedAd.equals("ad1")){
                    showAdmobNativeAd1()
                }
                else if (AdmobNativeAd.selectedAd.equals("ad2")){
                    showAdmobNativeAd2()
                }

                selectedNative = "ad"
            } else {
                hideNativeAnim()
                selectedNative = "no"
            }
        }
        else{
            hideNativeAnim()
            selectedNative="no"
        }
    }
    fun makeNonClickAbleAD(){
        ad_media.isClickable = false
        ad_headline.isClickable = false
        ad_body.isClickable = false
        ad_icon.isClickable = false
    }
    fun makeNonClickAbleFB(){
        native_ad_media.isClickable = false
        native_ad_social_context.isClickable = false
        native_ad_body.isClickable = false
        native_ad_icon.isClickable = false
    }
    fun showAdmobNativeAd1(){

            if (AdmobNativeAd.nativeAd1 != null) {
                addStatus=true
                AdmobNativeAd.loadAdId2(this)
                unifiedNativeAd =AdmobNativeAd.nativeAd1
                val adView = layoutInflater.inflate(
                    R.layout.customize_native_ad, null
                ) as UnifiedNativeAdView

                populateUnifiedNativeAdView(unifiedNativeAd, adView)
                admob_ad.removeAllViews()
                admob_ad.addView(adView)
                //remove animation
                tv_ad_loading.visibility = View.GONE
                pb_ad_loading.visibility = View.GONE
               // makeNonClickAbleAD()
                //Toast.makeText(this, "1", Toast.LENGTH_SHORT).show()
            } else if (AdmobNativeAd.adStatus1.equals("failed")) {
                hideNativeAnim()
                addStatus=true
                AdmobNativeAd.loadAdId2(this)
            } else {
                //trying to load
                Log.d("ddd", "dd")
            }
    }
    fun showAdmobNativeAd2(){

        if (AdmobNativeAd.nativeAd2 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
            unifiedNativeAd =
                AdmobNativeAd.nativeAd2
            val adView = layoutInflater.inflate(
                R.layout.customize_native_ad, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
           // makeNonClickAbleAD()
            //Toast.makeText(this, "2", Toast.LENGTH_SHORT).show()
        } else if (AdmobNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId1(
                this
            )
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(adloadStatus: AdLoadStatus) {
        if(adloadStatus.isLoaded!!) {
           if(!addStatus!!){
               mIdType=adloadStatus.idType
               mIdNumber=adloadStatus.idNumber
               showNativeAd()
           }
        }
        else{
          if (!addStatus!!){
              hideNativeAnim()
          }
            addStatus=true
             loadNewNativeAd(adloadStatus.idType,adloadStatus.idNumber)
        }
    }

fun loadNewNativeAd(type:String,idNumber:String){
    if (type.equals("fb")){
        if (idNumber.equals("id1")){
            FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
        }
        else{
            FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
        }
    }
    else if(type.equals("ad")){
        if (idNumber.equals("id1")){
            AdmobNativeAd.loadAdId2(
                applicationContext
            )
        }
        else{
            AdmobNativeAd.loadAdId1(
                applicationContext
            )
        }
    }
}
    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd?,
        adView: UnifiedNativeAdView )
    {

        var media=adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }

            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun showAllNoteData(note_list: List<Note>) {
        val noteDatabase: NoteDatabase = DatabaseBuilder.getInstance(applicationContext)
        presenter = DashboardPresenter(this)
        presenter?.saveAllDataInDatabase(noteDatabase, note_list)
    }
    fun showInterstitialAd(){
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()


            } else {
                startActivity(Intent(this,QuotationActivity::class.java))
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            startActivity(Intent(this, QuotationActivity::class.java))
        }
    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {

            if (sharedPref!!.getQuotationActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getQuotationActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getQuotationActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getQuotationActivityInterAdStatusAD()
                    .equals("true")
            ) {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }

    fun showInterstitialFb(){

        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                //finish()
                startActivity(Intent(this,
                    QuotationActivity::class.java))
            },  sharedPref!!.getProgLoginInter())


        } else {
            //finish()
            startActivity(Intent(this,
                QuotationActivity::class.java))
        }
    }
    fun showInterstitialAdmob(){
        // Toast.makeText(this, sharedPref!!.getProgLoginInter().toString(), Toast.LENGTH_SHORT).show()
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.madMobShoeClose(this, Intent(this, QuotationActivity::class.java))

            },  sharedPref!!.getProgQuotation())
        }
        else{
            finish()
            startActivity(Intent(this, QuotationActivity::class.java))
        }
    }

}