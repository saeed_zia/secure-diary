package com.diary.with.lock.myjournal.notepad.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener
import com.diary.with.lock.myjournal.notepad.models.CheckListItem
import kotlinx.android.synthetic.main.to_do_item.view.*


class CheckListAdapter(private val list: ArrayList<CheckListItem>, val checkBoxClickListener: CheckBoxClickListener, textSize:Float,focusPosition: Int) : RecyclerView.Adapter<CheckListAdapter.ViewHolder>() {
       var checkList= arrayListOf<CheckListItem>()
       var mTextSize:Float?=null
       var mFocusPosition:Int?=null
      // var count:Int?=null
init {
    checkList=list
    mTextSize=textSize
    mFocusPosition=focusPosition

}
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckListAdapter.ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.to_do_item, parent, false)
        return CheckListAdapter.ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CheckListAdapter.ViewHolder, position: Int) {

        holder.bindItems(checkList?.get(position)!!,position+1)
        holder.itemView.et_text.textSize = mTextSize!!
        if(mFocusPosition==position){
            holder.itemView.et_text.requestFocus()
        }
        holder.itemView.checkbox
            .setOnCheckedChangeListener(
                CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    if(holder.itemView.et_text.text.toString().equals("")){
                        holder.itemView.checkbox.isChecked=false
                    }
                    else{
                        checkBoxClickListener.onCheckBoxClickListener(isChecked,position)
                    }

                })

        holder.itemView.et_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkBoxClickListener.onAfterTextChangeListener(s.toString(),position)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
//        holder.itemView.et_text.setOnEditorActionListener { v, actionId, event ->
//            var handled = false
//            if (actionId == EditorInfo.IME_ACTION_SEND) {
//                // sendMessage()
//            }
//                handled = true
//            handled
//        }

        holder.itemView.et_text.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.getAction() === KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                           checkBoxClickListener.onEnterPressed(position)
                            mFocusPosition=position+1
                           // holder.itemView.et_text.requestFocus()
                           // return true
                        }
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_DEL -> {
                           if (holder.itemView.et_text.text!!.toString().equals("")) {
                               checkBoxClickListener.deleteCheckItem(position)
                           }
                        }
                    }
                }

                return false
            }
        })
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return checkList!!.size
    }
    fun updateCheckList(newCheckList1: ArrayList<CheckListItem>) {
        //checkList!!.clear()
        checkList=newCheckList1
        notifyDataSetChanged()
    }
   // fun addNewCheckItem(){
       // checkList!!.add(CheckListItem(false,""))
      //  notifyDataSetChanged()
  //  }
    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(checkListItem: CheckListItem,count:Int) {
            val checkBox=itemView.findViewById<CheckBox>(R.id.checkbox)
            val checkText=itemView.findViewById<EditText>(R.id.et_text)
            val count_tv=itemView.findViewById<TextView>(R.id.tv_count)
            try{
                checkText.setText(checkListItem.text)
                checkBox.isChecked=checkListItem.checkStatus
               // checkText.requestFocus()
                count_tv.setText(count.toString())
            }
            catch (e:Exception){
                e.printStackTrace()
            }

        }
    }
    fun requestFcus(){

    }
}