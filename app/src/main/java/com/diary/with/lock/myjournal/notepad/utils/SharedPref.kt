package com.diary.with.lock.myjournal.notepad.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.ContextCompat.getColor
import com.diary.with.lock.myjournal.notepad.R



class  SharedPref(private val context : Context) {
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "password"
    var mContext: Context? = context

    val sharedPref: SharedPreferences =  mContext?.getSharedPreferences(PREF_NAME, PRIVATE_MODE)!!

    fun getPassword():String{
      val password=sharedPref.getString("userPassword","null")
        return password!!
    }
    fun setPassword( password:String){
        val editor = sharedPref.edit()
        editor.putString("userPassword", password)
        editor.apply()
    }
    fun getAnswer1():String{
       var ans1= sharedPref.getString("Answer1","null")!!
        return ans1
    }
    fun getAnswer2():String{
        var ans2=sharedPref.getString("Answer2","null")!!
        return ans2
    }
    fun setAnswers(ans1:String,ans2:String){
        val editor = sharedPref.edit()
        editor.putString("Answer1", ans1)
        editor.putString("Answer2",ans2)
        editor.apply()
    }
    fun getBgColor():Int{
        var col=sharedPref.getInt("bgColor", getColor(context, R.color.colorPrimary))!!
        return col
    }
    fun setBgColor(c:Int){
        val editor = sharedPref.edit()
        editor.putInt("bgColor",c)
        editor.apply()
    }
    fun setTextSize(s:Float){
        val editor = sharedPref.edit()
        editor.putFloat("textSize",s)
        editor.apply()
    }
    fun getTextSize():Float{
        var s=sharedPref.getFloat("textSize",18.0f)!!
        return s
    }
    fun setLanguage(lang:String){
        val editor = sharedPref.edit()
        editor.putString("Language",lang)
        editor.apply()

    }
    fun getLanguage():String{
        return sharedPref.getString("Language","en")!!

    }
    fun getPermissionStatus():Boolean{
       return sharedPref.getBoolean("permission",false)
    }
    fun setPermissionStatus(){
        val editor = sharedPref.edit()
        editor.putBoolean("permission",true)
        editor.apply()
    }
    fun setTutorialStatua(){
        val editor = sharedPref.edit()
        editor.putBoolean("tutorial",true)
        editor.apply()
    }
   fun getTutorialStatus():Boolean{
       return sharedPref.getBoolean("tutorial",false)
   }
    fun setFont(font:String){
        val editor = sharedPref.edit()
        editor.putString("font",font)
        editor.apply()
    }
    fun getFont():String{
        return sharedPref.getString("font","fonts/roboto_regular.ttf")!!
    }

    fun isBillPayed():Boolean{
        return sharedPref.getBoolean("BillPay",false)!!
    }
    fun payBillStatus(billStatus:Boolean){
        val editor = sharedPref.edit()
        editor.putBoolean("BillPay",billStatus)
        editor.apply()
    }
    fun isPrivacyPolicyAccepted():Boolean{
        return sharedPref.getBoolean("privacyPolicyStatus",false)!!
    }
    fun privacyPolicyAccepted(){
        val editor = sharedPref.edit()
        editor.putBoolean("privacyPolicyStatus",true)
        editor.apply()
    }
    fun getAdId():String{
        return sharedPref.getString("AdId","idDefault value")!!
    }
    fun setAdId(id:String){
        val editor = sharedPref.edit()
        editor.putString("AdId",id)
        editor.apply()
    }
    fun isUsePassword():Boolean{
        return sharedPref.getBoolean("isUsePassword",true)!!
    }
    fun setUsePassword(status:Boolean){
        val editor = sharedPref.edit()
        editor.putBoolean("isUsePassword",status)
        editor.apply()
    }
    fun isPasswordSetup():Boolean{
        return sharedPref.getBoolean("isPasswordSetup",false)!!
    }
    fun setPasswordSetupStatus(status: Boolean){
        val editor = sharedPref.edit()
        editor.putBoolean("isPasswordSetup",status)
        editor.apply()
    }

    //
   fun setcheckListActivityAdStatus(status: String){
        val editor = sharedPref.edit()
        editor.putString("CheckList",status)
        editor.apply()
    }
    fun getcheckListActivityAdStatus():String{
        return sharedPref.getString("CheckList","false")!!
    }
    fun setColorStyleActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ColorStyleInterAD",status)
        editor.apply()
    }
    fun getColorStyleActivityInterAdStatusAD():String{
        return sharedPref.getString("ColorStyleInterAD","false")!!
    }
    fun setColorStyleActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ColorStyleInterFB",status)
        editor.apply()
    }
    fun getColorStyleActivityInterAdStatusFB():String{
        return sharedPref.getString("ColorStyleInterFB","false")!!
    }

    fun setDiaryWriterActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("DiaryWriterAD",status)
        editor.apply()
    }
    fun getDiaryWriterActivityNativeAdStatusAD():String{
        return sharedPref.getString("DiaryWriterAD","false")!!
    }
    fun setDiaryWriterActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("DiaryWriterFB",status)
        editor.apply()
    }
    fun getDiaryWriterActivityNativeAdStatusFB():String{
        return sharedPref.getString("DiaryWriterFB","false")!!
    }
    fun setLanguageActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("LanguageFB",status)
        editor.apply()
    }
    fun getLanguageActivityNativeAdStatusFB():String{
        return sharedPref.getString("LanguageFB","false")!!
    }
    fun setLanguageActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("LanguageAD",status)
        editor.apply()
    }
    fun getLanguageActivityNativeAdStatusAD():String{
        return sharedPref.getString("LanguageAD","false")!!
    }
    fun setLanguageActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("LanguageInterAD",status)
        editor.apply()
    }
    fun getLanguageActivityInterAdStatusAD():String{
        return sharedPref.getString("LanguageInterAD","false")!!
    }
    fun setLanguageActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("LanguageInterFB",status)
        editor.apply()
    }
    fun getLanguageActivityInterAdStatusFB():String{
        return sharedPref.getString("LanguageInterFB","false")!!
    }
    fun setLockSettingActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("LockSettingFB",status)
        editor.apply()
    }
    fun getLockSettingActivityNativeAdStatusFB():String{
        return sharedPref.getString("LockSettingFB","false")!!
    }
    fun setLockSettingActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("LockSettingAD",status)
        editor.apply()
    }
    fun getLockSettingActivityNativeAdStatusAD():String{
        return sharedPref.getString("LockSettingAD","false")!!
    }
    fun setLoginActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("LoginInterFB",status)
        editor.apply()
    }
    fun getLoginActivityInterAdStatusFB():String{
        return sharedPref.getString("LoginInterFB","false")!!
    }
    fun setLoginActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("LoginInterAD",status)
        editor.apply()
    }
    fun getLoginActivityInterAdStatusAD():String{
        return sharedPref.getString("LoginInterAD","false")!!
    }
    fun setPermissionActivityAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("PermissionFB",status)
        editor.apply()
    }
    fun getPermissionActivityAdStatusFB():String{
        return sharedPref.getString("PermissionFB","false")!!
    }
    fun setPermissionActivityAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("PermissionAD",status)
        editor.apply()
    }
    fun getPermissionActivityAdStatusAD():String{
        return sharedPref.getString("PermissionAD","false")!!
    }
    fun setPrivacyActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("PrivacyInterFB",status)
        editor.apply()
    }
    fun getPrivacyActivityInterAdStatusFB():String{
        return sharedPref.getString("PrivacyInterFB","false")!!
    }
    fun setPrivacyActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("PrivacyInterAD",status)
        editor.apply()
    }
    fun getPrivacyActivityInterAdStatusAD():String{
        return sharedPref.getString("PrivacyInterAD","false")!!
    }
    fun setQuotationActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("QuotationFB",status)
        editor.apply()
    }
    fun getQuotationActivityNativeAdStatusFB():String{
        return sharedPref.getString("QuotationFB","false")!!
    }
    fun setQuotationActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("QuotationAD",status)
        editor.apply()
    }
    fun getQuotationActivityNativeAdStatusAD():String{
        return sharedPref.getString("QuotationAD","false")!!
    }
    fun setQuotationActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("QuotationInterFB",status)
        editor.apply()
    }
    fun getQuotationActivityInterAdStatusFB():String{
        return sharedPref.getString("QuotationInterFB","false")!!
    }
    fun setQuotationActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("QuotationInterAD",status)
        editor.apply()
    }
    fun getQuotationActivityInterAdStatusAD():String{
        return sharedPref.getString("QuotationInterAD","false")!!
    }
    fun setReminderActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ReminderFB",status)
        editor.apply()
    }
    fun getReminderActivityNativeAdStatusFB():String{
        return sharedPref.getString("ReminderFB","false")!!
    }
    fun setReminderActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ReminderAD",status)
        editor.apply()
    }
    fun getReminderActivityNativeAdStatusAD():String{
        return sharedPref.getString("ReminderAD","false")!!
    }
    fun setReminderActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ReminderInterFB",status)
        editor.apply()
    }
    fun getReminderActivityInterAdStatusFB():String{
        return sharedPref.getString("ReminderInterFB","false")!!
    }
    fun setReminderActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ReminderInterAD",status)
        editor.apply()
    }
    fun getReminderActivityInterAdStatusAD():String{
        return sharedPref.getString("ReminderInterAD","false")!!
    }
    fun setThankYouActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ThankYouInterFB",status)
        editor.apply()
    }
    fun getThankYouActivityInterAdStatusFB():String{
        return sharedPref.getString("ThankYouInterFB","false")!!
    }
    fun setThankYouActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ThankYouInterAD",status)
        editor.apply()
    }
    fun getThankYouActivityInterAdStatusAD():String{
        return sharedPref.getString("ThankYouInterAD","false")!!
    }
    fun setTrashActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("TrashFB",status)
        editor.apply()
    }
    fun getTrashActivityNativeAdStatusFB():String{
        return sharedPref.getString("TrashFB","false")!!
    }
    fun setTrashActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("TrashAD",status)
        editor.apply()
    }
    fun getTrashActivityNativeAdStatusAD():String{
        return sharedPref.getString("TrashAD","false")!!
    }
    fun setTrashActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("TrashInterFB",status)
        editor.apply()
    }
    fun getTrashActivityInterAdStatusFB():String{
        return sharedPref.getString("TrashInterFB","false")!!
    }
    fun setTrashActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("TrashInterAD",status)
        editor.apply()
    }
    fun getTrashActivityInterAdStatusAD():String{
        return sharedPref.getString("TrashInterAD","false")!!
    }
    fun setTutorialActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("TutorialFB",status)
        editor.apply()
    }
    fun getTutorialActivityNativeAdStatusFB():String{
        return sharedPref.getString("TutorialFB","false")!!
    }
    fun setTutorialActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("TutorialAD",status)
        editor.apply()
    }
    fun getTutorialActivityNativeAdStatusAD():String{
        return sharedPref.getString("TutorialAD","false")!!
    }
    fun setTutorialActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("TutorialInterFB",status)
        editor.apply()
    }
    fun getTutorialActivityInterAdStatusFB():String{
        return sharedPref.getString("TutorialInterFB","false")!!
    }
    fun setTutorialActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("TutorialInterAD",status)
        editor.apply()
    }
    fun getTutorialActivityInterAdStatusAD():String{
        return sharedPref.getString("TutorialInterAD","false")!!
    }
    fun setDashboardActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("DashboardFB",status)
        editor.apply()
    }
    fun getDashboardActivityNativeAdStatusFB():String{
        return sharedPref.getString("DashboardFB","false")!!
    }
    fun setDashboardActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("DashboardAD",status)
        editor.apply()
    }
    fun getDashboardActivityNativeAdStatusAD():String{
        return sharedPref.getString("DashboardAD","false")!!
    }
    fun setFeedbackActivityInterAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("FeedbackInterFB",status)
        editor.apply()
    }
    fun getFeedbackActivityInterAdStatusFB():String{
        return sharedPref.getString("FeedbackInterFB","false")!!
    }
    fun setFeedbackActivityInterAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("FeedbackInterAD",status)
        editor.apply()
    }
    fun getFeedbackActivityInterAdStatusAD():String{
        return sharedPref.getString("FeedbackInterAD","false")!!
    }
    fun setBottomSheetActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("BottomSheetFB",status)
        editor.apply()
    }
    fun getBottomSheetActivityNativeAdStatusFB():String{
        return sharedPref.getString("BottomSheetFB","false")!!
    }
    fun setBottomSheetActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("BottomSheetAD",status)
        editor.apply()
    }
    fun getBottomSheetActivityNativeAdStatusAD():String{
        return sharedPref.getString("BottomSheetAD","false")!!
    }

    fun setSplashNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("SplashAD",status)
        editor.apply()
    }
    fun getSplashNativeAdStatusAD():String{
        return sharedPref.getString("SplashAD","false")!!
    }

    fun setSplashNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("SplashFB",status)
        editor.apply()
    }
    fun getSplashNativeAdStatusFB():String{
        return sharedPref.getString("SplashFB","false")!!
    }
    fun setStorageActivityNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("StorageFB",status)
        editor.apply()
    }
    fun getStorageActivityNativeAdStatusFB():String{
        return sharedPref.getString("StorageFB","false")!!
    }
    fun setStorageActivityNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("StorageAD",status)
        editor.apply()
    }
    fun getStorageActivityNativeAdStatusAD():String{
        return sharedPref.getString("StorageAD","false")!!
    }

    fun setColorStyleNativeAdStatusAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ColorStyleAD",status)
        editor.apply()
    }
    fun getColorStyleNativeAdStatusAD():String{
        return sharedPref.getString("ColorStyleAD","false")!!
    }
    fun setColorStyleNativeAdStatusFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ColorStyleFB",status)
        editor.apply()
    }
    fun getColorStyleNativeAdStatusFB():String{
        return sharedPref.getString("ColorStyleFB","false")!!
    }

    fun setLockSettingInterFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("LockSettingInterFB",status)
        editor.apply()
    }
    fun getLockSettingInterFB():String{
        return sharedPref.getString("LockSettingInterFB","false")!!
    }
    fun setLockSettingInterAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("LockSettingInterAD",status)
        editor.apply()
    }
    fun getLockSettingInterAD():String{
        return sharedPref.getString("LockSettingInterAD","false")!!
    }

    fun setWriteNoteInterAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("WriteNoteInterAD",status)
        editor.apply()
    }
    fun getWriteNoteInterAD():String{
        return sharedPref.getString("WriteNoteInterAD","false")!!
    }

    fun setWriteNoteInterFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("WriteNoteInterFB",status)
        editor.apply()
    }
    fun getWriteNoteInterFB():String{
        return sharedPref.getString("WriteNoteInterFB","false")!!
    }
    fun setChecklistInterAD(status: String){
        val editor = sharedPref.edit()
        editor.putString("ChecklistInterAD",status)
        editor.apply()
    }
    fun getChecklistInterAD():String{
        return sharedPref.getString("ChecklistInterAD","false")!!
    }

    fun setChecklistInterFB(status: String){
        val editor = sharedPref.edit()
        editor.putString("ChecklistInterFB",status)
        editor.apply()
    }
    fun getChecklistInterFB():String{
        return sharedPref.getString("ChecklistInterFB","false")!!
    }

    fun setAllNativeStatus(status: String){
        val editor = sharedPref.edit()
        editor.putString("StatusOfAllNative",status)
        editor.apply()
    }
    fun getAllNativeStatus():String{
        return sharedPref.getString("StatusOfAllNative","false")!!
    }


//
    fun setProgLoginInter(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgLoginInter",time)
        editor.apply()
    }
     fun getProgLoginInter():Long{
         return sharedPref.getLong("ProgLoginInter",2000)!!
     }
    fun setProgChecklist(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgChecklist",time)
        editor.apply()
    }
    fun getProgChecklist():Long{
        return sharedPref.getLong("",2000)!!
    }
    fun setProgColorStyle(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgColorStyle",time)
        editor.apply()
    }
    fun getProgColorStyle():Long{
        return sharedPref.getLong("ProgColorStyle",2000)!!
    }
    fun setProgLanguage(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgLanguage",time)
        editor.apply()
    }
    fun getProgLanguage():Long{
        return sharedPref.getLong("ProgLanguage",2000)!!
    }
    fun setProgLockSetting(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgLockSetting",time)
        editor.apply()
    }
    fun getProgLockSetting():Long{
        return sharedPref.getLong("ProgLockSetting",2000)!!
    }
    fun setProgPrivacyPolicy(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgPrivacyPolicy",time)
        editor.apply()
    }
    fun getProgPrivacyPolicy():Long{
        return sharedPref.getLong("ProgPrivacyPolicy",2000)!!
    }
    fun setProgQuotation(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgQuotation",time)
        editor.apply()
    }
    fun getProgQuotation():Long{
        return sharedPref.getLong("ProgQuotation",2000)!!
    }
    fun setProgRateAndReview(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgRateAndReview",time)
        editor.apply()
    }
    fun getProgRateAndReview():Long{
        return sharedPref.getLong("ProgRateAndReview",2000)!!
    }
    fun setProgReminder(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgReminder",time)
        editor.apply()
    }
    fun getProgReminder():Long{
        return sharedPref.getLong("ProgReminder",2000)!!
    }
    fun setProgThankyou(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgThankyou",time)
        editor.apply()
    }
    fun getProgThankyou():Long{
        return sharedPref.getLong("ProgThankyou",2000)!!
    }
    fun setProgTrash(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgTrash",time)
        editor.apply()
    }
    fun getProgTrash():Long{
        return sharedPref.getLong("ProgTrash",2000)!!
    }
    fun setProgTutorial(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgTutorial",time)
        editor.apply()
    }
    fun getProgTutorial():Long{
        return sharedPref.getLong("ProgTutorial",2000)!!
    }
    fun setProgFeedback(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgFeedback",time)
        editor.apply()
    }
    fun getProgFeedback():Long{
        return sharedPref.getLong("ProgFeedback",2000)!!
    }

    fun setProgWriteNote(time: Long){
        val editor = sharedPref.edit()
        editor.putLong("ProgWriteNote",time)
        editor.apply()
    }
    fun getProgWriteNote():Long{
        return sharedPref.getLong("ProgWriteNote",2000)!!
    }



//quotation reference icon

    fun setQrIconStatus(status:String){
    val editor = sharedPref.edit()
    editor.putString("QrIconStatus",status)
    editor.apply()
    }
    fun getQrIconStatus():String{
      return sharedPref.getString("QrIconStatus","false")!!
    }

    fun setQrIconURL(icon:String){
        val editor = sharedPref.edit()
        editor.putString("QrIconUrl",icon)
        editor.apply()
    }

    fun getQrIconUrl():String{
        return sharedPref.getString("QrIconUrl","https://play.google.com/store/apps/details?id=com.qr.barcodes.scanner.reader.generator.productivity")!!
    }

}