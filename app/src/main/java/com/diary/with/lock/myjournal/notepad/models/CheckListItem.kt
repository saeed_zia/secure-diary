package com.diary.with.lock.myjournal.notepad.models

data class CheckListItem(var checkStatus:Boolean, var text:String)