package com.diary.with.lock.myjournal.notepad.views.activities.Dashboard

import android.util.Log
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DashboardPresenter (view:DashboardContract.View):DashboardContract.BackupPresenter{

    var mView=view
    override fun getAllNoteDatFromBackupDatabase(appDatabase: NoteDatabase) {
        var list = listOf<Note>()
        try {
            GlobalScope.launch(Dispatchers.Default) {
                list = appDatabase.noteDao().getAll()
                mView.showAllNoteData(list)
            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun saveAllDataInDatabase(appDatabase: NoteDatabase, note_list: List<Note>) {
        GlobalScope.launch (Dispatchers.Main){
            try {
                appDatabase.noteDao().insertAllNotes(note_list)
            }
            catch (e:Exception){
                Log.d("get hello",e.toString())
            }
        }
    }


}