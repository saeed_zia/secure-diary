package com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity


import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase


interface DiaryWriterContract {

    interface View{

        fun showAllNoteData(note_list: List<Note>)
    }

    interface Presenter{
        fun getAllNoteDatFromDatabase( appDatabase: NoteDatabase)
        //fun deleteNoteFromDatabase(appDatabase: NoteDatabase,noteId:Int)
        fun moveNoteToTrash(appDatabase: NoteDatabase,noteId:Int)
        //
        fun moveNoteToTrashInBackup(appDatabase: NoteDatabase,noteId:Int)
    }

}