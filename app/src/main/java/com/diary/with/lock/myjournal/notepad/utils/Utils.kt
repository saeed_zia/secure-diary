package com.diary.with.lock.myjournal.notepad.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.widget.Toast


class Utils {
companion object{

    var idType:String="ad"
    var idNumber:String="id1"
    fun showToastCenter(context: Context,text:String){
        val toast = Toast.makeText(context, text, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.show()
    }

    fun openQrReference(context: Context){
        var sharedPref=SharedPref(context)
        var pkgName=sharedPref.getQrIconUrl()

        val uri: Uri = Uri.parse("market://details?id=$pkgName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK )
        try {
            context.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
              e.printStackTrace()
            context.startActivity( Intent(  Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$pkgName")))
        }
    }
}


}