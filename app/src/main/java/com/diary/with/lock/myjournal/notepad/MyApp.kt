package com.diary.with.lock.myjournal.notepad

import android.app.Application
import android.content.res.Configuration
import android.content.res.Resources
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.widget.Toast
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.google.GoogleEmojiProvider
import java.util.*


 class MyApp: Application() {
  private var locale: Locale? = null
  var sharedPref: SharedPref?=null



  override fun onCreate() {
    super.onCreate()
    EmojiManager.install(GoogleEmojiProvider())
    sharedPref=SharedPref(applicationContext)
   // setLocale()


    val builder = VmPolicy.Builder()
    StrictMode.setVmPolicy(builder.build())
  }

  override fun onConfigurationChanged(newConfig: Configuration) {
    super.onConfigurationChanged(newConfig)
   // setLocale()
//        if (locale != null) { Locale.setDefault(locale)
//            val config = Configuration(newConfig)
//            config.locale = locale
//            baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
//        }
  }
  fun changeLang(lang: String) {
    val config =baseContext.resources.configuration
    if ("" != lang && config.locale.language != lang) {
      sharedPref!!.setLanguage(lang)
      locale = Locale(lang)
      Locale.setDefault(locale)
      val conf = Configuration(config)
      conf.locale = locale
      baseContext.resources .updateConfiguration(conf, baseContext.resources.displayMetrics)
      Toast.makeText(applicationContext, "change", Toast.LENGTH_SHORT).show()
    }
  }

  fun getLang(): String? {
    return sharedPref!!.getLanguage()
  }

  //    fun changeFontStyle(id:Int){
//        try {
//            TypefaceUtil.overrideFont(getApplicationContext(), "aguafina_script", "fonts/aguafina_script.ttf");
//        }
//        catch (e:Exception){
//            e.printStackTrace()
//        }
//
//    }
  fun setLocale() {
    val resources: Resources = resources
    val configuration: Configuration = resources.getConfiguration()
    val locale: Locale = Locale.JAPAN
    if (configuration.locale != locale) {
      configuration.setLocale(locale)
      resources.updateConfiguration(configuration, null)
    }
  }


  fun getLocale():Locale{
    // return Locale(sharedPref!!.getLanguage())
    return Locale("it")
  }


}