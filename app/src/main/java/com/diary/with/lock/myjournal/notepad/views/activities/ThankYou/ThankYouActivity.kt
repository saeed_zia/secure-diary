package com.diary.with.lock.myjournal.notepad.views.activities.ThankYou

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_thank_you.*
import kotlinx.android.synthetic.main.loading_layout.*

class ThankYouActivity : BaseActivity() {
    var sharedPref: SharedPref?=null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thank_you)

        sharedPref=SharedPref(applicationContext)
        loadInterAd()
        var rating=intent.getFloatExtra("rating",1f)
        tv_rating.setText(rating.toString())

        ok.setOnClickListener{
          showInterstitialAd()
       }

    }


    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getThankYouActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getThankYouActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getThankYouActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getThankYouActivityInterAdStatusAD()
                    .equals("true"))
            {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }
    fun showInterstitialAd(){
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                finish()
                startActivity(Intent(this,
                    DashboardActivity::class.java))

            },  sharedPref!!.getProgThankyou())


        } else {
            finish()
            startActivity(Intent(this,
                DashboardActivity::class.java))
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility= View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShoeClose(this, Intent(this, DashboardActivity::class.java))

            },  sharedPref!!.getProgThankyou())
        }
        else{
            finish()
            startActivity(Intent(this, DashboardActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        var bgColor=sharedPref?.getBgColor()
        thank_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
    }
}