package com.diary.with.lock.myjournal.notepad.interfaces

interface TrashListeners {
    fun deleteNoteListener(position:Int)
    fun restoreNoteListener(id:Int,position: Int)
}