package com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

import com.diary.with.lock.myjournal.notepad.utils.SharedPref

import java.util.*


open class BaseActivity : AppCompatActivity() {
    var sp: SharedPref? = null
    private var mCurrentLocale1: Locale? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sp = SharedPref(applicationContext)
        setLocale1(sp?.getLanguage()!!)

    }
    override fun onRestart() {
        super.onRestart()
        val locale = getLocale()
        if (!locale.equals(mCurrentLocale1)) {
            mCurrentLocale1 = locale
            recreate()

        }
    }

    fun getLocale(): Locale {
        return Locale(sp!!.getLanguage())

    }

    fun setLocale1(lang: String) {
        Log.d("checklanguageInbase", lang.toString())
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
        mCurrentLocale1 = myLocale

    }
}