package com.diary.with.lock.myjournal.notepad.models

import com.google.gson.annotations.SerializedName

data class CheckListModel(
    @SerializedName("id")
    val id:Int?,
    @SerializedName("checkTitle")
    val noteTitle: String,

    @SerializedName("taskList")
    val addressList:List<CheckListItem>,
    @SerializedName("day")
    val day:String,
    @SerializedName("month")
    val month:String,
    @SerializedName("year")
    val year:String

)