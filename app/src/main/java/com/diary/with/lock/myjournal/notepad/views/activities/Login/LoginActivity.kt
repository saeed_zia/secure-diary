package com.diary.with.lock.myjournal.notepad.views.activities.Login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View.OnFocusChangeListener
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import com.diary.with.lock.myjournal.notepad.views.activities.SecurityQuestion.SecurityQuestionActivity
import kotlinx.android.synthetic.main.activity_login.*
import me.aflak.libraries.callback.FingerprintDialogCallback
import me.aflak.libraries.dialog.FingerprintDialog


class LoginActivity : BaseActivity(),FingerprintDialogCallback,TextWatcher {

    var sharedPref: SharedPref?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
//



        sharedPref=SharedPref(applicationContext)
        setSetting()
        setClickListener()

      // var id= sharedPref!!.getAdId()
       // Toast.makeText(applicationContext, id, Toast.LENGTH_LONG).show()
}

    fun setClickListener(){
        animation_view.setOnClickListener{
            if(FingerprintDialog.isAvailable(this)) {
                FingerprintDialog.initialize(this)
                    .title(getString(R.string.finger_print_scan))
                    .message(getString(R.string.please_scan_your))
                    .callback(this)
                    .show()
            }
        }
        enter_pasd.setHint("******")

       enter_pasd.setOnClickListener{
           sv.scrollTo(0, sv.getBottom())
       }
        enter_pasd.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                enter_pasd.setHint("")
                sv.scrollTo(0, sv.getBottom())
            }
            else{
                enter_pasd.setHint("******")
            }
        })
        enter_pasd.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
               if(enter_pasd.text.toString().length>0){
                   enter_paswd.setError(null)
               }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        login_btn.setOnClickListener {
            var paswd = enter_pasd.text.toString()
            if (paswd.length >2) {
                if (sharedPref!!.getPassword().equals(paswd)) {
                   finish()
                   startActivity(Intent(this, DashboardActivity::class.java))

                } else {
                    enter_paswd.setError(getString(R.string.invalid_password))
                    sv.scrollTo(0, sv.getBottom())
                }
            }
            else{
                sv.scrollTo(0, sv.getBottom())
                enter_paswd.setError(getString(R.string.invalid_password))
            }
        }
        forget_password.setOnClickListener {
            intent=Intent(this, SecurityQuestionActivity::class.java)
            intent.putExtra("source","LoginActivity")
            startActivity(intent)
            //loadFbAds()

        }
    }



    fun setSetting(){
       // enter_pasd.setTransformationMethod(PasswordTransformationMethod.getInstance())
        var bgColor=sharedPref?.getBgColor()
        login_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()

    }

    override fun onAuthenticationSucceeded() {
        finish()
        startActivity(Intent(this, DashboardActivity::class.java))

    }

    override fun onAuthenticationCancel() {
        enter_pasd.requestFocus()
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//       if(count==0){
//        error(R.string.cannot_be_blank)
//       }
    }


}