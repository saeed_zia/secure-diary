package com.diary.with.lock.myjournal.notepad.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.interfaces.QuotationListeners
import com.diary.with.lock.myjournal.notepad.models.QuoteModel

import kotlinx.android.synthetic.main.quote_item.view.*
import java.util.*


class QuotationAdapter(var quoteArrayList: ArrayList<QuoteModel>, private val quotationListeners: QuotationListeners) : RecyclerView.Adapter<QuotationAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.quote_item, parent, false)
        return QuotationAdapter.ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: QuotationAdapter.ViewHolder, position: Int) {
        holder.bindItems(quoteArrayList[position])
        holder.itemView.image_copy.setOnClickListener{
           // Toast.makeText(, "Copied", Toast.LENGTH_SHORT).show()
         quotationListeners.onCopyClickListener(holder.itemView.tv_quote_author.text.toString(),holder.itemView.tv_quote_text.text.toString())
        }
        holder.itemView.image_share.setOnClickListener{
         quotationListeners.onShareClickListener(holder.itemView.tv_quote_author.text.toString(),holder.itemView.tv_quote_text.text.toString())
        }
        holder.itemView.setOnClickListener{
            quotationListeners.onShareClickListener(holder.itemView.tv_quote_author.text.toString(),holder.itemView.tv_quote_text.text.toString())
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return quoteArrayList.size
    }
    fun updateCheckList(newQuoteList: ArrayList<QuoteModel>) {
        quoteArrayList=newQuoteList
        notifyDataSetChanged()
    }
    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(quoteModel: QuoteModel) {
            val text=itemView.findViewById<TextView>(R.id.tv_quote_text)
            val author=itemView.findViewById<TextView>(R.id.tv_quote_author)

            try{
                val randomizer = Random()
                val random_quote: String = quoteModel.Quotes.get(randomizer.nextInt(quoteModel.Quotes.size))
                text.setText(random_quote)
                author.setText(quoteModel.auther)


            }
            catch (e:Exception){
                e.printStackTrace()
            }

        }
    }
}