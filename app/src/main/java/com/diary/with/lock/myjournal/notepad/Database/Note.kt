package com.diary.with.lock.myjournal.notepad.Database

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.diary.with.lock.myjournal.notepad.models.CheckListItem


@Entity(tableName = "note_table")
@TypeConverters(Converters::class,CheckListTypeConverter::class)
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val type:String,
    val noteTitle: String,
    val noteContent: String,
    val date: String,
    val hasImage:Boolean,
    val imageList:List<String>,
    val isTrashed:Boolean,
    val checkList:List<CheckListItem>?

)




