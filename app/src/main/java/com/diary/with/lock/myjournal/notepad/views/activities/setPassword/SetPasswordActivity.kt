package com.diary.with.lock.myjournal.notepad.views.activities.setPassword

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import com.diary.with.lock.myjournal.notepad.views.activities.SecurityQuestion.SecurityQuestionActivity

import kotlinx.android.synthetic.main.activity_set_password.*
import kotlinx.android.synthetic.main.toolbar.*

class SetPasswordActivity : BaseActivity(),TextWatcher {

    var sharedPref: SharedPref? = null
    var source=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_password)

        source = intent.getStringExtra("source")!!
        sharedPref = SharedPref(applicationContext)
        setSetting()
        //activity_title.setText("Set Password")

        nextPassword.setOnClickListener {
            var password = Password.text.toString()
            var confirm = Confirm_pasd.text.toString()

            if (password.length>2) {
                if (password.equals(confirm)) {
                    if(enter_new.text.toString().equals(getString(R.string.enter_current_pin))){
                        if(password.equals(sharedPref!!.getPassword())){
                            enter_new.setText("Enter New Password")
                            Password.setText("")
                            Confirm_pasd.setText("")
                            btn_skip.visibility=View.GONE
                        }
                        else{
                            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show()
                        }

                    }
                    else if(enter_new.text.toString().equals("Enter New Password")){ //user is  coming after entering successful current password(Reset password )  or forget password
                          sharedPref?.setPassword(password)
                        if(source.equals("SecurityQuestionActivity")){ //  forget password
                            finishAffinity()
                            startActivity(Intent(this, DashboardActivity::class.java))
                        }
                        else {
                            if (sharedPref!!.isPasswordSetup()) {  //security question already setup
                                finishAffinity()
                                startActivity(Intent(this, DashboardActivity::class.java))
                            } else {
                                intent = Intent(this, SecurityQuestionActivity::class.java)
                                intent.putExtra("source", "SetPasswordActivity")
                                startActivity(intent)
                            }
                        }

                          }
                    else if(enter_new.text.toString().equals("Set password")){ //first time
                        sharedPref?.setPassword(password)
                        intent = Intent(this, SecurityQuestionActivity::class.java)
                        intent.putExtra("source", "SetPasswordActivity")
                        startActivity(intent)
                    }


                } else {
                    lo_confirm.setError(getString(R.string.both_should_same))
                }
            } else {
                lo_pasd.setError(getString(R.string.cannot_be_blank))
            }
        }

        btn_skip.setOnClickListener{
            sharedPref!!.setUsePassword(false)
            finishAffinity()
            startActivity(Intent(this,
                DashboardActivity::class.java))
        }
        back_arrow.setOnClickListener {
            onBackPressed()
        }
        qr_icon.setOnClickListener{
            try {
                Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    fun setSetting() {
        btn_skip.visibility=View.VISIBLE
        if(source.equals("SecurityQuestionActivity")){  //forget password activity
            enter_new.setText("Enter New Password")
            btn_skip.visibility=View.GONE
        }
        else if(sharedPref!!.isPasswordSetup()) {
             enter_new.setText(getString(R.string.enter_current_pin))
         }
        else{
             enter_new.setText("Set password")
         }
//         if(source.equals("FirstTime")){
             back_arrow.visibility= View.GONE
//         }
    }


    override fun onResume() {
        super.onResume()
        var bgColor = sharedPref?.getBgColor()
        set_password_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

}