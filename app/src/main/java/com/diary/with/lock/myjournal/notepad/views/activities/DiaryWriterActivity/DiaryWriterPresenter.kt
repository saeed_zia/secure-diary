package com.diary.with.lock.myjournal.notepad.views.activities.DiaryWriterActivity

import android.util.Log
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DiaryWriterPresenter (view: DiaryWriterContract.View): DiaryWriterContract.Presenter{
    var mView=view

    override fun getAllNoteDatFromDatabase(appDatabase: NoteDatabase) {
        var list = listOf<Note>()
        try {
            GlobalScope.launch(Dispatchers.Default) {
                list = appDatabase.noteDao().getAll()
                mView.showAllNoteData(list)
            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }
//
//    override fun deleteNoteFromDatabase(appDatabase: NoteDatabase,id:Int) {
//        try {
//
//            GlobalScope.launch(Dispatchers.Default) {
//                 appDatabase.noteDao().deleteNoteById(id)
//
//            }
//
//        }
//        catch (e: Exception){
//            Log.d("get hello",e.toString())
//        }
//    }

    override fun moveNoteToTrash(appDatabase: NoteDatabase, noteId: Int) {
        try {

            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().moveToTrash(noteId,true)

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun moveNoteToTrashInBackup(appDatabase: NoteDatabase, noteId: Int) {
        try {
            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().moveToTrash(noteId,true)

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }
//


}