package com.diary.with.lock.myjournal.notepad.views.activities.checkList

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Database.DatabaseBuilder
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.adapters.CheckListAdapter
import com.diary.with.lock.myjournal.notepad.interfaces.CheckBoxClickListener
import com.diary.with.lock.myjournal.notepad.models.CheckListItem
import com.diary.with.lock.myjournal.notepad.models.NoteModel
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.ReminderActivity.ReminderActivity
import com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNoteContract
import com.diary.with.lock.myjournal.notepad.views.activities.WriteNote.WriteNotePresenter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.activity_check_list.*
import kotlinx.android.synthetic.main.loading_layout.*
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class CheckListActivity : BaseActivity(), CheckBoxClickListener, WriteNoteContract.View {
    var sharedPref: SharedPref?=null
    var checkArraylist= arrayListOf<CheckListItem>()
    var adapter: CheckListAdapter?=null
    var isNewNote: Boolean?=null
    var selectedNoteModel: NoteModel?=null
    var cal = Calendar.getInstance()
    var presenter: WriteNotePresenter?=null
    var  emojiPopup:EmojiPopup?=null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_list)
        sharedPref= SharedPref(this)

        setSetting()
        loadInterAd()
        adapter = CheckListAdapter(checkArraylist,this,getTextSize(),0)
        receiveIntent()
        check_list_recycler_view.addItemDecoration(
         DividerItemDecoration(applicationContext,DividerItemDecoration.VERTICAL) )
        check_list_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
        //creating our adapter
        check_list_recycler_view.adapter = adapter
        back_button.setOnClickListener{
            onBackPressed()
        }


        date_picker.setOnClickListener{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                cal.set(Calendar.MONTH,monthOfYear)
                cal.set(Calendar.YEAR,year)
                val myFormat = "EEE MMM dd yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                val s= sdf.format(cal.getTime())
                date_picker.setText(s)
                Log.d("title","hell")
            }, year, month, day)

            dpd.show()
        }
        menu_button.setOnClickListener{
            val popupMenu: PopupMenu = PopupMenu(this,menu_button)
            popupMenu.menuInflater.inflate(R.menu.popup_menu,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.menu_share ->
                    {
                        shareText()
                    }

                }
                true
            })
            popupMenu.show()
        }

        reminder_btn.setOnClickListener{
            startActivity(Intent(applicationContext, ReminderActivity::class.java))
        }

        save_fab.setOnClickListener{
            if(checkArraylist.size>1){
                if(title_input.text.toString().equals("")){
                    showAlertTitleFirstDialog()
                }
                else{
                    saveCheckList()
                    showInterAd()
                }
            }
            else if(checkArraylist.size==1){
                if(!checkArraylist[0].text.equals("")){
                    if(title_input.text.toString().equals("")){
                        showAlertTitleFirstDialog()
                    }
                    else{
                        saveCheckList()
                       showInterAd()
                    }
                }
                else{
                    showAlertEnterSomething()
                }
            }
        }
    }
    fun showAlertEnterSomething(){

            val builder = AlertDialog.Builder(this)
            //set title for alert dialog
            builder.setTitle("Note is empty")
            //set message for alert dialog
            builder.setMessage("Add text  first to save Content.")

            //performing positive action
            builder.setPositiveButton(getString(R.string.ok)){ dialogInterface, which ->
                //super.onBackPressed()
                //Toast.makeText(applicationContext,"clicked yes",Toast.LENGTH_LONG).show()
            }

//        builder.setNegativeButton(getString(R.string.cancel)){dialogInterface, which ->
//            // Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
//        }
            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()

    }
    fun getTextSize():Float{
       return  sharedPref!!.getTextSize()
    }
    fun shareText() {
        val path: File = applicationContext.getFilesDir()
        val file = File(path, "Note.txt")
        val stream = FileOutputStream(file)
        try {
            var s="Date:"+date_picker.text.toString()+"\n"
            s=s+"Title:"+title_input.text.toString()+"\n"
            s=s+checkArraylist.toString()
            stream.write(s.toByteArray())
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        finally {
            stream.close()
        }
        val shareIntent = Intent(Intent.ACTION_SEND).apply {
            type ="*/*" //will accepts all types of files, if you want specific then change it on your need.
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(Intent.EXTRA_SUBJECT,"Sharing file from the Diary with Lock")
            putExtra( Intent.EXTRA_TEXT,"This file contain data of one Note")
            val fileURI = FileProvider.getUriForFile(
                applicationContext!!, applicationContext!!.packageName + ".provider", File(file.toString())
            )
            putExtra(Intent.EXTRA_STREAM, fileURI)
        }
        startActivity(shareIntent)
    }
    fun receiveIntent(){
        isNewNote = intent.getBooleanExtra("isNewNote",false)
        if(!isNewNote!!) //update note
        {
            val gson = Gson()

            var selectedNote: String? = intent.getStringExtra("selectedNote")
            if (selectedNote != null) {
                val type: Type = object : TypeToken<NoteModel?>() {}.getType()
                selectedNoteModel = gson.fromJson<NoteModel>(selectedNote, type)
                title_input.setText(selectedNoteModel?.noteTitle)
                var tempDate=selectedNoteModel?.month+"-"+selectedNoteModel?.day+"-"+selectedNoteModel?.year
                val d=getFormatedDate(tempDate)
                date_picker.setText(d)
                //set global cal object,it will be used in save if date not changed
                var sdf = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
                cal.time=sdf.parse(tempDate)//
                checkArraylist=selectedNoteModel!!.checkList as ArrayList<CheckListItem>
                adapter!!.updateCheckList(checkArraylist)

            }
        }
        else{
            //set date for new note
            var c= Calendar.getInstance()
            val myFormat = "EEE MMM dd yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            val d= sdf.format(c.getTime()).toString()
            date_picker.setText(d)
            addFirstCheckItem()
            adapter!!.updateCheckList(checkArraylist)
        }

    }
    private fun addFirstCheckItem(){
        checkArraylist.add(CheckListItem(false,""))
    }
    override fun onBackPressed() {
        if(checkArraylist.size>1){
            if(title_input.text.toString().equals("")){
               showAlertDialog()
            }
            else{
                saveCheckList()
                showInterAd()
            }
        }
       else if(checkArraylist.size==1){
            if(!checkArraylist[0].text.equals("")){
                if(title_input.text.toString().equals("")){
                  showAlertDialog()
                }
                else{
                    saveCheckList()
                    showInterAd()
                }
            }
            else{
                showInterAd()
            }
        }

    }
    fun showAlertTitleFirstDialog(){
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Title is empty")
        //set message for alert dialog
            builder.setMessage("Add title first to save Content.")

        //performing positive action
        builder.setPositiveButton(getString(R.string.ok)){ dialogInterface, which ->
            //super.onBackPressed()
            //Toast.makeText(applicationContext,"clicked yes",Toast.LENGTH_LONG).show()
        }

//        builder.setNegativeButton(getString(R.string.cancel)){dialogInterface, which ->
//            // Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
//        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
    fun showAlertDialog(){
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Are you sure")
        //set message for alert dialog
        if(isNewNote!!){
            builder.setMessage(getString(R.string.if_title_save))
        }
        else{
            builder.setMessage(getString(R.string.if_title_update))
        }

        //performing positive action
        builder.setPositiveButton(getString(R.string.go_back)){ dialogInterface, which ->
            showInterAd()
            //Toast.makeText(applicationContext,"clicked yes",Toast.LENGTH_LONG).show()
        }

        builder.setNegativeButton(getString(R.string.cancel)){dialogInterface, which ->
            // Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
    private fun saveCheckList(){
        var hasImages:Boolean=false
        var title = title_input.text.toString()


        val myFormat = "MMM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val date = sdf.format(cal.getTime())

        var tempAddress=arrayListOf<String>()

        val noteDatabase: NoteDatabase = DatabaseBuilder.getInstance(applicationContext)
        presenter = WriteNotePresenter(this)
        if(!title.equals("")) {
            if (checkArraylist.size>0) {
                if (isNewNote!!) {
                    val note = Note(null, "checkListNote",title,"" , date, hasImages, tempAddress, false,checkArraylist)
                    presenter?.saveNoteInDatabase(noteDatabase, note)
                    Utils.showToastCenter(this,"Saving")
                } else { //update note

                    val note = Note(selectedNoteModel!!.id, "checkListNote",title,"" , date, hasImages, tempAddress, false,checkArraylist)
                    presenter?.updateNoteInDatabase(noteDatabase, note)
                    Utils.showToastCenter(this,"Updating")
                }


                // onBackPressed()
               // super.onBackPressed()
            }
            else{

               // Toast.makeText(applicationContext, "Cannot be empty", Toast.LENGTH_SHORT).show()
            }
        }
        else{
            title_input.setError(getString(R.string.cannot_be_empty))
           // Toast.makeText(applicationContext, "Cannot be empty", Toast.LENGTH_SHORT).show()
        }
    }


    private fun getFormatedDate(date:String):String{

        var c=Calendar.getInstance()
        var sdf =SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        c.time=sdf.parse(date)//

        val myFormat = "EEE MMM dd yyyy" // mention the format you need
        sdf = SimpleDateFormat(myFormat, Locale.US)
        val d= sdf.format(c.getTime())

        return d
    }

    override fun onResume() {
        super.onResume()
        adapter!!.notifyDataSetChanged()
    }

    fun addItems(){
       // checkArraylist.add(CheckListItem(false,"This is first text"))
       // checkArraylist.add(CheckListItem(2,false,"This is second text"))
       // checkArraylist.add(CheckListItem(3,false,"This is third text"))
       // adapter!!.updateCheckList(checkArraylist)
    }

    override fun onCheckBoxClickListener(status:Boolean,position: Int) {
        checkArraylist.get(position).checkStatus=status
    }

    override fun onAfterTextChangeListener(newText: String,position: Int) {

           checkArraylist.get(position).text=newText
    }

    override fun onEnterPressed(position: Int) {
       // Toast.makeText(this, "newly added", Toast.LENGTH_SHORT).show()

        checkArraylist.add(position+1,CheckListItem(false,""))
        adapter = CheckListAdapter(checkArraylist,this,getTextSize(),position+1)
        check_list_recycler_view.adapter=adapter
       // adapter!!.updateCheckList(checkArraylist)
    }

    override fun deleteCheckItem(position: Int) {
            if(checkArraylist.size>1) {
                checkArraylist.removeAt(position)
               // adapter!!.updateCheckList(checkArraylist)
                adapter = CheckListAdapter(checkArraylist,this,getTextSize(),0)
                check_list_recycler_view.adapter=adapter
            }

    }

    fun setSetting(){
        check_list_root.setBackgroundColor(sharedPref!!.getBgColor())
        window.statusBarColor = sharedPref!!.getBgColor()
    }

    override fun showAllData(note_list: List<Note>) {

    }
    fun showInterAd(){
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getChecklistInterFB()
                    .equals("true") && sharedPref!!.getChecklistInterAD()
                    .equals("false"))
            {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getChecklistInterFB()
                    .equals("false") && sharedPref!!.getChecklistInterAD()
                    .equals("true"))
            {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }

    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgChecklist())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility= View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgChecklist())
        }
        else{
            finish()
            //startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
}