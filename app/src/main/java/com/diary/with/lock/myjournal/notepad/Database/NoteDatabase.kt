package com.diary.with.lock.myjournal.notepad.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.io.File

@Database(entities = [Note::class], version = 1)
//@TypeConverters(Converters::class)
abstract class NoteDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao

}
object DatabaseBuilder {

    private var INSTANCE: NoteDatabase? = null

    fun getInstance(context: Context): NoteDatabase {
        if (INSTANCE == null) {
            synchronized(NoteDatabase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            NoteDatabase::class.java,
            "diary-database"
        ).build()
//.fallbackToDestructiveMigration()


    private var BACKUP_INSTANCE: NoteDatabase? = null

    fun getBackupInstance(context: Context,file: File): NoteDatabase {


        if (BACKUP_INSTANCE == null) {
            synchronized(NoteDatabase::class) {
                BACKUP_INSTANCE = buildBackupRoomDB(context,file)
            }
        }
        return BACKUP_INSTANCE!!
    }
    private fun buildBackupRoomDB(context: Context,file: File) =

        Room.databaseBuilder(context.applicationContext,NoteDatabase::class.java,file.toString())

            .build()
}