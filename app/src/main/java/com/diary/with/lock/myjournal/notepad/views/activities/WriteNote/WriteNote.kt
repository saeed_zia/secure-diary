package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Database.DatabaseBuilder
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.models.ImageModel
import com.diary.with.lock.myjournal.notepad.models.NoteModel
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.LineEditText
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.ReminderActivity.ReminderActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.activity_write_note.*
import kotlinx.android.synthetic.main.loading_layout.*
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class WriteNote : BaseActivity() ,View.OnClickListener ,WriteNoteContract.View {

    var sharedPref: SharedPref?=null
    var presenter:WriteNotePresenter?=null
    var cal = Calendar.getInstance()
    var isNewNote: Boolean?=null
    val IMAGE_PICK_GALLERY=1
    val IMAGE_PICK_CAMERA=2
    var selectedNoteModel: NoteModel?=null
    var selectedImages = arrayListOf<Bitmap>()
    var imageIds= arrayListOf<ImageModel>()
    var  emojiPopup:EmojiPopup?=null
   // var dialog :Dialog?=null
    var storagePermission=false
    var cameraPermission=false
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_note)

        sharedPref=SharedPref(applicationContext)
        emojiPopup = EmojiPopup.Builder.fromRootView(write_root_constraint).build(content_input)

        setSetting()
        setImageIds()
        receiveIntent()
        setClickListeners()
        loadInterAd()
    }
    fun setClickListeners(){
        cross1.setOnClickListener(this)
        cross2.setOnClickListener(this)
        cross3.setOnClickListener(this)
        cross4.setOnClickListener(this)
        imageView1.setOnClickListener(this)
        imageView2.setOnClickListener(this)
        imageView3.setOnClickListener(this)
        imageView4.setOnClickListener(this)

        save_fab.setOnClickListener {
           requestStoragePermissions()
        }
        content_input.setOnClickListener{
            if(emojiPopup!!.isShowing()){
               emojiPopup!!.dismiss()
            }
        }
        back_button.setOnClickListener{
            onBackPressed()
        }
        reminder_btn.setOnClickListener{
            startActivity(Intent(applicationContext, ReminderActivity::class.java))
        }
        menu_button.setOnClickListener{
            val popupMenu: PopupMenu = PopupMenu(this,menu_button)
            popupMenu.menuInflater.inflate(R.menu.popup_menu,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.menu_share ->
                    {
                        // shareNote()
                       //shareNote()
                        shareText()
                    }

                }
                true
            })
            popupMenu.show()
        }
        date_picker.setOnClickListener{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                cal.set(Calendar.MONTH,monthOfYear)
                cal.set(Calendar.YEAR,year)
                val myFormat = "EEE MMM dd yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                val s= sdf.format(cal.getTime())
                date_picker.setText(s)
                Log.d("title","hell")
            }, year, month, day)

            dpd.show()
        }

        emoji_button.setOnClickListener{

            emojiPopup!!.toggle() // Toggles visibility of the Popup.
          //  emojiPopup.dismiss(); // Dismisses the Popup.
           // emojiPopup.isShowing(); // Returns true when Popup is showing.
        }

        add_image.setOnClickListener{
            showDialog()
        }
    }

    override fun onBackPressed() {

        val inputMethodManager: InputMethodManager =getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isActive()) {
            if (getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(
                    getCurrentFocus()!!.getWindowToken(), 0)
            }
        }
        var title = title_input.text.toString()
        var content = content_input.text.toString()
        if(!title.equals("") || !content.equals("")) {
                showAlertDialog()
        }
       else{
            showInterAd()
        }

    }
    fun saveNote(){
        var hasImages:Boolean=false
        var title = title_input.text.toString()
        var content = content_input.text.toString()

        val myFormat = "MMM/dd/yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val date = sdf.format(cal.getTime())

        var tempAddress=arrayListOf<String>()

        for (bitmap in selectedImages) {
            var add = SaveInStorageBitmap(bitmap)
            tempAddress.add(add)
        }

        if(selectedImages.size>0){hasImages=true}  // database parameter
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/Maximus/BackupDiary/")
        if (!myDir.exists()){
            myDir.mkdirs()
        }
        val myFile=File(myDir,"backup-diary-database")
        val noteDatabase: NoteDatabase = DatabaseBuilder.getInstance(applicationContext)
        val noteBackupDatabase: NoteDatabase = DatabaseBuilder.getBackupInstance(applicationContext,myFile)
        presenter = WriteNotePresenter(this)
        if(!title.equals("")) {
            if (!content.equals("")) {
                if (isNewNote!!) {
                    val note = Note(null, "simpleNote",title, content, date, hasImages, tempAddress, false,null)
                    presenter?.saveNoteInDatabase(noteDatabase, note)
                    presenter?.saveNoteInBackupDatabase(noteBackupDatabase, note)
                    Utils.showToastCenter(this,"Saving")
                } else { //update note
                    updateStorage(tempAddress)
                    val note = Note(selectedNoteModel?.id,"simpleNote", title, content, date,  hasImages,tempAddress,false,null)
                    presenter?.updateNoteInDatabase(noteDatabase, note)
                    presenter?.updateNoteInBackupDatabase(noteBackupDatabase, note)
                    Toast.makeText(this, "Updating", Toast.LENGTH_SHORT).show()
                }


                // onBackPressed()
               showInterAd()
            }
            else{
                content_input.setError("Cannot be empty")
                Toast.makeText(applicationContext, "Cannot be empty", Toast.LENGTH_SHORT).show()
            }
        }
        else{
            title_input.setError("Cannot be empty")
            Toast.makeText(applicationContext, "Cannot be empty", Toast.LENGTH_SHORT).show()
        }
    }
    fun receiveIntent(){
        isNewNote = intent.getBooleanExtra("isNewNote",false)
        if(!isNewNote!!) //update note
        {
            val gson = Gson()

            var selectedNote: String? = intent.getStringExtra("selectedNote")
            if (selectedNote != null) {
                val type: Type = object : TypeToken<NoteModel?>() {}.getType()
                selectedNoteModel = gson.fromJson<NoteModel>(selectedNote, type)
                title_input.setText(selectedNoteModel?.noteTitle)
                content_input.setText(selectedNoteModel?.noteContent)
                var tempDate=selectedNoteModel?.month+"-"+selectedNoteModel?.day+"-"+selectedNoteModel?.year
                val d=getFormatedDate(tempDate)
                date_picker.setText(d)
              //set global cal object,it will be used in save if date not changed
                var sdf =SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
                cal.time=sdf.parse(tempDate)//

                loadImages(selectedNoteModel?.addressList!!)
            }
        }
        else{
            //set date for new note
            var c=Calendar.getInstance()
            val myFormat = "EEE MMM dd yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            val d= sdf.format(c.getTime()).toString()
            date_picker.setText(d)
        }

    }
    fun setImageIds(){
        imageIds.add(ImageModel(R.id.imageView1,R.id.cross1))
        imageIds.add(ImageModel(R.id.imageView2,R.id.cross2))
        imageIds.add(ImageModel(R.id.imageView3,R.id.cross3))
        imageIds.add(ImageModel(R.id.imageView4,R.id.cross4))
    }
    fun setSetting(){
        var bgColor=sharedPref?.getBgColor()
        write_root_constraint?.setBackgroundColor(bgColor!!)
        toolbar1.setBackgroundColor(bgColor!!)
       var t_size= sharedPref?.getTextSize()
        content_input.setTextSize(TypedValue.COMPLEX_UNIT_SP, t_size!!)
        window.statusBarColor = bgColor
       var font_path=sharedPref!!.getFont()
        val face = Typeface.createFromAsset( assets,font_path)
        content_input.setTypeface(face)
        title_input.setTypeface(face)
        save_fab.setBackgroundTintList(ColorStateList.valueOf(bgColor))
      //  content_input.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20.0f,  getResources().getDisplayMetrics()), 1.0f);
//        val txtFrench = java.lang.String.format(Locale.JAPAN, "%s", "hello")
//        content_input.setText(txtFrench)
    }
    fun updateStorage(addresses:ArrayList<String>){
        for(ad in selectedNoteModel?.addressList!!){
            if(!addresses.contains(ad)){
                deleteImage(ad)
            }
        }
    }
    fun deleteImage(path:String){
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/Maximus/Diary/")
        if (myDir.isDirectory()) {

                try{
                    if( File(path).exists()){
                        File(path).delete()
                    }
                }
                catch (e:Exception){
                    Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
                }
        }
    }
    fun shareNote(){
        if(true) {

            var edit = findViewById<LineEditText>(R.id.content_input)
            var s = edit.text.toString()

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            var date = date_picker.text.toString()
            var subject = title_input.text.toString()
            // var content=note_content.text
            var text = "Date:" + date + "\n" + "Subject:" + subject + "\n" + s
           // writeAndShareFile(text)
            writeFile()
            //shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            // startActivity(Intent.createChooser(shareIntent,"Share Via\n Maximus Technologies"))
        }
    }

    fun loadImages(imageList:List<String>){
        selectedImages.clear()
     for(address in imageList){
         var imgFile = File(address)

         if (imgFile.exists()) {
             var myBitmap: Bitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
             selectedImages.add(myBitmap)
         }
     }
        refreshImageLayout()
    }

    fun refreshImageLayout(){
        var k=selectedImages
      if(selectedImages.size==0){
          imageView1.setImageBitmap(null)
      }
        else {
          for (index in 0 until selectedImages.size) {
              var imageView = findViewById<ImageView>(imageIds[index].imageId)
              imageView.setImageBitmap(selectedImages[index])
              imageView = findViewById<ImageView>(imageIds[index].crossId)
              imageView.setImageResource(R.drawable.ic_baseline_cancel_24)
              imageView.setBackgroundColor(getColor(R.color.white))

              //imageView.visibility=View.VISIBLE
          }
      }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //
        if (requestCode === IMAGE_PICK_GALLERY && resultCode === Activity.RESULT_OK && data != null) {
            if (data != null) {
                // Get the URI of the selected file
              try {
                  var uri = data.getData();
                  val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                  if (selectedImages.size < 4) {
                      selectedImages.add(bitmap)
                      refreshImageLayout()
                  } else {
                      Toast.makeText(
                          applicationContext,
                          "You cannot select more than 4 images in 1 note",
                          Toast.LENGTH_SHORT
                      ).show()
                  }
              }
              catch (e:Exception){
                  e.printStackTrace()
              }
            }
        }
        else if(requestCode === IMAGE_PICK_CAMERA && resultCode === Activity.RESULT_OK && data != null){
           try {
               val photo = data!!.extras!!["data"] as Bitmap?
               if (selectedImages.size < 4) {
                   selectedImages.add(photo!!)
                   refreshImageLayout()
                   //SaveInStorageBitmap(photo!!,"hello")
               } else {
                   Toast.makeText(  applicationContext, "You cannot select more than 4 images in 1 note", Toast.LENGTH_SHORT).show()
               }
           }
           catch (e:Exception){
               e.printStackTrace()
           }
        }
    }

    fun getFormatedDate( date:String):String{

        var c=Calendar.getInstance()
        var sdf =SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        c.time=sdf.parse(date)//

        val myFormat = "EEE MMM dd yyyy" // mention the format you need
        sdf = SimpleDateFormat(myFormat, Locale.US)
        val d= sdf.format(c.getTime())

        return d
    }

     fun checkPermissions(nextFun: String){
         if (( ContextCompat.checkSelfPermission( this,Manifest.permission.WRITE_EXTERNAL_STORAGE ) == 0)&&
             ( ContextCompat.checkSelfPermission( this,Manifest.permission.READ_EXTERNAL_STORAGE ) == 0)&&
             ContextCompat.checkSelfPermission( this,Manifest.permission.CAMERA ) == 0)
         {
             if(nextFun.equals("camera")){
                 val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                 startActivityForResult(takePicture, IMAGE_PICK_CAMERA)

             }
             else{
                 val intent = Intent(Intent.ACTION_GET_CONTENT)
                 intent.type = "image/*"
                 startActivityForResult(intent, IMAGE_PICK_GALLERY)
             }
         }
         else{
             showPermissionTutorialDialog(nextFun)
         }

    }


    override fun onResume() {
        super.onResume()

    }

    private fun showPermissionTutorialDialog(nextFun:String) {
        var dialog=Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.permission_tutorial_dialog)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val allow=dialog!!.findViewById<Button>(R.id.btn_allow_access)
        allow.setOnClickListener{
            dialog!!.dismiss()

           requestPermissions(nextFun)
        }
    }


    fun requestPermissions(nextFun:String){

        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        Permissions.check(this /*context*/, permissions,null /*rationale*/,null /*options*/, object : PermissionHandler() {
            override fun onGranted() {
               if(nextFun.equals("camera")){
                   val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                   startActivityForResult(takePicture, IMAGE_PICK_CAMERA)
               }
                else if(nextFun.equals("gallery")){
                   val intent = Intent(Intent.ACTION_GET_CONTENT)
                   intent.type = "image/*"
                   startActivityForResult(intent, IMAGE_PICK_GALLERY)
               }

            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)

            }
        })

    }
    private fun showStoragePermissionDialog(nextFun:String) {
        var dialog=Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.storage_permission_dialog)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val allow=dialog!!.findViewById<Button>(R.id.btn_allow_access)
        allow.setOnClickListener{
            dialog!!.dismiss()

            requestStoragePermissions()
        }
    }
    fun requestStoragePermissions(){

        val permissions = arrayOf( Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        Permissions.check(this /*context*/, permissions,null /*rationale*/,null /*options*/, object : PermissionHandler() {
            override fun onGranted() {
                saveNote()
            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                Toast.makeText(applicationContext, "Storage permission is required", Toast.LENGTH_SHORT).show()
            }
        })

    }
    private fun showDialog() {
        var dialog=Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(true)
        dialog!!.setContentView(R.layout.selecter_type)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
        val gallery=dialog!!.findViewById<Button>(R.id.btn_gallery)
        gallery.setOnClickListener{
            dialog!!.dismiss()
           checkPermissions("gallery")
        }
        val camera=dialog!!.findViewById<Button>(R.id.btn_camera)
        camera.setOnClickListener {
            dialog!!.dismiss()
           checkPermissions("camera")
        }

    }
    fun showAlertDialog(){
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Are you sure")
        //set message for alert dialog
        if(isNewNote!!){
            builder.setMessage("Content not saved.Are you still want to go back?")
        }
        else{
            builder.setMessage("Content not updated.Are you still want to go back?")
        }

        //performing positive action
        builder.setPositiveButton("GO BACK"){dialogInterface, which ->
            showInterAd()
            //Toast.makeText(applicationContext,"clicked yes",Toast.LENGTH_LONG).show()
        }

        builder.setNegativeButton("CANCEL"){dialogInterface, which ->
           // Toast.makeText(applicationContext,"clicked No",Toast.LENGTH_LONG).show()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
    private fun showImageDialog(position:Int) {
        val settingsDialog = Dialog(this)
        settingsDialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        settingsDialog.setContentView(R.layout.image_viewer)
        var cancel_btn=settingsDialog.findViewById<ImageView>(R.id.image_cross)
        cancel_btn.setOnClickListener{
            settingsDialog.dismiss()
        }
       var imageView=settingsDialog.findViewById<ImageView>(R.id.image)
        var selected_imageView:ImageView?=null
        when (position) {
             1 -> {    selected_imageView=findViewById<ImageView>(R.id.imageView1)}
            2->{ selected_imageView=findViewById<ImageView>(R.id.imageView2)}
            3->{ selected_imageView=findViewById<ImageView>(R.id.imageView3)}
            4->{ selected_imageView=findViewById<ImageView>(R.id.imageView4)}
        }

        if(selected_imageView!!.drawable!=null){
            val drawable = selected_imageView!!.drawable as BitmapDrawable
            val bitmap = drawable.bitmap
            imageView.setImageBitmap(bitmap)
            settingsDialog.show()
        }
    }

     fun SaveInStorageBitmap(qrBitmapImage: Bitmap):String{

            val root = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/Maximus/Diary/")
            myDir.mkdirs()
            val dateFormat = SimpleDateFormat("dd-MM-yyyy' 'hh_mm_ss.SSS")
            val timeStamp: String = dateFormat.format(Date())

            val fname = "Diary-$timeStamp.jpg"
            var file = File(myDir, fname)
            //Toast.makeText(this, "File path " + file.toString(), Toast.LENGTH_SHORT).show()
            try {
                val out = FileOutputStream(file)
                qrBitmapImage.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return file.toString() ///file path to store in database

    }

    override fun showAllData(note_list: List<Note>) {

    }

   fun writeFile(){
       val path: File = applicationContext.getFilesDir()
       val file = File(path, "config.txt")
       val stream = FileOutputStream(file)
       try {
           stream.write("text-to-write".toByteArray())
       }
       catch (e:Exception){
           e.printStackTrace()
       }
       finally {
           stream.close()
       }

       try {
           val intentShareFile = Intent(Intent.ACTION_SEND)
           if (file.exists()) {
               intentShareFile.type = "*/*"
               intentShareFile.putExtra(Intent.EXTRA_STREAM,Uri.parse("config.txt"))
               intentShareFile.putExtra(Intent.EXTRA_SUBJECT,"Sharing File...")
               intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...")
               startActivity(Intent.createChooser(intentShareFile, "Share File"))
           }
           }

       catch (e:java.lang.Exception)
       {
           e.printStackTrace()
       }

   }
    fun shareText() {
        val path: File = applicationContext.getFilesDir()
        val file = File(path, "Note.txt")
        val stream = FileOutputStream(file)
        try {
                var s=content_input.text.toString()
                stream.write(s.toByteArray())


            val shareIntent = Intent(Intent.ACTION_SEND).apply {
                type ="*/*" //will accepts all types of files, if you want specific then change it on your need.
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                putExtra(Intent.EXTRA_SUBJECT,"Sharing file from the Diary with Lock")
                putExtra( Intent.EXTRA_TEXT,"This file contain the ")
                val fileURI = FileProvider.getUriForFile(
                    applicationContext!!, applicationContext!!.packageName + ".provider",File(file.toString()))
                putExtra(Intent.EXTRA_STREAM, fileURI)
            }
            startActivity(shareIntent)
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        finally {
            stream.close()
        }
    }
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.cross1 -> {
                val bitmap: Bitmap = (imageView1.getDrawable() as BitmapDrawable).getBitmap()
                selectedImages.remove(bitmap)
                imageView1.setImageResource(0)
                cross1.setImageResource(0)
               // refreshImageLayout()
            }
            R.id.cross2 -> {
                val bitmap: Bitmap = (imageView2.getDrawable() as BitmapDrawable).getBitmap()
                selectedImages.remove(bitmap)
                imageView2.setImageResource(0)
                cross2.setImageResource(0)
            }
            R.id.cross3 -> {
                val bitmap: Bitmap = (imageView3.getDrawable() as BitmapDrawable).getBitmap()
                selectedImages.remove(bitmap)
                imageView3.setImageResource(0)
                cross3.setImageResource(0)
              
            }
            R.id.cross4 -> {
                val bitmap: Bitmap = (imageView4.getDrawable() as BitmapDrawable).getBitmap()
                selectedImages.remove(bitmap)
                imageView4.setImageResource(0)
                cross4.setImageResource(0)
            }
            R.id.imageView1->{
                showImageDialog(1)
            }
            R.id.imageView2->{
              showImageDialog(2)
            }
            R.id.imageView3->{
               showImageDialog(3)
            }
            R.id.imageView4->{
                showImageDialog(4)
            }

            else -> {
            }
        }
    }
    fun showInterAd(){
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getWriteNoteInterFB()
                    .equals("true") && sharedPref!!.getWriteNoteInterAD()
                    .equals("false"))
            {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getWriteNoteInterFB()
                    .equals("false") && sharedPref!!.getWriteNoteInterAD()
                    .equals("true"))
            {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }

    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!=null && FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgWriteNote())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgWriteNote())
        }
        else{
            finish()
            //startActivity(Intent(this, DashboardActivity::class.java))
        }
    }
}