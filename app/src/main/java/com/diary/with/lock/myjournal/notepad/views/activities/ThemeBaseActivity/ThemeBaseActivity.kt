package com.diary.with.lock.myjournal.notepad.views.activities.ThemeBaseActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.diary.with.lock.myjournal.notepad.R

import com.diary.with.lock.myjournal.notepad.utils.SharedPref


open  class ThemeBaseActivity : AppCompatActivity() {
    var sp: SharedPref?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_theme_base)
        sp= SharedPref(applicationContext)
        setMyTheme(sp!!.getFont())
    }

    fun setMyTheme(font:String){
     when(font){
            "roboto_theme" -> {  setTheme(R.style.Roboto_theme)}
            "great_vibes" ->{  setTheme(R.style.Great_Vibes_theme)}
            "kaushan"    ->{  setTheme(R.style.Kaushan_theme)}
            "abhayalibre_regular"->{  setTheme(R.style.Abhaya_theme)}
            "aguafina_script"->{  setTheme(R.style.Aguafina_theme)}


        }
    }
}