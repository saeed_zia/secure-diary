package com.diary.with.lock.myjournal.notepad.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.interfaces.FrameClickListener
import kotlinx.android.synthetic.main.quotation_frame_item.view.*


class QuotationFrameAdapter(var context: Context,arrayList: ArrayList<Int>,var frameClickListener: FrameClickListener) :
    RecyclerView.Adapter<QuotationFrameAdapter.ItemHolder>() {
    var marrayList= arrayListOf<Int>()
    init {
       marrayList=arrayList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val viewHolder = LayoutInflater.from(parent.context)
            .inflate(R.layout.quotation_frame_item, parent, false)
        return ItemHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return marrayList.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val frameId: Int = marrayList.get(position)
       holder.bindItems(frameId)
        holder.itemView.iv_frame.setOnClickListener{
            frameClickListener.onFrameClick(position)
        }
//        holder.itemView.setOnClickListener {
//
//            when (position) {
//                1 -> {
//                    Toast.makeText(context,frameId.toString(), Toast.LENGTH_SHORT).show()
//
//                }
//                2 -> {
//                    Toast.makeText(context,frameId.toString(), Toast.LENGTH_SHORT).show()
//                    // true
//                }
//
//            }
//        }

    }

    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(imageId:Int) {
            var iconsframe = itemView.findViewById<ImageView>(R.id.iv_frame)
            try{
                iconsframe.setImageResource(imageId)
            }
            catch (e:Exception){
                e.printStackTrace()
            }

        }
    }
}