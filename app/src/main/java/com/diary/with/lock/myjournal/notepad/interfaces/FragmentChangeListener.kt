package com.diary.with.lock.myjournal.notepad.interfaces

import androidx.fragment.app.Fragment

interface FragmentChangeListener {
    fun replaceFragment(fragment: Fragment?)
    fun storageAllow()
}