package com.diary.with.lock.myjournal.notepad.interfaces

import com.diary.with.lock.myjournal.notepad.models.NoteModel



interface CellClickListener {
    fun onCellClickListener(note: NoteModel)

}