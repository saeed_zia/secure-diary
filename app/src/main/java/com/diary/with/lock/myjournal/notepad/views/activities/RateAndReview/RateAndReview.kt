package com.diary.with.lock.myjournal.notepad.views.activities.RateAndReview

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.RatingBar.OnRatingBarChangeListener
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.ThankYou.ThankYouActivity
import kotlinx.android.synthetic.main.activity_rate_and_review.*
import kotlinx.android.synthetic.main.toolbar.*

class RateAndReview : BaseActivity() {

    var sharedPref: SharedPref? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_and_review)
        sharedPref = SharedPref(applicationContext)


        simpleRatingBar.setOnRatingBarChangeListener(OnRatingBarChangeListener { ratingBar, rating, fromUser ->

            if (rating > 4) {
                val uri: Uri = Uri.parse("market://details?id=$packageName")
                val goToMarket = Intent(Intent.ACTION_VIEW, uri)
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(
                    Intent.FLAG_ACTIVITY_NO_HISTORY or
                            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK )
                try {
                    startActivity(goToMarket)
                } catch (e: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
                        )
                    )
                }
            } else {
                var intent=Intent(applicationContext, ThankYouActivity::class.java)
                intent.putExtra("rating",rating)
                startActivity(intent)
            }
        })
        back_arrow.setOnClickListener {
            onBackPressed()
        }
        qr_icon.setOnClickListener{
            try {
                Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }

    }



    override fun onResume() {
        super.onResume()
        var bgColor = sharedPref?.getBgColor()
        rate_review_root?.setBackgroundColor(bgColor!!)
        window.statusBarColor = sharedPref!!.getBgColor()
    }
    fun setSetting(){
        if(sharedPref!!.getQrIconStatus().equals("false")){
            qr_icon.visibility= View.GONE
        }
    }

}