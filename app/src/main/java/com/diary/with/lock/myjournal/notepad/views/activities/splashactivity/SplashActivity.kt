package com.diary.with.lock.myjournal.notepad.views.activities.splashactivity

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.BuildConfig
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Dashboard.DashboardActivity
import com.diary.with.lock.myjournal.notepad.views.activities.Login.LoginActivity
import com.diary.with.lock.myjournal.notepad.views.activities.PrivacyPolicy.PrivacyPolicy
import com.diary.with.lock.myjournal.notepad.views.activities.Tutorials.TutorialActivity
import com.diary.with.lock.myjournal.notepad.views.activities.setPassword.SetPasswordActivity
import com.facebook.ads.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.customize_native_ad.view.*
import kotlinx.android.synthetic.main.loading_layout.*
import java.util.*


class SplashActivity : BaseActivity(), BillingProcessor.IBillingHandler{

    var sharedPref: SharedPref?=null
    private var fbRemoteConfig: FirebaseRemoteConfig? = null
    // This is the loading time of the splash screen
    private val SPLASH_TIME_OUT:Long = 5000 // 1 sec
    var remoteConfig:FirebaseRemoteConfig?=null
   // private var ads: InterstitalAdsSplash?=null
    var bp: BillingProcessor? = null
    var interStatus:Boolean=false
    private var mCurrentLocale: Locale? = null
    var selectedInter:String?=null
    private var ads: InterstitalAdsSplash?=null
    //
    var selectedNative:String?=null
    var unifiedNativeAd:UnifiedNativeAd?=null
    var nativeAd: NativeAd?=null
    var nativeAdLayout: NativeAdLayout?=null
    var adView: ConstraintLayout?=null
    var addStatus:Boolean?=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sharedPref=SharedPref(applicationContext)


        setSetting()
        showBookAnimation()
        setUpRemoteConfig()

        //showSplashScreen()
      //  setLocale(sharedPref!!.getLanguage())
        bp = BillingProcessor(this, getString(R.string.in_app_billing), this)
        bp!!.initialize()
        fcmSubscription()
        if(!BuildConfig.DEBUG) {
           // fcmSubscription()
            if (bp!!.isPurchased(packageName)) {
                sharedPref!!.payBillStatus(true)
            } else {
                sharedPref!!.payBillStatus(false)
            }
        }
      btn_start.setOnClickListener{

          sharedPref!!.privacyPolicyAccepted()
        //  showSplashScreen()
          showInterstitialAd()
//          var intent=Intent(this, TutorialActivity::class.java)
//          intent.putExtra("source","SplashActivity")
//          startActivity(intent)
      }
     tv_privacy.setOnClickListener{
         startActivity(Intent(this, PrivacyPolicy::class.java))
     }

    if(sharedPref!!.isPrivacyPolicyAccepted()){
        btn_start.visibility= View.GONE
        tv_privacy.visibility= View.GONE }
    }
    fun showBookAnimation(){
       if(sharedPref!!.getTutorialStatus()){
           native_ad_const.visibility=View.VISIBLE
           animation_view.visibility=View.VISIBLE
           tv_loading.visibility=View.VISIBLE
       }
        else{
           book_animation.visibility=View.VISIBLE
       }
    }

    fun initializeAdmobAd(){

        if(!sharedPref!!.isBillPayed() ){
            MobileAds.initialize(getApplicationContext(), getString(R.string.admob_app_id))

            if(!sharedPref!!.getAllNativeStatus().equals("false")){
                loadNativeAd()
            }
        }
    }
    fun setLocale(lang: String) {
        Log.d("checklanguageInbase", lang.toString())
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
        mCurrentLocale = myLocale

    }
    fun showSplashScreen(){

        Handler(Looper.getMainLooper()).postDelayed({

        showInterstitialAd()
            // close this activity
            //finish()
        }, SPLASH_TIME_OUT)
    }
    fun showStartButton(){
        Handler().postDelayed({
            btn_start.visibility= View.VISIBLE

        },  SPLASH_TIME_OUT)
    }

    fun goToNextActivity(){
        var isPasswordSetup=sharedPref!!.isPasswordSetup()
        var tutorialStatus=sharedPref!!.getTutorialStatus()
        val permissionStatus=sharedPref!!.getPermissionStatus()
        val isPasswordUse=sharedPref!!.isUsePassword()

            if(tutorialStatus){
                if (isPasswordUse){
                    if (isPasswordSetup) {

                       startActivity(Intent(this,LoginActivity::class.java))
                    } else {

                        var intent = Intent(applicationContext, SetPasswordActivity::class.java)
                        intent.putExtra("source", "FirstTime")
                        startActivity(intent)
                    }
                }
                else{

                    startActivity(Intent(this,DashboardActivity::class.java))
                }
            }
            else
            {
                finish()
                var intent=Intent(this, TutorialActivity::class.java)
                intent.putExtra("source","SplashActivity")
                startActivity(intent)
            }

    }
    fun loadNativeAd(){

        if(!sharedPref!!.isBillPayed()) {

                AdmobNativeAd.loadAdId2(this)
                AdmobNativeAd.selectedAd="ad2"

                FacebookNativeAd.loadNativeFacebookAd1(this)
                FacebookNativeAd.selectedAd="ad1"

        }
        else{

        }
    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {

            if (sharedPref!!.getLoginActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getLoginActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if (sharedPref!!.getLoginActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getLoginActivityInterAdStatusAD()
                    .equals("true"))
            {
                InterstitalAdsSplash.loadInterstitialAd(this)
                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }

    fun showInterstitialAd(){
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            }
            else {

            }
        }
        else if(selectedInter.equals("ad")){

          showInterstitialAdmob()
        }
        else{
            goToNextActivity()
        }
    }

    fun showInterstitialFb(){

        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
               goToNextActivity()

            },  sharedPref!!.getProgLoginInter())


        } else {
            goToNextActivity()
        }
    }

    fun showInterstitialAdmob(){
        // Toast.makeText(this, sharedPref!!.getProgLoginInter().toString(), Toast.LENGTH_SHORT).show()
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE

        var isPasswordSetup=sharedPref!!.isPasswordSetup()
        var tutorialStatus=sharedPref!!.getTutorialStatus()
        val permissionStatus=sharedPref!!.getPermissionStatus()
        val isPasswordUse=sharedPref!!.isUsePassword()

            Handler().postDelayed({
                ads= InterstitalAdsSplash()
                if(ads!=null) {


                    if (tutorialStatus) {
                        if (isPasswordUse) {
                            if (isPasswordSetup) {

                                ads!!.madMobShoeClose(this, Intent(this, LoginActivity::class.java))
                            } else {

                                var intent =
                                    Intent(applicationContext, SetPasswordActivity::class.java)
                                intent.putExtra("source", "FirstTime")
                                ads!!.madMobShoeClose(this, intent)
                            }
                        } else {

                            ads!!.madMobShoeClose(this, Intent(this, DashboardActivity::class.java))

                        }
                    } else {
                        finish()
                        var intent = Intent(this, TutorialActivity::class.java)
                        intent.putExtra("source", "SplashActivity")
                        ads!!.madMobShoeClose(this, intent)
                    }

                }
            },  sharedPref!!.getProgLoginInter())

    }


    fun setSetting(){
        var bgColor=sharedPref!!.getBgColor()
        splash_root?.setBackgroundColor(bgColor)
        window.statusBarColor = bgColor
        version.setText("V${BuildConfig.VERSION_CODE}")
    }

    override fun onResume() {
        super.onResume()
        //var bgColor=sharedPref?.getBgColor()
        //splash_root?.setBackgroundColor(bgColor!!)
    }

    override fun onBillingInitialized() {

    }

    override fun onPurchaseHistoryRestored() {
       // sharedPref!!.payBillStatus(true)
    }

    override fun onProductPurchased(productId: String, details: TransactionDetails?) {
    //  sharedPref!!.payBillStatus(true)
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {

    }
    private fun fcmSubscription() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = packageName
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(
                NotificationManager::class.java )
            notificationManager.createNotificationChannel(
                NotificationChannel(  channelId,  channelName, NotificationManager.IMPORTANCE_HIGH ))
        }
        FirebaseMessaging.getInstance()
            .subscribeToTopic(packageName)
            .addOnCompleteListener { task ->
               // var msg = getString(R.string.msg_subscribed)
                if (!task.isSuccessful) {
                   // msg = getString(R.string.msg_subscribe_failed)
                }
            }
    }

    private fun setUpRemoteConfig() {
        remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(1)
            .build()
        remoteConfig!!.setConfigSettingsAsync(configSettings)
        remoteConfig!!.setDefaultsAsync(R.xml.remote_config_parameter)
        remoteConfig!!.fetchAndActivate().addOnCompleteListener(OnCompleteListener<Boolean?> { task ->



            if (task.isSuccessful) {
                remoteConfig!!.activate()
                FirebaseRemoteConfig.getInstance().activate()
                remoteConfig!!.activate()
//
               sharedPref!!.setProgLoginInter(remoteConfig!!.getLong("ProgLoginInter"))

//
                sharedPref!!.setSplashNativeAdStatusAD(remoteConfig!!.getString("SplashAD"))
                showNativeAd()


                sharedPref!!.setColorStyleActivityInterAdStatusFB(remoteConfig!!.getString("ColorStyleInterFB"))
                sharedPref!!.setDiaryWriterActivityNativeAdStatusFB(remoteConfig!!.getString("DiaryWriterFB"))
                sharedPref!!.setLanguageActivityNativeAdStatusFB(remoteConfig!!.getString("LanguageFB"))
                sharedPref!!.setLanguageActivityInterAdStatusFB(remoteConfig!!.getString("LanguageInterFB"))
                sharedPref!!.setLockSettingActivityNativeAdStatusFB(remoteConfig!!.getString("LockSettingFB"))
                sharedPref!!.setLockSettingInterFB(remoteConfig!!.getString("LockSettingInterFB"))
                sharedPref!!.setLoginActivityInterAdStatusFB(remoteConfig!!.getString("LoginInterFB"))
                sharedPref!!.setPrivacyActivityInterAdStatusFB(remoteConfig!!.getString("PrivacyPolicyInterFB"))
                sharedPref!!.setQuotationActivityNativeAdStatusFB(remoteConfig!!.getString("QuotationFB"))
                sharedPref!!.setQuotationActivityInterAdStatusFB(remoteConfig!!.getString("QuotationInterFB"))
                sharedPref!!.setReminderActivityNativeAdStatusFB(remoteConfig!!.getString("ReminderFB"))
                sharedPref!!.setReminderActivityInterAdStatusFB(remoteConfig!!.getString("ReminderInterFB"))
                sharedPref!!.setThankYouActivityInterAdStatusFB(remoteConfig!!.getString("ThankYouInterFB"))
                sharedPref!!.setTrashActivityNativeAdStatusFB(remoteConfig!!.getString("TrashFB"))
                sharedPref!!.setTrashActivityInterAdStatusFB(remoteConfig!!.getString("TrashInterFB"))
                sharedPref!!.setTutorialActivityNativeAdStatusFB(remoteConfig!!.getString("TutorialFB"))
                sharedPref!!.setTutorialActivityInterAdStatusFB(remoteConfig!!.getString("TutorialInterFB"))
                sharedPref!!.setDashboardActivityNativeAdStatusFB(remoteConfig!!.getString("DashboardFB"))
                sharedPref!!.setFeedbackActivityInterAdStatusFB(remoteConfig!!.getString("FeedBackInterFB"))
                sharedPref!!.setBottomSheetActivityNativeAdStatusFB(remoteConfig!!.getString("BottomSheetFB"))
                sharedPref!!.setColorStyleNativeAdStatusFB(remoteConfig!!.getString("ColorStyleFB"))
                sharedPref!!.setSplashNativeAdStatusFB(remoteConfig!!.getString("SplashFB"))
                sharedPref!!.setWriteNoteInterFB(remoteConfig!!.getString("WriteNoteInterFB"))
                sharedPref!!.setChecklistInterFB(remoteConfig!!.getString("ChecklistInterFB"))
 //
                sharedPref!!.setAllNativeStatus(remoteConfig!!.getString("StatusOfAllNative"))

  //
                //

                if(remoteConfig!!.getString("ColorStyleInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setColorStyleActivityInterAdStatusAD(remoteConfig!!.getString("ColorStyleInterAD"))
                sharedPref!!.setDiaryWriterActivityNativeAdStatusAD(remoteConfig!!.getString("DiaryWriterAD"))
                sharedPref!!.setLanguageActivityNativeAdStatusAD(remoteConfig!!.getString("LanguageAD"))
                if(remoteConfig!!.getString("LanguageInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setLanguageActivityInterAdStatusAD(remoteConfig!!.getString("LanguageInterAD"))
                sharedPref!!.setLockSettingActivityNativeAdStatusAD(remoteConfig!!.getString("LockSettingAD"))
                if(remoteConfig!!.getString("LockSettingInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setLockSettingInterAD(remoteConfig!!.getString("LockSettingInterAD"))
                if(remoteConfig!!.getString("LoginInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setLoginActivityInterAdStatusAD(remoteConfig!!.getString("LoginInterAD"))
                if(remoteConfig!!.getString("PrivacyPolicyInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setPrivacyActivityInterAdStatusAD(remoteConfig!!.getString("PrivacyPolicyInterAD"))
                sharedPref!!.setQuotationActivityNativeAdStatusAD(remoteConfig!!.getString("QuotationAD"))
                if(remoteConfig!!.getString("QuotationInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setQuotationActivityInterAdStatusAD(remoteConfig!!.getString("QuotationInterAD"))
                sharedPref!!.setReminderActivityNativeAdStatusAD(remoteConfig!!.getString("ReminderAD"))
                if(remoteConfig!!.getString("ReminderInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setReminderActivityInterAdStatusAD(remoteConfig!!.getString("ReminderInterAD"))
                if(remoteConfig!!.getString("ThankYouInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setThankYouActivityInterAdStatusAD(remoteConfig!!.getString("ThankYouInterAD"))
                sharedPref!!.setTrashActivityNativeAdStatusAD(remoteConfig!!.getString("TrashAD"))
                if(remoteConfig!!.getString("TrashInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setTrashActivityInterAdStatusAD(remoteConfig!!.getString("TrashInterAD"))
                sharedPref!!.setTutorialActivityNativeAdStatusAD(remoteConfig!!.getString("TutorialAD"))
                if(remoteConfig!!.getString("TutorialInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setTutorialActivityInterAdStatusAD(remoteConfig!!.getString("TutorialInterAD"))
                sharedPref!!.setDashboardActivityNativeAdStatusAD(remoteConfig!!.getString("DashboardAD"))
                if(remoteConfig!!.getString("FeedBackInterAD").equals("true")){
                    interStatus=true
                }
                sharedPref!!.setFeedbackActivityInterAdStatusAD(remoteConfig!!.getString("FeedBackInterAD"))
                sharedPref!!.setWriteNoteInterAD(remoteConfig!!.getString("WriteNoteInterAD"))
                sharedPref!!.setChecklistInterAD(remoteConfig!!.getString("ChecklistInterAD"))



                sharedPref!!.setBottomSheetActivityNativeAdStatusAD(remoteConfig!!.getString("BottomSheetAD"))
                sharedPref!!.setColorStyleNativeAdStatusAD(remoteConfig!!.getString("ColorStyleAD"))

                 //

                sharedPref!!.setProgLoginInter(remoteConfig!!.getLong("ProgLogin"))
                sharedPref!!.setProgChecklist(remoteConfig!!.getLong("ProgChecklist"))
                sharedPref!!.setProgColorStyle(remoteConfig!!.getLong("ProgColorStyle"))
                sharedPref!!.setProgLanguage(remoteConfig!!.getLong("ProgLanguage"))
                sharedPref!!.setProgLockSetting(remoteConfig!!.getLong("ProgLockSetting"))
                sharedPref!!.setProgPrivacyPolicy(remoteConfig!!.getLong("ProgPrivacyPolicy"))
                sharedPref!!.setProgQuotation(remoteConfig!!.getLong("ProgQuotation"))
                sharedPref!!.setProgRateAndReview(remoteConfig!!.getLong("ProgRateAndReview"))
                sharedPref!!.setProgReminder(remoteConfig!!.getLong("ProgReminder"))
                sharedPref!!.setProgThankyou(remoteConfig!!.getLong("ProgThankyou"))
                sharedPref!!.setProgTrash(remoteConfig!!.getLong("ProgTrash"))
                sharedPref!!.setProgTutorial(remoteConfig!!.getLong("ProgTutorial"))
                sharedPref!!.setProgFeedback(remoteConfig!!.getLong("ProgFeedback"))
                sharedPref!!.setProgWriteNote(remoteConfig!!.getLong("ProgWriteNote"))
                //
                sharedPref!!.setQrIconStatus(remoteConfig!!.getString("QrIconStatus"))
                sharedPref!!.setQrIconURL(remoteConfig!!.getString("QrIconUrl"))
                //

                initializeAdmobAd()

                loadInterAd()

                if(sharedPref!!.isPrivacyPolicyAccepted()){
                    showSplashScreen()
                    btn_start.visibility= View.GONE
                    tv_privacy.visibility= View.GONE
                }
                else{
                   showStartButton()
                }

            } else {
                sharedPref!!.setColorStyleActivityInterAdStatusFB("false")
                sharedPref!!.setDiaryWriterActivityNativeAdStatusFB("false")
                sharedPref!!.setLanguageActivityNativeAdStatusFB("false")
                sharedPref!!.setLanguageActivityInterAdStatusFB("false")
                sharedPref!!.setLockSettingActivityNativeAdStatusFB("false")
                sharedPref!!.setLockSettingInterFB("false")
                sharedPref!!.setLoginActivityInterAdStatusFB("false")
                sharedPref!!.setPrivacyActivityInterAdStatusFB("false")
                sharedPref!!.setQuotationActivityNativeAdStatusFB("false")
                sharedPref!!.setQuotationActivityInterAdStatusFB("false")
                sharedPref!!.setReminderActivityNativeAdStatusFB("false")
                sharedPref!!.setReminderActivityInterAdStatusFB("false")
                sharedPref!!.setThankYouActivityInterAdStatusFB("false")
                sharedPref!!.setTrashActivityNativeAdStatusFB("false")
                sharedPref!!.setTrashActivityInterAdStatusFB("false")
                sharedPref!!.setTutorialActivityNativeAdStatusFB("false")
                sharedPref!!.setTutorialActivityInterAdStatusFB("false")
                sharedPref!!.setDashboardActivityNativeAdStatusFB("false")
                sharedPref!!.setFeedbackActivityInterAdStatusFB("false")
                sharedPref!!.setBottomSheetActivityNativeAdStatusFB("false")
                sharedPref!!.setSplashNativeAdStatusFB("false")
                sharedPref!!.setWriteNoteInterFB("false")
                sharedPref!!.setColorStyleNativeAdStatusFB("false")
                //

                sharedPref!!.setColorStyleActivityInterAdStatusAD("false")
                sharedPref!!.setDiaryWriterActivityNativeAdStatusAD("false")
                sharedPref!!.setLanguageActivityNativeAdStatusAD("false")
                sharedPref!!.setLanguageActivityInterAdStatusAD("false")
                sharedPref!!.setLockSettingActivityNativeAdStatusAD("false")
                sharedPref!!.setLockSettingInterAD("false")
                sharedPref!!.setLoginActivityInterAdStatusAD("false")
                sharedPref!!.setPrivacyActivityInterAdStatusAD("false")
                sharedPref!!.setQuotationActivityNativeAdStatusAD("false")
                sharedPref!!.setQuotationActivityInterAdStatusAD("false")
                sharedPref!!.setReminderActivityNativeAdStatusAD("false")
                sharedPref!!.setReminderActivityInterAdStatusAD("false")
                sharedPref!!.setThankYouActivityInterAdStatusAD("false")
                sharedPref!!.setTrashActivityNativeAdStatusAD("false")
                sharedPref!!.setTrashActivityInterAdStatusAD("false")
                sharedPref!!.setTutorialActivityNativeAdStatusAD("false")
                sharedPref!!.setTutorialActivityInterAdStatusAD("false")
                sharedPref!!.setDashboardActivityNativeAdStatusAD("false")
                sharedPref!!.setFeedbackActivityInterAdStatusAD("false")
                sharedPref!!.setBottomSheetActivityNativeAdStatusAD("false")
                sharedPref!!.setSplashNativeAdStatusAD("false")
                sharedPref!!.setWriteNoteInterAD("false")

                sharedPref!!.setColorStyleNativeAdStatusAD("false")
                //
                sharedPref!!.setQrIconStatus("false")

              //  InterstitalAdsSplash.loadInterstitialAd(this)
                if(sharedPref!!.isPrivacyPolicyAccepted()){

                    Handler().postDelayed({
                        goToNextActivity()

                    },  SPLASH_TIME_OUT)
                    btn_start.visibility= View.GONE
                    tv_privacy.visibility= View.GONE
                }
                else{
                   showStartButton()
                }

            }
        })
    }




    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        native_ad_const.visibility=View.GONE
    }
    fun showNativeAd(){

            if(sharedPref!!.getTutorialStatus()) {
                if (!sharedPref!!.isBillPayed()) {
                    if (sharedPref!!.getSplashNativeAdStatusFB()
                            .equals("true") && sharedPref!!.getSplashNativeAdStatusAD()
                            .equals("false")
                    ) {
                        showFacebookNativeAd()
                        selectedNative = "fb"
                    } else if ((sharedPref!!.getSplashNativeAdStatusFB()
                            .equals("false") && sharedPref!!.getSplashNativeAdStatusAD()
                            .equals("true"))
                    ) {
                        showAdmobNativeAd()
                        selectedNative = "ad"
                    } else {
                        hideNativeAnim()
                        selectedNative = "no"
                    }
                } else {
                    hideNativeAnim()
                    selectedNative = "no"
                }
            }

    }



    fun loadNewNativeAd(type:String,idNumber:String){
        if (type.equals("fb")){
            if (idNumber.equals("id1")){
                FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
            }
            else{
                FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
            }
        }
        else if(type.equals("ad")){
            if (idNumber.equals("id1")){
                AdmobNativeAd.loadAdId2(applicationContext)
            }
            else{
                AdmobNativeAd.loadAdId1(applicationContext)
            }
        }
    }
    private fun populateUnifiedNativeAdView(unifiedNativeAd: UnifiedNativeAd?,adView: UnifiedNativeAdView)
    {

        var media=adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }

            /* if (unifiedNativeAd.advertiser == null) {
                 adView.advertiserView.visibility = View.INVISIBLE
             } else {
                 adView.ad_advertiser.text = unifiedNativeAd.advertiser
                 adView.advertiserView.visibility = View.VISIBLE
             }*/
            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.native_ad_container)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
        //native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }

    fun showAdmobNativeAd(){

        val builder = AdLoader.Builder(this,getString(R.string.admob_native_id1))
            .forUnifiedNativeAd { unifiedNativeAd ->
                val adView = layoutInflater .inflate(R.layout.customize_native_ad, null) as UnifiedNativeAdView

                populateUnifiedNativeAdView(unifiedNativeAd,adView)
                admob_ad.removeAllViews()
                admob_ad.addView(adView)
            }
        builder.withAdListener(object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                //tv_ad_loading.visibility = View.GONE
                //pb_ad_loading.visibility= View.GONE

            }
            override fun onAdLoaded() {
                super.onAdLoaded()
                tv_ad_loading.visibility = View.INVISIBLE
                pb_ad_loading!!.visibility = View.INVISIBLE

            }

        })
        var adLoader: AdLoader = builder.build()
        adLoader.loadAd(AdRequest.Builder().build())
    }
    fun showFacebookNativeAd() {

        nativeAd = NativeAd(applicationContext,getString(R.string.facebook_native_id1))
        val nativeAdListener: NativeAdListener = object : NativeAdListener {
            override fun onMediaDownloaded(ad: Ad) {
                // Native ad finished downloading all assets

            }
            override fun onError(ad: Ad?, adError: AdError) {
                Log.d("d","d")

            }

            override fun onAdLoaded(ad: Ad) {
                if (nativeAd == null || nativeAd != ad) {

                }
                tv_ad_loading.visibility = View.INVISIBLE
                pb_ad_loading!!.visibility = View.INVISIBLE

                inflateAd(nativeAd!!)

                // Inflate Native Ad into Container

            }
            override fun onAdClicked(ad: Ad) {
                // Native ad clicked

            }
            override fun onLoggingImpression(ad: Ad) {
                // Native ad impression
                //   Toast.makeText(context, "fb id1 impression", Toast.LENGTH_SHORT).show()
            }
        }
        // Request an ad
        nativeAd!!.loadAd(nativeAd!!.buildLoadAdConfig()
                .withAdListener(nativeAdListener)
                .build())
    }

}