package com.diary.with.lock.myjournal.notepad.views.activities

import android.content.Context
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.formats.UnifiedNativeAd
import org.greenrobot.eventbus.EventBus


open class AdmobNativeAd {
    companion object {
        var  nativeAd1: UnifiedNativeAd? = null
        var adStatus1:String?=null
        var  nativeAd2: UnifiedNativeAd? = null
        var adStatus2:String?=null
        var selectedAd:String?=null

        var idType:String?=null
        var idNumber:String?=null


        fun loadAdId1(context: Context) {
         selectedAd="ad1"
            val builder = AdLoader.Builder(context, context.getString(R.string.admob_native_id1))
                .forUnifiedNativeAd { unifiedNativeAd ->
                    if (nativeAd1 != null) {
                        nativeAd1?.destroy()
                    }
                    nativeAd1 = unifiedNativeAd

                }
            builder.withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(p0: Int) {
                    super.onAdFailedToLoad(p0)
                    adStatus1="failed"
                    Utils.idType="ad"
                    Utils.idNumber="id1"
                    EventBus.getDefault().post(AdLoadStatus(false,"ad","id1"))

                }

                override fun onAdClosed() {
                    super.onAdClosed()

                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adStatus1="loaded"
                    Utils.idType="ad"
                    Utils.idNumber="id1"
                    EventBus.getDefault().post(AdLoadStatus(true,"ad","id1"))

                }

                override fun onAdImpression() {
                    super.onAdImpression()
                   // Toast.makeText(context, "Admob id1 impression", Toast.LENGTH_SHORT).show()
                }
            })
            var adLoader: AdLoader = builder.build()
            adLoader.loadAd(AdRequest.Builder().build())
        }

        fun loadAdId2(context: Context) {
            selectedAd="ad2"
            val builder = AdLoader.Builder(context, context.getString(R.string.admob_native_id2))
                .forUnifiedNativeAd { unifiedNativeAd ->
                    if (nativeAd2 != null) {
                        nativeAd2?.destroy()
                    }
                    nativeAd2 = unifiedNativeAd

                }
            builder.withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(p0: Int) {
                    super.onAdFailedToLoad(p0)
                    adStatus2="failed"
                    Utils.idType="ad"
                    Utils.idNumber="id2"
                    EventBus.getDefault().post(AdLoadStatus(false,"ad","id2"))

                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adStatus2="loaded"
                    Utils.idType="ad"
                    Utils.idNumber="id2"
                    EventBus.getDefault().post(AdLoadStatus(true,"ad","id2"))

                }

                override fun onAdImpression() {
                    super.onAdImpression()
                  //  Toast.makeText(context, "Admob id2 impression", Toast.LENGTH_SHORT).show()
                }
            })
            var adLoader: AdLoader = builder.build()
            adLoader.loadAd(AdRequest.Builder().build())
        }
    }
}