package com.diary.with.lock.myjournal.notepad.Ads

import android.content.Context
import android.util.Log
import com.diary.with.lock.myjournal.notepad.R
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.facebook.ads.InterstitialAd
import com.facebook.ads.InterstitialAdListener

class FacebookAds {
    companion object{
        var interstitialAd: InterstitialAd? = null

        fun facebookInterstitialAds(context: Context) {
            try {
                interstitialAd =
                    InterstitialAd(context, context.getString(R.string.facebook_interstitial))
                Log.d("id", context.getString(R.string.facebook_interstitial))

                val interstitialAdListener: InterstitialAdListener =
                    object : InterstitialAdListener {
                        override fun onInterstitialDisplayed(ad: Ad) {
                            // Interstitial ad displayed callback

                        }

                        override fun onInterstitialDismissed(ad: Ad) {
                            // Interstitial dismissed callback
                            // finish()

                        }

                        override fun onError(ad: Ad?, adError: AdError) {
                            // Ad error callback
                            Log.e(
                                "Facebook ad",
                                "Interstitial ad failed to load: " + adError.getErrorMessage()
                            )
                        }

                        override fun onAdLoaded(ad: Ad) {
                            // Interstitial ad is loaded and ready to be displayed
                        }

                        override fun onAdClicked(ad: Ad) {
                            // Ad clicked callback
                        }

                        override fun onLoggingImpression(ad: Ad) {
                            // Ad impression logged callback
                            Log.d("Facebook ad", "Interstitial ad impression logged!")
                        }
                    }
                interstitialAd!!.loadAd(
                    interstitialAd!!.buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build()
                )

            }
            catch (e:Exception){
                e.printStackTrace()
               // Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

}