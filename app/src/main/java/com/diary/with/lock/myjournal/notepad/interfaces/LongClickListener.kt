package com.diary.with.lock.myjournal.notepad.interfaces

import com.diary.with.lock.myjournal.notepad.models.NoteModel


interface LongClickListener {
   // fun onLongClickListener(note: NoteModel, position:Int)
    fun shareTextClickListener(note: NoteModel)
    fun moveToTrashclickListener( position:Int)
}