package com.diary.with.lock.myjournal.notepad.views.activities.Dashboard

import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase

interface DashboardContract {

    interface View{

        fun showAllNoteData(note_list: List<Note>)
    }
    interface BackupPresenter{
        fun getAllNoteDatFromBackupDatabase( appDatabase: NoteDatabase)
        fun saveAllDataInDatabase(appDatabase: NoteDatabase,note_list: List<Note>)
    }
}