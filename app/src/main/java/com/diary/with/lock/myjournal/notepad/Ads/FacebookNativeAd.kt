package com.diary.with.lock.myjournal.notepad.Ads

import android.content.Context
import android.util.Log
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdListener
import org.greenrobot.eventbus.EventBus

open class FacebookNativeAd {
    companion object{
        var  nativeAd1: NativeAd? = null
        var  nativeAd2: NativeAd? = null
        var adStatus1:String?=null
        var adStatus2:String?=null
        var selectedAd:String?=null
       // var adStatus:String?=null
        var idTye:String?=null
        var idNumber:String?=null
        fun loadNativeFacebookAd1(context:Context) {
           selectedAd="ad1"
            nativeAd1 = NativeAd(context,context.getString(R.string.facebook_native_id1))
            val nativeAdListener: NativeAdListener = object : NativeAdListener {
                override fun onMediaDownloaded(ad: Ad) {
                    // Native ad finished downloading all assets

                }
                override fun onError(ad: Ad?, adError: AdError) {
                    Log.d("d","d")
                    adStatus1 ="failed"
                    Utils.idType="fb"
                    Utils.idNumber="id1"
                    EventBus.getDefault().post(AdLoadStatus(false,"fb","id1"))
                }

                override fun onAdLoaded(ad: Ad) {

                    if (nativeAd1 == null || nativeAd1 != ad) {
                        return
                    }
                    adStatus1 ="loaded"
                    Utils.idType="fb"
                    Utils.idNumber="id1"
                    EventBus.getDefault().post(AdLoadStatus(true,"fb","id1"))
                    // Inflate Native Ad into Container

                }
                override fun onAdClicked(ad: Ad) {
                    // Native ad clicked

                }
                override fun onLoggingImpression(ad: Ad) {
                    // Native ad impression
                 //   Toast.makeText(context, "fb id1 impression", Toast.LENGTH_SHORT).show()
                }
            }
            // Request an ad
            nativeAd1!!.loadAd(
                nativeAd1!!.buildLoadAdConfig()
                    .withAdListener(nativeAdListener)
                    .build())
        }

         fun loadNativeFacebookAd2(context:Context) {
             selectedAd="ad2"
            nativeAd2 = NativeAd(context,context.getString(R.string.facebook_native_id2))
            val nativeAdListener: NativeAdListener = object : NativeAdListener {
                override fun onMediaDownloaded(ad: Ad) {
                    // Native ad finished downloading all assets

                }

                override fun onError(ad: Ad?, adError: AdError) {
                    Log.d("d","d")
                    adStatus2 ="failed"
                    Utils.idType="fb"
                    Utils.idNumber="id2"
                    EventBus.getDefault().post(AdLoadStatus(false,"fb","id2"))
                }

                override fun onAdLoaded(ad: Ad) {


                    if (nativeAd2 == null || nativeAd2 != ad) {
                        return
                    }
                    // Inflate Native Ad into Container
                    adStatus2 ="loaded"
                    Utils.idType="fb"
                    Utils.idNumber="id2"
                    EventBus.getDefault().post(AdLoadStatus(true,"fb","id2"))

                }
                override fun onAdClicked(ad: Ad) {
                    // Native ad clicked

                }
                override fun onLoggingImpression(ad: Ad) {
                    // Native ad impression
                  //  Toast.makeText(context, "fb id2 impression", Toast.LENGTH_SHORT).show()
                }
            }
            // Request an ad
            nativeAd2!!.loadAd(
                nativeAd2!!.buildLoadAdConfig()
                    .withAdListener(nativeAdListener)
                    .build())
        }
    }
}