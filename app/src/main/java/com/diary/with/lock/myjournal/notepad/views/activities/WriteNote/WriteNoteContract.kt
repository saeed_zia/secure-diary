package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote



import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase


interface WriteNoteContract {
    interface View{

        fun showAllData(note_list: List<Note>)
    }

    interface Presenter{
        fun getAllDatFromDatabase( appDatabase: NoteDatabase)
        fun saveNoteInDatabase(appDatabase: NoteDatabase, note: Note)
        fun updateNoteInDatabase(appDatabase: NoteDatabase, note: Note)
        //

    }
    interface BackupPresenter{

        fun saveNoteInBackupDatabase(appDatabase: NoteDatabase, note: Note)
        fun updateNoteInBackupDatabase(appDatabase: NoteDatabase, note: Note)
    }


}