package com.diary.with.lock.myjournal.notepad.BroadCastReceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import com.diary.with.lock.myjournal.notepad.Services.ForegroundService
import com.diary.with.lock.myjournal.notepad.utils.NotificationUtils
import java.util.*

//
//class AlarmBroadcastReceiver : BroadcastReceiver() {
//
//
//    override fun onReceive(context: Context, intent: Intent) {
//        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                context.startForegroundService(Intent(context, ForegroundService::class.java))
//            } else {
//                context.startService(Intent(context, ForegroundService::class.java))
//            }
//        }
//        else {
//
//            val currentTime = Calendar.getInstance().getTime().toString()
//            val notificationUtils = NotificationUtils( context,currentTime )
//            val notification = notificationUtils.getNotificationBuilder()!!.build()
//            notificationUtils.getManager().notify(150, notification)
//        }
//    }
//}

class AlarmBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        try {
            val currentTime = Calendar.getInstance().getTime().toString()
            // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
            val notificationUtils = NotificationUtils(context, currentTime)
            val notification = notificationUtils.getNotificationBuilder()!!.build()
            notificationUtils.getManager().notify(150, notification)

            val serviceIntent = Intent(context, ForegroundService::class.java)

            serviceIntent.putExtra("timeInMillis", 1)
            serviceIntent.putExtra("status", "stop")
            ContextCompat.startForegroundService(context, serviceIntent)
        }
        catch (e:Exception){

        }
    }
}