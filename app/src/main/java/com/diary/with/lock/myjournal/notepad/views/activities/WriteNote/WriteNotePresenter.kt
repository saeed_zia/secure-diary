package com.diary.with.lock.myjournal.notepad.views.activities.WriteNote

import android.util.Log
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class WriteNotePresenter (view:WriteNoteContract.View):WriteNoteContract.Presenter,WriteNoteContract.BackupPresenter {

    var mView=view

    override fun getAllDatFromDatabase( appDatabase: NoteDatabase) {

        var list = listOf<Note>()
          try {

              GlobalScope.launch(Dispatchers.Default) {
                 list = appDatabase.noteDao().getAll()
                 Log.d("hello","hellllo")
                 mView.showAllData(list)
              }

         }
         catch (e:Exception){
              Log.d("get hello",e.toString())
          }
        
    }

    override fun saveNoteInDatabase( appDatabase: NoteDatabase,note: Note) {
      GlobalScope.launch (Dispatchers.Main){
          try {
              appDatabase.noteDao().insertNote(note)
          }
          catch (e:Exception){
              Log.d("get hello",e.toString())
          }
      }

    }


    override fun updateNoteInDatabase(appDatabase: NoteDatabase, note: Note) {
        GlobalScope.launch (Dispatchers.Default){
            try {
               appDatabase.noteDao().updateNote(note)
            }
            catch (e:Exception){
                Log.d("get hello",e.toString())
            }
        }
    }



    override fun saveNoteInBackupDatabase( appDatabase: NoteDatabase,note: Note) {
        GlobalScope.launch (Dispatchers.Main){
            try {
                appDatabase.noteDao().insertNote(note)
            }
            catch (e:Exception){
                Log.d("get hello",e.toString())
            }
        }

    }


    override fun updateNoteInBackupDatabase(appDatabase: NoteDatabase, note: Note) {
        GlobalScope.launch (Dispatchers.Default){
            try {
                appDatabase.noteDao().updateNote(note)
            }
            catch (e:Exception){
                Log.d("get hello",e.toString())
            }
        }
    }
}