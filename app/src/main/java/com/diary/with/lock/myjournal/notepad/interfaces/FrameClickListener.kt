package com.diary.with.lock.myjournal.notepad.interfaces

interface FrameClickListener {
    fun onFrameClick(position:Int)
}