package com.diary.with.lock.myjournal.notepad.utils


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import com.diary.with.lock.myjournal.notepad.R
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.LoadAdError


class InterstitalAdsSplash {

    companion object {
        var sharedPref: SharedPref? = null
        private var mInterstitialAd: InterstitialAd? = null
        private var interstitialAd_fb: com.facebook.ads.InterstitialAd? = null

        fun loadInterstitialAd(context: Context) {
            sharedPref = SharedPref(context)


            if (mInterstitialAd == null) {

                Log.d("mInterstit", "Already loaded")
                mInterstitialAd = InterstitialAd(context)
                mInterstitialAd!!.adUnitId = context.getString(R.string.admob_splash_interstitial)

            }

            if (!sharedPref!!.isBillPayed()) {

                if (mInterstitialAd!!.isLoaded) {
                    Log.d("mInterstit", "Already loaded")

                } else {
                    if (!sharedPref!!.isBillPayed()) {
                        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                        Log.d("mInterstit", " newReq")
                    }
                }

            }


        }

        /*fun loadSplashInterstitialAd(context: Context) {





            if (splashInterstita == null) {

                Log.d("mInterstit", "Already loaded")
                splashInterstita = InterstitialAd(context)
                splashInterstita!!.adUnitId = context.getString(R.string.live_earth_admob_interstitial_ad)
                //   val adRequest =
            }

            if (!AppUtils.getBillingStatus()) {

                if (splashInterstita!!.isLoaded) {
                    Log.d("mInterstit", "Already loaded")

                } else {
                    if (!AppUtils.getBillingStatus()) {
                        splashInterstita!!.loadAd(AdRequest.Builder().build())
                        Log.d("mInterstit", " newReq")
                    }
                }

            }


        }*/


        fun loadInterstitialAd_fb(context: Context): com.facebook.ads.InterstitialAd {

            if (interstitialAd_fb == null) {

//                if (BuildConfig.DEBUG)
//                {
//                interstitialAd_fb =
//                    com.facebook.ads.InterstitialAd(context, pref.getString("fb_id","346362029328343_368774563753756"))
//                }
//                else
//                {
                interstitialAd_fb =
                    com.facebook.ads.InterstitialAd(context, "346362029328343_368774563753756")

//                }

                if (!false) {
                    interstitialAd_fb!!.loadAd()
                }
                Log.d("mInterstit", "== null facebook request")

            } else {
                if (interstitialAd_fb!!.isAdLoaded) {
                    Log.d("mInterstit", "facebook Already loaded")

                } else {
                    if (!false) {
                        interstitialAd_fb!!.loadAd()
                    }


                    Log.d("mInterstit", "facebook request")

                }
            }

//            interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
//                override fun onInterstitialDisplayed(ad: Ad) {
//                    Log.d("FacebookAdsLog", "Displayed")
//                }
//
//                override fun onInterstitialDismissed(ad: Ad) {
//                    Log.d("FacebookAdsLog", "Dismissed")
////                    if (pref.getBoolean("ads", true)) {
////                        interstitialAd_fb!!.loadAd()
////                    }
//                }
//
//                override fun onError(ad: Ad, adError: AdError) {
//                    Log.d("FacebookAdsLog", "Errors")
//                    //  interstitialAd_fb!!.loadAd()
//                }
//
//                override fun onAdLoaded(ad: Ad) {
//                    Log.d("FacebookAdsLog", "Loaded")
//                }
//
//                override fun onAdClicked(ad: Ad) {
//                    Log.d("FacebookAdsLog", "Clicked")
//
//
//                }
//
//                override fun onLoggingImpression(ad: Ad) {
//                    Log.d("FacebookAdsLog", "Impression")
//
//
//                }
//            })
            return interstitialAd_fb as com.facebook.ads.InterstitialAd
        }

    }

    /*  fun startActivityMadiation(context: Activity, newIntent: Intent) {

          if (interstitialAd_fb != null) {
              if (interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated) {
                  interstitialAd_fb!!.show()
              } else {

                  if (mInterstitialAd!!.isLoaded) {
                      mInterstitialAd!!.show()
                  } else {
                      context.startActivity(newIntent)
  //                context.finish()
                  }
                  mInterstitialAd!!.adListener = object : AdListener() {
                      override fun onAdClosed() {
                          context.startActivity(newIntent)

                          if (pref.getBoolean("ads", true)) {
                              interstitialAd_fb!!.loadAd()
                          }
  //                    context.finish()

                      }
                  }
              }
          } else {
              context.startActivity(newIntent)
          }

          if (interstitialAd_fb != null) {

              interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
                  override fun onInterstitialDisplayed(ad: Ad) {
                      Log.d("FacebookAdsLog", "onInterstitialDisplayed")


                      if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
                          editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()

                          context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 2))

  //                    context.onBackPressed()


  //                    interstitialAd_fb!!.set
                      } else {

                          editor.putInt("counter", 0).commit()
                      }
                  }

                  override fun onInterstitialDismissed(ad: Ad) {
  //                if (pref.getBoolean("ads", true)) {
  //                    interstitialAd_fb!!.loadAd()
  //                }
                      context.startActivity(newIntent)

                      interstitialAd_fb = null
  //                context.finish()
                  }

                  override fun onError(ad: Ad, adError: AdError) {
                      Log.d("FacebookAdsLog", "onError")

                  }

                  override fun onAdLoaded(ad: Ad) {
                      Log.d("FacebookAdsLog", "onAdLoaded")

                  }

                  override fun onAdClicked(ad: Ad) {
                      Log.d("FacebookAdsLog", "onAdClicked")
                      editor.putInt("counter", 1).commit()

                  }

                  override fun onLoggingImpression(ad: Ad) {
                      Log.d("FacebookAdsLog", "onLoggingImpression")


                  }
              })
          }
      }

  */


//    fun startActivityMadiationClose(context: Activity, newIntent: Intent) {
//        if (!sharedPref!!.getAdsPref()&& interstitialAd_fb != null && interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated) {
//            interstitialAd_fb!!.show()
//            interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
//                override fun onInterstitialDisplayed(ad: Ad) {
//                    //   Log.d("FacebookAdsLog1", pref.getInt("counter", 0).toString() + "onInterstitialDisplayed")
//
//
////                interstitialAd_fb!!.loadAd()
//
//
////                if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
////                    editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()
//
////                    context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 1))
//
//
////                    interstitialAd_fb=null
//
////                    interstitialAd_fb!!.set
////                } else {
////
////                    editor.putInt("counter", 0).commit()
////                }
//
//
////                interstitialAd_fb = null
//
//
////                if (pref.getBoolean("ads", true)) {
////                    interstitialAd_fb!!.loadAd()
////                }
//
//
//                }
//
//                override fun onInterstitialDismissed(ad: Ad) {
//
////                if (pref.getBoolean("ads", true)) {
////                    interstitialAd_fb!!.loadAd()
//
////                interstitialAd_fb = null
////                }
//                    if (!sharedPref!!.getAdsPref()) {
//                        interstitialAd_fb!!.loadAd()
//                    }
//                    try {
//                        context.startActivity(newIntent)
//                    } catch (e: ActivityNotFoundException) {
//                        e.printStackTrace()
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
////                context.finish()
//
//
//                }
//
//                override fun onError(ad: Ad, adError: AdError) {
//                    Log.d("FacebookAdsLog1", "onError")
////                interstitialAd_fb = null
//
//
//                }
//
//                override fun onAdLoaded(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onAdLoaded")
//
//                }
//
//                override fun onAdClicked(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onAdClicked")
//
////                editor.putInt("counter", 1).commit()
//                }
//
//                override fun onLoggingImpression(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onLoggingImpression")
//
//
////                (interstitialAd_fb as com.facebook.ads.InterstitialAd).destroy()
//
//
//                }
//            })
//
//
//        } else {
//
//            if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.getAdsPref()) {
//                mInterstitialAd!!.show()
//
//                mInterstitialAd?.adListener = object : AdListener() {
//                    override fun onAdClosed() {
//
//                        if (!sharedPref!!.getAdsPref()) {
//                            mInterstitialAd!!.loadAd(AdRequest.Builder().build())
//                        }
//
//                        try {
//                            context.startActivity(newIntent)
//                        } catch (e: ActivityNotFoundException) {
//                            e.printStackTrace()
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }
////                    context.finish()
//
//                    }
//                }
//
//            } else {
//                try {
//                    context.startActivity(newIntent)
//                } catch (e: ActivityNotFoundException) {
//                    e.printStackTrace()
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//
////                context.finish()
//            }
//
//        }
//
//
//    }
//
//
//    fun finishActivityMadiationClose(context: Activity) {
//        if (!sharedPref!!.getAdsPref() && interstitialAd_fb != null && interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated) {
//            interstitialAd_fb!!.show()
//            interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
//                override fun onInterstitialDisplayed(ad: Ad) {
//                    //   Log.d("FacebookAdsLog1", pref.getInt("counter", 0).toString() + "onInterstitialDisplayed")
//
//
////                interstitialAd_fb!!.loadAd()
//
//
////                if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
////                    editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()
//
////                    context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 1))
//
//
////                    interstitialAd_fb=null
//
////                    interstitialAd_fb!!.set
////                } else {
////
////                    editor.putInt("counter", 0).commit()
////                }
//
//
////                interstitialAd_fb = null
//
//
////                if (pref.getBoolean("ads", true)) {
////                    interstitialAd_fb!!.loadAd()
////                }
//
//
//                }
//
//                override fun onInterstitialDismissed(ad: Ad) {
//
////                if (pref.getBoolean("ads", true)) {
////                    interstitialAd_fb!!.loadAd()
//
////                interstitialAd_fb = null
////                }
//                    if (!sharedPref!!.getAdsPref()) {
//                        interstitialAd_fb!!.loadAd()
//                    }
//                    //    context.startActivity(newIntent)
//                    context.finish()
//
//
//                }
//
//                override fun onError(ad: Ad, adError: AdError) {
//                    Log.d("FacebookAdsLog1", "onError")
////                interstitialAd_fb = null
//
//
//                }
//
//                override fun onAdLoaded(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onAdLoaded")
//
//                }
//
//                override fun onAdClicked(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onAdClicked")
//
////                editor.putInt("counter", 1).commit()
//                }
//
//                override fun onLoggingImpression(ad: Ad) {
//                    Log.d("FacebookAdsLog1", "onLoggingImpression")
//
//
////                (interstitialAd_fb as com.facebook.ads.InterstitialAd).destroy()
//
//
//                }
//            })
//        } else {
//
//            if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.getAdsPref()) {
//                mInterstitialAd!!.show()
//                mInterstitialAd!!.adListener = object : AdListener() {
//                    override fun onAdClosed() {
//
//                        if (!sharedPref!!.getAdsPref()) {
//
//                            mInterstitialAd!!.loadAd(AdRequest.Builder().build())
//                        }
//
//                        // context.startActivity(newIntent)
//                        context.finish()
//
//                    }
//                }
//            } else {
//                //context.startActivity(newIntent)
//                context.finish()
//            }
//
//        }
//
//
//    }


    fun ShowAdMob(context: Activity, newIntent: Intent) {
        if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
            mInterstitialAd!!.show()
            mInterstitialAd!!.adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!sharedPref!!.isBillPayed()) {
                        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                    }
                    context.startActivity(newIntent)
//                context.finish()

                }
            }
        } else {
            context.startActivity(newIntent)
//            context.finish()
        }

    }

    /* fun ShowSplashAdMob(context: Activity, newIntent: Intent) {
         if (splashInterstita!!.isLoaded && !AppUtils.getBillingStatus()) {
             splashInterstita!!.show()
         } else {
             context.startActivity(newIntent)
 //            context.finish()
         }
         splashInterstita!!.adListener = object : AdListener() {
             override fun onAdClosed() {
                 if (!AppUtils.getBillingStatus()) {
                     splashInterstita!!.loadAd(AdRequest.Builder().build())
                 }
                 context.startActivity(newIntent)
 //                context.finish()

             }
         }
     }*/


    fun adMobShoeClose(context: Activity, newIntent: Intent) {
        sharedPref = SharedPref(context)
        if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
            mInterstitialAd!!.show()
            mInterstitialAd!!.adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!sharedPref!!.isBillPayed()) {
                        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                    }
                    context.startActivity(newIntent)
                    context.finish()

                }

                override fun onAdFailedToLoad(p0: LoadAdError?) {
                    super.onAdFailedToLoad(p0)
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                }


            }
        } else {
            context.startActivity(newIntent)
            context.finish()
        }

    }

    fun madMobShoeClose(context: Activity, newIntent: Intent) {
        sharedPref = SharedPref(context)
        if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
            mInterstitialAd!!.show()
            mInterstitialAd!!.adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!sharedPref!!.isBillPayed()) {
                        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                    }
                    context.startActivity(newIntent)
                    //context.finish()

                }

                override fun onAdFailedToLoad(p0: LoadAdError?) {
                    super.onAdFailedToLoad(p0)
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                }


            }
        } else {
            context.startActivity(newIntent)
            // context.finish()
        }

    }

    fun adMobStartActivityResult(context: Activity, newIntent: Intent, Code: Int) {
        if (mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
            mInterstitialAd!!.show()
        } else {
            context.startActivityForResult(newIntent, Code)
//            context.finish()
        }
        mInterstitialAd!!.adListener = object : AdListener() {
            override fun onAdClosed() {
                if (!sharedPref!!.isBillPayed()) {
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                }
                context.startActivityForResult(newIntent, Code)
//                context.finish()

            }
        }
    }

    fun adMobShowCloseOnly(context: Activity) {

        sharedPref = SharedPref(context)
        if (mInterstitialAd != null) {
            if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
                mInterstitialAd?.show()
                mInterstitialAd?.adListener = object : AdListener() {
                    override fun onAdClosed() {
                        if (!sharedPref!!.isBillPayed()) {
                            mInterstitialAd?.loadAd(AdRequest.Builder().build())
                        }

                        context.finish()
                    }

                    override fun onAdFailedToLoad(p0: LoadAdError?) {
                        super.onAdFailedToLoad(p0)
                        mInterstitialAd?.loadAd(AdRequest.Builder().build())

                    }
                }
            } else {
                context.finish()
            }
        } else {
            context.finish()
        }

    }


    /*fun adMobShowExitDialog(context: Activity) {

        if(mInterstitialAd !=null) {

            if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !SharePrefData.getInstance().adS_PREFS) {
                mInterstitialAd?.show()
                mInterstitialAd?.adListener = object : AdListener() {
                    override fun onAdClosed() {
                        if (!SharePrefData.getInstance().adS_PREFS) {
                            mInterstitialAd?.loadAd(AdRequest.Builder().build())
                        }

                        val dialog = GpsExitDialog(context)
                        dialog.setCanceledOnTouchOutside(false)
                        dialog.show()

                    }
                }
            } else {

                val dialog = GpsExitDialog(context)
                dialog.setCanceledOnTouchOutside(false)
                dialog.show()
            }
        }else{
            val dialog = GpsExitDialog(context)
            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
        }
    }*/

    /*fun specificAdForNearByActivity(context: Context, name:String) {
        if (pref.getBoolean("ads", true) && interstitialAd_fb!=null && interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated ) {
            interstitialAd_fb!!.show()
        } else {

            if (mInterstitialAd!!.isLoaded && pref.getBoolean("ads", true)) {
                mInterstitialAd!!.show()
            } else {
                try {
                    MapUtils.openNearBy(context, name)

                }catch (e:ActivityNotFoundException){
                    e.printStackTrace()
                }catch (e:Exception){
                    e.printStackTrace()
                }

//                context.finish()
            }
            mInterstitialAd!!.adListener = object : AdListener() {
                override fun onAdClosed() {

                    if (pref.getBoolean("ads", true)) {
                        mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                    }

                    try {
                        MapUtils.openNearBy(context, name)
                    }catch (e:ActivityNotFoundException){
                        e.printStackTrace()
                    }catch (e:Exception){
                        e.printStackTrace()
                    }
//                    context.finish()

                }
            }
        }


        interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
            override fun onInterstitialDisplayed(ad: Ad) {
                Log.d("FacebookAdsLog1", pref.getInt("counter", 0).toString() + "onInterstitialDisplayed")


//                interstitialAd_fb!!.loadAd()


//                if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
//                    editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()

//                    context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 1))


//                    interstitialAd_fb=null

//                    interstitialAd_fb!!.set
//                } else {
//
//                    editor.putInt("counter", 0).commit()
//                }


//                interstitialAd_fb = null


//                if (pref.getBoolean("ads", true)) {
//                    interstitialAd_fb!!.loadAd()
//                }


            }

            override fun onInterstitialDismissed(ad: Ad) {

//                if (pref.getBoolean("ads", true)) {
//                    interstitialAd_fb!!.loadAd()

//                interstitialAd_fb = null
//                }
                if (pref.getBoolean("ads", true)) {
                    interstitialAd_fb!!.loadAd()
                }
                try {
                    MapUtils.openNearBy(context, name)
                }catch (e:ActivityNotFoundException){
                    e.printStackTrace()
                }catch (e:Exception){
                    e.printStackTrace()
                }
//                context.finish()


            }

            override fun onError(ad: Ad, adError: AdError) {
                Log.d("FacebookAdsLog1", "onError")
//                interstitialAd_fb = null



            }

            override fun onAdLoaded(ad: Ad) {
                Log.d("FacebookAdsLog1", "onAdLoaded")

            }

            override fun onAdClicked(ad: Ad) {
                Log.d("FacebookAdsLog1", "onAdClicked")

//                editor.putInt("counter", 1).commit()
            }

            override fun onLoggingImpression(ad: Ad) {
                Log.d("FacebookAdsLog1", "onLoggingImpression")


//                (interstitialAd_fb as com.facebook.ads.InterstitialAd).destroy()


            }
        })
    }*/


    fun adMobShowOnly(context: Activity) {
        if (mInterstitialAd != null && mInterstitialAd!!.isLoaded && !sharedPref!!.isBillPayed()) {
            if (!sharedPref!!.isBillPayed()) {
                mInterstitialAd!!.show()
                mInterstitialAd!!.adListener = object : AdListener() {
                    override fun onAdClosed() {
                        if (!sharedPref!!.isBillPayed()) {
                            mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                        }

                    }

                    override fun onAdFailedToLoad(p0: Int) {
                        super.onAdFailedToLoad(p0)
                    }

                    override fun onAdLoaded() {
                        super.onAdLoaded()
                    }
                }
            }


        } else {

        }
    }

    /* fun Backpress(context: Activity) {

 //        if (interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated) {
 //            interstitialAd_fb!!.show()
 //        } else {

         if (interstitialAd_fb!!.isAdLoaded) {
             interstitialAd_fb!!.show()
         } else {
             CustomDialogue(context).show()

         }

         interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
             override fun onInterstitialDisplayed(ad: Ad) {
 //                Log.d("FacebookAdsLog1", pref.getInt("counter", 0).toString() + "onInterstitialDisplayed")


 //                interstitialAd_fb!!.loadAd()


 //                if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
 //                    editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()

 //                    context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 1))


 //                    interstitialAd_fb=null

 //                    interstitialAd_fb!!.set
 //                } else {
 //
 //                    editor.putInt("counter", 0).commit()
 //                }


 //                interstitialAd_fb = null


 //                if (pref.getBoolean("ads", true)) {
 //                    interstitialAd_fb!!.loadAd()
 //                }


             }

             override fun onInterstitialDismissed(ad: Ad) {

 //                if (pref.getBoolean("ads", true)) {
 //                    interstitialAd_fb!!.loadAd()

 //                interstitialAd_fb = null
 //                }
                 if (pref.getBoolean("ads", true)) {
                     interstitialAd_fb!!.loadAd()
                 }
                 CustomDialogue(context).show()
 //                context.finish()


             }

             override fun onError(ad: Ad, adError: AdError) {
                 Log.d("FacebookAdsLog1", "onError")
 //                interstitialAd_fb = null



             }

             override fun onAdLoaded(ad: Ad) {
                 Log.d("FacebookAdsLog1", "onAdLoaded")

             }

             override fun onAdClicked(ad: Ad) {
                 Log.d("FacebookAdsLog1", "onAdClicked")

 //                editor.putInt("counter", 1).commit()
             }

             override fun onLoggingImpression(ad: Ad) {
                 Log.d("FacebookAdsLog1", "onLoggingImpression")


 //                (interstitialAd_fb as com.facebook.ads.InterstitialAd).destroy()


             }
         })

 //        interstitialAd_fb!!.adListener = object : AdListener() {
 //            override fun onAdClosed() {
 //                CustomDialogue(context).show()
 //
 //
 //            }
 //        }
 //        }

 //
 //        interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
 //            override fun onInterstitialDisplayed(ad: Ad) {
 //                Log.d("FacebookAdsLog", "onInterstitialDisplayed")
 //            }
 //
 //            override fun onInterstitialDismissed(ad: Ad) {
 //                CustomDialogue(context).show()
 //
 //            }
 //
 //            override fun onError(ad: Ad, adError: AdError) {
 //                Log.d("FacebookAdsLog", "onError")
 //
 //            }
 //
 //            override fun onAdLoaded(ad: Ad) {
 //                Log.d("FacebookAdsLog", "onAdLoaded")
 //
 //            }
 //
 //            override fun onAdClicked(ad: Ad) {
 //                Log.d("FacebookAdsLog", "onAdClicked")
 //            }
 //
 //            override fun onLoggingImpression(ad: Ad) {
 //                Log.d("FacebookAdsLog", "onLoggingImpression")
 //
 //
 //            }
 //        })
     }
 */


    /*fun startIntentChooserMediation(context: Activity, newIntent: Intent) {
        if (!AppUtils.getBillingStatus() && interstitialAd_fb!=null && interstitialAd_fb!!.isAdLoaded && !interstitialAd_fb!!.isAdInvalidated ) {
            interstitialAd_fb!!.show()
            interstitialAd_fb!!.setAdListener(object : InterstitialAdListener {
                override fun onInterstitialDisplayed(ad: Ad) {
                    //   Log.d("FacebookAdsLog1", pref.getInt("counter", 0).toString() + "onInterstitialDisplayed")


//                interstitialAd_fb!!.loadAd()


//                if (pref.getInt("counter", 0) > 0 && pref.getInt("counter", 0) < 12) {
//                    editor.putInt("counter", pref.getInt("counter", 0) + 1).commit()

//                    context.startActivity(Intent(context, Transparent::class.java).putExtra("activity", 1))


//                    interstitialAd_fb=null

//                    interstitialAd_fb!!.set
//                } else {
//
//                    editor.putInt("counter", 0).commit()
//                }


//                interstitialAd_fb = null


//                if (pref.getBoolean("ads", true)) {
//                    interstitialAd_fb!!.loadAd()
//                }


                }

                override fun onInterstitialDismissed(ad: Ad) {

//                if (pref.getBoolean("ads", true)) {
//                    interstitialAd_fb!!.loadAd()

//                interstitialAd_fb = null
//                }
                    if (!AppUtils.getBillingStatus()) {
                        interstitialAd_fb!!.loadAd()
                    }
                    try {
                        context.startActivity(Intent.createChooser(newIntent, "Share Via"))
                    }catch (e:ActivityNotFoundException){
                        e.printStackTrace()
                    }catch (e:Exception){
                        e.printStackTrace()
                    }
//                context.finish()


                }

                override fun onError(ad: Ad, adError: AdError) {
                    Log.d("FacebookAdsLog1", "onError")
//                interstitialAd_fb = null



                }

                override fun onAdLoaded(ad: Ad) {
                    Log.d("FacebookAdsLog1", "onAdLoaded")

                }

                override fun onAdClicked(ad: Ad) {
                    Log.d("FacebookAdsLog1", "onAdClicked")

//                editor.putInt("counter", 1).commit()
                }

                override fun onLoggingImpression(ad: Ad) {
                    Log.d("FacebookAdsLog1", "onLoggingImpression")


//                (interstitialAd_fb as com.facebook.ads.InterstitialAd).destroy()


                }
            })


        } else {

            if (mInterstitialAd!=null && mInterstitialAd!!.isLoaded && !AppUtils.getBillingStatus()) {
                mInterstitialAd!!.show()

                mInterstitialAd?.adListener = object : AdListener() {
                    override fun onAdClosed() {

                        if (!AppUtils.getBillingStatus()) {
                            mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                        }

                        try {
                            context.startActivity(Intent.createChooser(newIntent, "Share Via"))
                        }catch (e:ActivityNotFoundException){
                            e.printStackTrace()
                        }catch (e:Exception){
                            e.printStackTrace()
                        }
//                    context.finish()

                    }
                }

            } else {
                try {
                    context.startActivity(Intent.createChooser(newIntent, "Share Via"))
                }catch (e:ActivityNotFoundException){
                    e.printStackTrace()
                }catch (e:Exception){
                    e.printStackTrace()
                }

//                context.finish()
            }

        }



    }*/

}