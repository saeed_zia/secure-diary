package com.diary.with.lock.myjournal.notepad.views.activities.ColorStyle

import android.app.Dialog
import android.content.ComponentName
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.Ads.FacebookNativeAd
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.AdLoadStatus
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.utils.Utils
import com.diary.with.lock.myjournal.notepad.views.activities.AdmobNativeAd
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import com.facebook.ads.AdOptionsView
import com.facebook.ads.NativeAd
import com.facebook.ads.NativeAdLayout
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import kotlinx.android.synthetic.main.activity_color_style.*
import kotlinx.android.synthetic.main.customize_native_ad.*
import kotlinx.android.synthetic.main.customize_native_ad.view.*
import kotlinx.android.synthetic.main.loading_fb_native.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class ColorStyleActivity : BaseActivity(), ColorPickerDialogListener,View.OnClickListener {

    var sharedPref: SharedPref? = null
    var selectColor: Int? = null
    var selectedSize: Float? = null
   var selectedInter:String?=null
    var nativeAd: NativeAd?=null
    var nativeAdLayout: NativeAdLayout?=null
    var adView: ConstraintLayout?=null
    var selectedNative:String?=null
    var unifiedNativeAd: UnifiedNativeAd?=null
    var addStatus:Boolean?=false
    private var ads: InterstitalAdsSplash?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_style)

        sharedPref = SharedPref(applicationContext)
        loadInterAd()

        setSetting()
       // seekBarListener()
        clickListener()
        showNativeAd()

        back_arrow.setOnClickListener {
            onBackPressed()
        }
        tv_font.setOnClickListener {
            val dialog = Dialog(this@ColorStyleActivity)
            dialog.setContentView(R.layout.font_selector_dialog)
            dialog.setTitle("This is my custom dialog box")
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            dialog.show()
            val rdg = dialog.findViewById(R.id.radio_group) as RadioGroup
            // val rd2 = dialog.findViewById(R.id.rd_2) as RadioButton
            rdg.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.rd_1 -> {
                        sharedPref!!.setFont("fonts/roboto_regular.ttf")
                        setMyTheme("roboto_theme")
                        dialog.dismiss()
                    }
                    R.id.rd_2 -> {
                        sharedPref!!.setFont("fonts/abhayalibre_regular.ttf")
                        setMyTheme("abhayalibre_regular")
                        dialog.dismiss()
                    }
                    R.id.rd_3 -> {
                        sharedPref!!.setFont("fonts/great_vibes.otf")
                        setMyTheme("great_vibes")
                        dialog.dismiss()
                    }
                    R.id.rd_4 -> {
                        sharedPref!!.setFont("fonts/kaushan.otf")
                        setMyTheme("kaushan")
                        dialog.dismiss()
                    }
                    R.id.rd_5 -> {
                        sharedPref!!.setFont("fonts/aguafina_script.ttf")
                        setMyTheme("aguafina_script")
                        dialog.dismiss()
                    }
                }
            })
           // dialog.show()
        }
        // simpleSeekBar.scaleY=3f


       // default_setting.setOnClickListener {
//            sharedPref?.setBgColor(getColor(R.color.colorPrimary))
//            sharedPref?.setTextSize(18f)
//            sharedPref?.setFont("fonts/roboto_regular.ttf")
//            setSetting()
//            onResume()



    //    }
        iv_more.setOnClickListener{
            ColorPickerDialog.newBuilder().show(this)
        }
     //   apply_setting.setOnClickListener{

     //   }
    }


    fun setMyTheme(font: String) {
        when (font) {
            "roboto_theme" -> {
                val face = Typeface.createFromAsset(assets, "fonts/roboto_regular.ttf")
                tv_font.setTypeface(face)
            }
            "great_vibes" -> {
                val face = Typeface.createFromAsset(assets, "fonts/kaushan.otf")
                tv_font.setTypeface(face)
            }
            "kaushan" -> {
                val face = Typeface.createFromAsset(assets, "fonts/kaushan.otf")
                tv_font.setTypeface(face)
            }
            "abhayalibre_regular" -> {
                val face = Typeface.createFromAsset(assets, "fonts/abhayalibre_regular.ttf")
                tv_font.setTypeface(face)
            }
            "aguafina_script" -> {
                val face = Typeface.createFromAsset(assets, "fonts/aguafina_script.ttf")
                tv_font.setTypeface(face)
            }
        }
    }

    fun setShortCut(iconNumber: Int) {
        try {

            packageManager.setComponentEnabledSetting(
                ComponentName(
                    this@ColorStyleActivity,
                    com.diary.with.lock.myjournal.notepad.utils.OneLauncherAlias::class.java
                ),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
            )
            packageManager.setComponentEnabledSetting(
                ComponentName(
                    this@ColorStyleActivity,
                    com.diary.with.lock.myjournal.notepad.utils.TwoLauncherAlias::class.java
                ),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
            )
            packageManager.setComponentEnabledSetting(
                ComponentName(
                    this@ColorStyleActivity,
                    com.diary.with.lock.myjournal.notepad.utils.ThreeLauncherAlias::class.java
                ),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
            )
            packageManager.setComponentEnabledSetting(
                ComponentName(
                    this@ColorStyleActivity,
                    com.diary.with.lock.myjournal.notepad.utils.FourLauncherAlias::class.java
                ),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
            )
            // packageManager.setComponentEnabledSetting(ComponentName(this, com.misles.dynamiclaunchericon.ThreeLauncherAlias::class.java), PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP)

            when (iconNumber) {
                1 -> { //pink
                    packageManager.setComponentEnabledSetting(
                        ComponentName(
                            this@ColorStyleActivity,
                            com.diary.with.lock.myjournal.notepad.utils.OneLauncherAlias::class.java
                        ),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
                    )
                }
                2 -> {
                    packageManager.setComponentEnabledSetting(
                        ComponentName(
                            this@ColorStyleActivity,
                            com.diary.with.lock.myjournal.notepad.utils.TwoLauncherAlias::class.java
                        ),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
                    )
                }
                3 -> {
                    packageManager.setComponentEnabledSetting(
                        ComponentName(
                            this@ColorStyleActivity,
                            com.diary.with.lock.myjournal.notepad.utils.ThreeLauncherAlias::class.java
                        ),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
                    )
                }
                4 -> {
                    packageManager.setComponentEnabledSetting(
                        ComponentName(
                            this@ColorStyleActivity,
                            com.diary.with.lock.myjournal.notepad.utils.FourLauncherAlias::class.java
                        ),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
                    )
                }

            }
            Toast.makeText(this, getString(R.string.shortcut_would), Toast.LENGTH_SHORT)
                .show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setSetting() {
        activity_title.setText(getString(R.string.color_style))
        selectedSize = sharedPref?.getTextSize()!!
        size_seekbar.setProgress(selectedSize!!)
        selectColor = sharedPref?.getBgColor()
        var font = sharedPref!!.getFont()
        tv_font.setTextSize(selectedSize!!)
        val face = Typeface.createFromAsset(assets, font)
//
        if(sharedPref!!.getQrIconStatus().equals("false")){
            qr_icon.visibility=View.GONE
        }

      //  font_textview.setTypeface(face)

    }
    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getColorStyleActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getColorStyleActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if ((sharedPref!!.getColorStyleActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getColorStyleActivityInterAdStatusAD()
                    .equals("true"))
            ) {

                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }

    override fun onBackPressed() {

        sharedPref?.setTextSize(selectedSize!!)
        sharedPref!!.setBgColor(selectColor!!)
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgColorStyle())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgColorStyle())
        }
        else{
            finish()
            //startActivity(Intent(this, DashboardActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        var bgColor = sharedPref?.getBgColor()
        color_style_root?.setBackgroundColor(bgColor!!)
        //font_textview.setTextSize(sharedPref!!.getTextSize())
        window.statusBarColor = sharedPref!!.getBgColor()

    }

    fun clickListener(){
        color1.setOnClickListener(this)
        color2.setOnClickListener(this)
        color3.setOnClickListener(this)
        color4.setOnClickListener(this)
        color5.setOnClickListener(this)
       //
        icon1_const.setOnClickListener(this)
        icon2_const.setOnClickListener(this)
        icon3_const.setOnClickListener(this)
        icon4_const.setOnClickListener(this)
        size_seekbar.setOnSeekChangeListener(object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams) {

                Log.i("FragmentActivity.TAG", seekParams.progress.toString())
                selectedSize=seekParams.progressFloat
                 tv_font.textSize=seekParams.progressFloat
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
        })

        qr_icon.setOnClickListener{
            try {

                Utils.openQrReference(this)
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }
    }
    override fun onDialogDismissed(dialogId: Int) {

    }

    override fun onColorSelected(dialogId: Int, color: Int) {
        color_style_root.setBackgroundColor(color)
        selectColor=color

    }

    override fun onClick(v: View?) {
       when(v!!.id){
           R.id.color1->{
             color_style_root.setBackgroundColor(Color.parseColor("#ff9b9b"))
               selectColor=Color.parseColor("#ff9b9b")
           }
           R.id.color2->{
               color_style_root.setBackgroundColor(Color.parseColor("#94eb9e"))
               selectColor=Color.parseColor("#94eb9e")
           }
           R.id.color3->{
               color_style_root.setBackgroundColor(Color.parseColor("#94caeb"))
               selectColor=Color.parseColor("#94caeb")
           }
           R.id.color4->{
               color_style_root.setBackgroundColor(Color.parseColor("#a594eb"))
               selectColor=Color.parseColor("#a594eb")
           }
           R.id.color5->{
               color_style_root.setBackgroundColor(Color.parseColor("#de94eb"))
               selectColor= Color.parseColor("#de94eb")
           }
         //
           R.id.icon1_const->{
               setShortCut(1)
               //Toast.makeText(this, "Icon has been changed", Toast.LENGTH_SHORT).show()
           }
           R.id.icon2_const->{
               setShortCut(2)
              // Toast.makeText(this, "Icon has been changed", Toast.LENGTH_SHORT).show()
           }
           R.id.icon3_const->{
               setShortCut(3)
              // Toast.makeText(this, "Icon has been changed", Toast.LENGTH_SHORT).show()
           }
           R.id.icon4_const->{
               setShortCut(4)
              // Toast.makeText(this, "Icon has been changed", Toast.LENGTH_SHORT).show()
           }

       }
    }
    private fun inflateAd(nativeAd: NativeAd) {
        nativeAd.unregisterView()

        // Add the Ad view into the ad container.
        nativeAdLayout = findViewById(R.id.native_ad_container)
        val inflater = LayoutInflater.from(this)
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        adView =inflater.inflate( R.layout.loading_fb_native, nativeAdLayout,false) as ConstraintLayout
        nativeAdLayout!!.addView(adView)

        // Add the AdOptionsView
        val adChoicesContainer: LinearLayout = findViewById(R.id.ad_choices_container)
        val adOptionsView = AdOptionsView(this, nativeAd, nativeAdLayout)
        adChoicesContainer.removeAllViews()
        adChoicesContainer.addView(adOptionsView, 0)

        // Create native UI using the ad metadata.
        val nativeAdIcon: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_icon)
        val nativeAdTitle: TextView = adView!!.findViewById(R.id.native_ad_title)
        val nativeAdMedia: com.facebook.ads.MediaView = adView!!.findViewById(R.id.native_ad_media)
        val nativeAdSocialContext: TextView = adView!!.findViewById(R.id.native_ad_social_context)
        val nativeAdBody: TextView = adView!!.findViewById(R.id.native_ad_body)
        val sponsoredLabel: TextView = adView!!.findViewById(R.id.native_ad_sponsored_label)
        val nativeAdCallToAction: Button = adView!!.findViewById(R.id.native_ad_call_to_action)

        // Set the Text.
        nativeAdTitle.text = nativeAd.advertiserName
        nativeAdBody.text = nativeAd.adBodyText
        nativeAdSocialContext.text = nativeAd.adSocialContext
        nativeAdCallToAction.setVisibility(if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE)
        nativeAdCallToAction.setText(nativeAd.adCallToAction)
        sponsoredLabel.text = nativeAd.sponsoredTranslation

        // Create a list of clickable views
        val clickableViews: MutableList<View> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(nativeAdIcon)
        clickableViews.add(nativeAdMedia)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(adView, nativeAdMedia, nativeAdIcon, clickableViews )
        //native_ad_container!!.setBackgroundResource(R.drawable.ad_border)
    }
    private fun showFacebookNative1() {
        //
        if (FacebookNativeAd.nativeAd1 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd2(this)
            nativeAd = FacebookNativeAd.nativeAd1
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
              // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus1.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd2(this)
            hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    private fun showFacebookNative2() {
        //
        if (FacebookNativeAd.nativeAd2 != null) {
            addStatus=true
            FacebookNativeAd.loadNativeFacebookAd1(this)
            nativeAd = FacebookNativeAd.nativeAd2
            inflateAd(nativeAd!!)
            //remove animation
            tv_ad_loading.visibility = View.INVISIBLE
            pb_ad_loading!!.visibility = View.INVISIBLE
            if (native_ad_media!=null) {
               // makeNonClickAbleFB()
            }
        } else if (FacebookNativeAd.adStatus2.equals("failed")) {
            FacebookNativeAd.loadNativeFacebookAd1(this)
            hideNativeAnim()
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }

    fun makeNonClickAbleAD(){
        ad_media.isClickable = false
        ad_headline.isClickable = false
        ad_body.isClickable = false
        ad_icon.isClickable = false
    }
    fun makeNonClickAbleFB(){
        native_ad_media.isClickable = false
        native_ad_social_context.isClickable = false
        native_ad_body.isClickable = false
        native_ad_icon.isClickable = false
    }
    fun hideNativeAnim(){
        tv_ad_loading.visibility = View.GONE
        pb_ad_loading.visibility = View.GONE
        native_ad_const.visibility=View.GONE
    }
    fun showNativeAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getColorStyleNativeAdStatusFB()
                    .equals("true") && sharedPref!!.getColorStyleNativeAdStatusAD()
                    .equals("false"))
            {
                if(FacebookNativeAd.selectedAd.equals("ad1")){
                    showFacebookNative1()
                }
                else if (FacebookNativeAd.selectedAd.equals("ad2")){
                    showFacebookNative2()
                }
                selectedNative = "fb"
            }
            else if ((sharedPref!!.getColorStyleNativeAdStatusFB()
                    .equals("false") && sharedPref!!.getColorStyleNativeAdStatusAD()
                    .equals("true")))
            {
                if(AdmobNativeAd.selectedAd.equals("ad1")){
                    showAdmobNativeAd1()
                }
                else if (AdmobNativeAd.selectedAd.equals("ad2")){
                    showAdmobNativeAd2()
                }
                selectedNative = "ad"
            } else {
                hideNativeAnim()
                selectedNative = "no"
            }
        }
        else{
            hideNativeAnim()
            selectedNative="no"
        }
    }
    fun showAdmobNativeAd1(){

        if (AdmobNativeAd.nativeAd1 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd1
            val adView = layoutInflater.inflate(
                R.layout.color_style_native_admob, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
           // makeNonClickAbleAD()

        } else if (AdmobNativeAd.adStatus1.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId2(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    fun showAdmobNativeAd2(){

        if (AdmobNativeAd.nativeAd2 != null) {
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
            unifiedNativeAd = AdmobNativeAd.nativeAd2
            val adView = layoutInflater.inflate(
                R.layout.color_style_native_admob, null
            ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView)
            admob_ad.removeAllViews()
            admob_ad.addView(adView)
            //remove animation
            tv_ad_loading.visibility = View.GONE
            pb_ad_loading.visibility = View.GONE
           // makeNonClickAbleAD()
        } else if (AdmobNativeAd.adStatus2.equals("failed")) {
            hideNativeAnim()
            addStatus=true
            AdmobNativeAd.loadAdId1(this)
        } else {
            //trying to load
            Log.d("ddd", "dd")
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(adloadStatus: AdLoadStatus) {
        if(adloadStatus.isLoaded!!) {
            if(!addStatus!!){
                showNativeAd()

            }
        }
        else{
            if (!addStatus!!){
                hideNativeAnim()
            }
            addStatus=true
            //
            loadNewNativeAd(adloadStatus.idType,adloadStatus.idNumber)
        }
    }

    fun loadNewNativeAd(type:String,idNumber:String){
        if (type.equals("fb")){
            if (idNumber.equals("id1")){
                FacebookNativeAd.loadNativeFacebookAd2(applicationContext)
            }
            else{
                FacebookNativeAd.loadNativeFacebookAd1(applicationContext)
            }
        }
        else if(type.equals("ad")){
            if (idNumber.equals("id1")){
                AdmobNativeAd.loadAdId2(applicationContext)
            }
            else{
                AdmobNativeAd.loadAdId1(applicationContext)
            }
        }
    }
    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd?,
        adView: UnifiedNativeAdView
    )
    {

        var media=adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
        media.setImageScaleType(ImageView.ScaleType.FIT_XY)

        adView.headlineView = adView.findViewById(R.id.ad_headline)
        //adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        adView.mediaView = media
        adView.callToActionView = adView.findViewById(R.id.btn_install)
        adView.iconView = adView.findViewById(R.id.ad_icon)
        adView.bodyView = adView.findViewById(R.id.ad_body)

        if (unifiedNativeAd != null) {
            adView.ad_headline.text = unifiedNativeAd.headline
        }
        if (unifiedNativeAd != null) {
            if (unifiedNativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.ad_body.text = unifiedNativeAd.body
                adView.bodyView.visibility = View.VISIBLE
            }

            /* if (unifiedNativeAd.advertiser == null) {
                 adView.advertiserView.visibility = View.INVISIBLE
             } else {
                 adView.ad_advertiser.text = unifiedNativeAd.advertiser
                 adView.advertiserView.visibility = View.VISIBLE
             }*/
            if (unifiedNativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                adView.ad_icon.setImageDrawable(unifiedNativeAd.icon.drawable)
                adView.iconView.visibility = View.VISIBLE
            }
            if (unifiedNativeAd.callToAction==null)
            {
                adView.btn_install.visibility= View.GONE
            }
            else{
                adView.btn_install.text = unifiedNativeAd.callToAction
            }

        }

        adView.setNativeAd(unifiedNativeAd)

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

}