package com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity

import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase


interface TrashContract {


    interface View {

        fun showAllData(note_list: List<Note>)
    }

    interface Presenter {
        fun getAllDatFromDatabase(appDatabase: NoteDatabase)
        fun deleteNoteFromDatabase(appDatabase: NoteDatabase, noteId: Int)
        fun restoreNote(appDatabase: NoteDatabase,noteId:Int)
        //
        fun deleteNoteFromBackupDatabase(appDatabase: NoteDatabase, noteId: Int)
        fun restoreNoteInBackup(appDatabase: NoteDatabase,noteId:Int)
    }
}