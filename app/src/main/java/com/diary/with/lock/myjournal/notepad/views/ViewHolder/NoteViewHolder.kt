package com.diary.with.lock.myjournal.notepad.views.ViewHolder


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.models.NoteModel



class NoteViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
    private var mTitleView: TextView? = null
    private var mContent: TextView? = null
    private var mMonth: TextView? = null
    private var mDay: TextView? = null
    private var myear: TextView? = null


    init {
        mTitleView = itemView.findViewById(R.id.note_title)
        mContent = itemView.findViewById(R.id.note_content)
        mMonth = itemView.findViewById(R.id.month)
        mDay = itemView.findViewById(R.id.day)
        myear = itemView.findViewById(R.id.year)
    }

    fun bind(note: NoteModel) {
        if(note.noteType.equals("simpleNote")){
            mTitleView?.text = note.noteTitle
            mContent?.text = note.noteContent
            mMonth?.text=note.month
            mDay?.text=note.day
            myear?.text=note.year
            mMonth!!.setBackgroundResource(R.drawable.claender_corner)
        }
        else if(note.noteType.equals("checkListNote")){
            mTitleView?.text = note.noteTitle
            if(note.checkList!!.size>0){
                mContent?.text=note.checkList[0].text
            }
            mMonth?.text=note.month
            mDay?.text=note.day
            myear?.text=note.year
            mMonth!!.setBackgroundResource(R.drawable.check_list_calender_corner)
        }

    }

}