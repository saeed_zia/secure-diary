package com.diary.with.lock.myjournal.notepad.views.activities.PrivacyPolicy


import android.os.Bundle
import android.os.Handler
import android.view.View
import com.diary.with.lock.myjournal.notepad.Ads.FacebookAds
import com.diary.with.lock.myjournal.notepad.R
import com.diary.with.lock.myjournal.notepad.utils.InterstitalAdsSplash
import com.diary.with.lock.myjournal.notepad.utils.SharedPref
import com.diary.with.lock.myjournal.notepad.views.activities.BaseActivity.BaseActivity
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import kotlinx.android.synthetic.main.loading_layout.*


class PrivacyPolicy : BaseActivity() {
    var sharedPref: SharedPref?=null
    private var ads: InterstitalAdsSplash?=null
   var selectedInter:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        sharedPref= SharedPref(this)
        loadInterAd()
    }

    fun loadInterAd(){
        if(!sharedPref!!.isBillPayed()) {
            if (sharedPref!!.getPrivacyActivityInterAdStatusFB()
                    .equals("true") && sharedPref!!.getPrivacyActivityInterAdStatusAD()
                    .equals("false")
            ) {
                FacebookAds.facebookInterstitialAds(this)
                selectedInter = "fb"
            } else if  (sharedPref!!.getPrivacyActivityInterAdStatusFB()
                    .equals("false") && sharedPref!!.getPrivacyActivityInterAdStatusAD()
                    .equals("true")
            ) {

                ads= InterstitalAdsSplash()
                selectedInter = "ad"
            } else {
                selectedInter = "no"
            }
        }
        else{
            selectedInter="no"
        }
    }
    override fun onBackPressed() {
        if(selectedInter.equals("fb")) {
            if ( FacebookAds.interstitialAd != null) {
                showInterstitialFb()

            } else {
                super.onBackPressed()
            }
        }
        else if(selectedInter.equals("ad")){
            ads= InterstitalAdsSplash()
            showInterstitialAdmob()
        }
        else{
            super.onBackPressed()
        }
    }
    fun showInterstitialFb(){
        if (FacebookAds.interstitialAd!!.isAdLoaded) {
            loading_root.visibility= View.VISIBLE
            constloademail.visibility= View.VISIBLE
            Handler().postDelayed({
                FacebookAds.interstitialAd!!.show()
                super.onBackPressed()

            },  sharedPref!!.getProgPrivacyPolicy())


        } else {
            super.onBackPressed()
        }
    }
    fun showInterstitialAdmob(){
        loading_root.visibility= View.VISIBLE
        constloademail.visibility=View.VISIBLE
        if(ads!=null) {
            Handler().postDelayed({
                ads?.adMobShowCloseOnly(this)

            },  sharedPref!!.getProgPrivacyPolicy())
        }
        else{
            super.onBackPressed()

        }
    }

    override fun onResume() {
        super.onResume()
        window.statusBarColor = sharedPref!!.getBgColor()

    }
}