package com.diary.with.lock.myjournal.notepad.interfaces

interface CheckBoxClickListener {
    fun onCheckBoxClickListener(status:Boolean,position: Int)
    fun onAfterTextChangeListener(text:String,position: Int)
    fun onEnterPressed(position: Int)
    fun deleteCheckItem(position: Int)
}