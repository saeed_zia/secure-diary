package com.diary.with.lock.myjournal.notepad.interfaces

interface QuotationListeners {
    fun  onCopyClickListener(author:String,quote:String)
    fun onShareClickListener(author:String,quote:String)
    fun onCellClickListener(quoteText: String)
}