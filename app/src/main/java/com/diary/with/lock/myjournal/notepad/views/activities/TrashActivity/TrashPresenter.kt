package com.diary.with.lock.myjournal.notepad.views.activities.TrashActivity

import android.util.Log
import com.diary.with.lock.myjournal.notepad.Database.Note
import com.diary.with.lock.myjournal.notepad.Database.NoteDatabase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class TrashPresenter (view: TrashContract.View): TrashContract.Presenter {
    var mView=view

    override fun getAllDatFromDatabase(appDatabase: NoteDatabase) {
        var list = listOf<Note>()
        try {

            GlobalScope.launch(Dispatchers.Default) {
                list = appDatabase.noteDao().getAll()
                mView.showAllData(list)
            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun deleteNoteFromDatabase(appDatabase: NoteDatabase, id:Int) {
        try {

            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().deleteNoteById(id)

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun restoreNote(appDatabase: NoteDatabase, noteId: Int) {
        try {

            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().restoreNote(noteId,false)  ///istrashed =false

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun deleteNoteFromBackupDatabase(appDatabase: NoteDatabase, noteId: Int) {
        try {

            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().deleteNoteById(noteId)

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }

    override fun restoreNoteInBackup(appDatabase: NoteDatabase, noteId: Int) {
        try {

            GlobalScope.launch(Dispatchers.Default) {
                appDatabase.noteDao().restoreNote(noteId,false)  ///istrashed =false

            }

        }
        catch (e: Exception){
            Log.d("get hello",e.toString())
        }
    }


}