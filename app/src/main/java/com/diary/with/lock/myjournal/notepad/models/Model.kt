package com.diary.with.lock.myjournal.notepad.models


import com.diary.with.lock.myjournal.notepad.R



enum class Model private constructor(val titleResId: Int, val layoutResId: Int) {
    RED(1, R.layout.fingerprint_tutorial),
    RED1(2, R.layout.language_tutorial),
    RED2(3,R.layout.reminder_tutorial),
    RED3(4, R.layout.tutorial_business),
    RED4(5, R.layout.tutorial_color),
    RED5(6, R.layout.tutorial_daily),
    RED6(7, R.layout.tutorial_girl),
    RED7(8, R.layout.tutorial_inspirational),
    RED8(9, R.layout.tutorial_love),
    RED9(10, R.layout.tutorial_romantic),
    RED10(11, R.layout.tutorial_student),
    RED11(12, R.layout.tutorial_todo)



}